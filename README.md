# RutesCompartides.cat

## Introducció

Aquest es el software lliure creat amb python/Django per a l'eina [RutesCompartides.cat](https://rutescompartides.cat)

## Instalació
Versió Postgres recomendada:
 PostgreSQL 10 i PostGIS 2.5

```
- Instalar Postgis: 
     sudo docker run --name postgis-instance -e POSTGRES_PASSWORD=docker -p 25432:5432 -d postgis/postgis:10-2.5
- Descargar repository git
cd rutescompartides
mkvirtualenv rutescompartides
pip install -r requirements.txt
sudo locale-gen ca_ES.utf8
sudo update-locale
cp local_settings_template.py local_settings.py
#modifica local_settings.py con tus credenciale privadas
python manage.py runserver
```

## API

curl -X POST http://0.0.0.0:8000/api/register/ \
  -H "Content-Type: application/json" \
  -d '{
    "username": "new_user",
    "email": "your_email@email.com",
    "password1": "your_password",
    "password2": "your_password",
    "firstname": "John",
    "lastname": "Doe",
    "phone": "123456789",
    "newsletter": "on"
  }'

curl -X POST http://0.0.0.0:8000/api/token/ -d "username=myusername&password=XXXXXX" 
curl -H "Authorization: Token YOUR_TOKEN_FROM_LOGIN" http://0.0.0.0:8000/api/triprequest/6/
curl -H "Authorization: Token YOUR_TOKEN_FROM_LOGIN" http://0.0.0.0:8000/api/triproffer/7/

curl http://0.0.0.0:8000/offer/view/all/geojson
curl http://0.0.0.0:8000/offer/view/123/geojson
curl http://0.0.0.0:8000/request/view/all/geojson
curl http://0.0.0.0:8000/request/view/125/geojson

curl http://0.0.0.0:8000/api/tripoffer/7/
curl -H "Authorization: Token YOUR_TOKEN_FROM_LOGIN" http://0.0.0.0:8000/api/tripoffer/7/

curl http://0.0.0.0:8000/api/triprequest/5/
curl -H "Authorization: Token YOUR_TOKEN_FROM_LOGIN" http://0.0.0.0:8000/api/triprequest/5/

## Llicència
Affero GNU GPL-v3
