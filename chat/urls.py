from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    # path("search/", views.search, name="search"),
    # path("addfriend/<str:name>", views.addFriend, name="addFriend"),
    path("msg/<str:username>", views.chat, name="chat"),
    path(
        "api/messages/<int:sender>/<int:receiver>",
        views.message_list,
        name="message-detail",
    ),
    path("api/messages", views.message_list, name="message-list"),
    path(
        "api/unseencount/<int:receiver>",
        views.get_unseen_messages_count_json,
        name="messages-unseen-count",
    ),
]
