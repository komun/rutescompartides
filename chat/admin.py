from django.contrib import admin
from .models import Messages

# Register your models here.


# admin.site.register(Friends)
class MessagesAdmin(admin.ModelAdmin):
    list_display = (
        "sender_name",
        "receiver_name",
        "timestamp",
        "time",
        "seen",
    )
    exclude = ("description",)


admin.site.register(Messages, MessagesAdmin)
