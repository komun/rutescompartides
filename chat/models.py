import re

from django.db import models
from django.utils import timezone

# from core.models import UserProfile
from django.contrib.auth.models import User

_urlfinderregex = re.compile(r"(?:http://)?\w+\.\S*[^.\s]")


def linkify(text):
    if re.search(_urlfinderregex, text):
        text = re.sub(
            _urlfinderregex, r'<a class="comurl" href="https://\g<0>">\g<0></a>', text
        )
    return text


class Messages(models.Model):
    description = models.TextField()
    sender_name = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="sender"
    )
    receiver_name = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="receiver"
    )
    time = models.TimeField(auto_now_add=True)
    seen = models.BooleanField(default=False)
    unseen_notif = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"To: {self.receiver_name} From: {self.sender_name}"

    @property
    def rich_description(self):
        return linkify(self.description)

    @property
    def smart_time(self):
        if self.timestamp.date() < timezone.datetime.today().date():
            return self.timestamp
        else:
            return self.time

    class Meta:
        ordering = ("timestamp",)


#
#
# class Friends(models.Model):
#
#     user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
#     friend = models.IntegerField()
#
#     def __str__(self):
#         return f"{self.friend}"
#
