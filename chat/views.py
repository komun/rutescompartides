from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from .models import Messages  # Friends
from core.models import UserProfile
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from chat.serializers import MessageSerializer
from django.db.models import Count


def get_unseen_messages_count_json(request, receiver):
    unseen_count = Messages.objects.filter(receiver_name=receiver, seen=False).count()
    return JsonResponse(unseen_count, safe=False)


def get_unseen_messages_count(id):
    unseen_count = Messages.objects.filter(receiver_name=id, seen=False).count()
    return unseen_count


def getUnseenMessages(id):
    unseen_msgs = (
        Messages.objects.filter(receiver_name=id, seen=False)
        .values("sender_name__username")
        .annotate(total=Count("sender_name__username"))
        .order_by()
    )
    usermsgs = {}
    for i in unseen_msgs:
        uername_sender = i["sender_name__username"]
        usermsgs[uername_sender] = i["total"]
    return usermsgs


def getFriendsList(id):
    """
    Get the list of friends of the  user
    :param: user id
    :return: list of friends
    """

    # userprofile = UserProfile.objects.get(id=id)

    mysenders = set(
        Messages.objects.filter(sender_name=id).values_list(
            "receiver_name__username", flat=True
        )
    )
    myrecievers = set(
        Messages.objects.filter(receiver_name=id).values_list(
            "sender_name__username", flat=True
        )
    )
    friend_usernames = list(mysenders.union(myrecievers))
    return friend_usernames

    # return []


def getUserProfileId(user):
    """
    Get the user id by the username
    :param user:
    :return: int
    """
    try:
        return user.userprofile.pk
    except:
        return 0


@login_required
def index(request):
    """
    Return the home page
    :param request:
    :return:
    """

    id = getUserProfileId(request.user)
    friends = getFriendsList(id)
    unseen_msgs = getUnseenMessages(id)
    print("unseen_msgs", unseen_msgs)

    return render(
        request, "chat/Base.html", {"friends": friends, "unseen_msgs": unseen_msgs}
    )


@login_required
def chat(request, username):
    """
    Get the chat between two users.
    :param request:
    :param username:
    :return:
    """
    friend = UserProfile.objects.get(user__username=username)
    id = getUserProfileId(request.user)
    curr_user = request.user.userprofile
    messages = Messages.objects.filter(
        sender_name=id, receiver_name=friend.pk
    ) | Messages.objects.filter(sender_name=friend.pk, receiver_name=id)

    if request.method == "GET":
        friends = getFriendsList(id)
        if username not in friends:
            friends.append(username)
        return render(
            request,
            "chat/messages.html",
            {
                "messages": messages,
                "friends": friends,
                "curr_user": curr_user,
                "friend": friend,
            },
        )


@login_required
@csrf_exempt
def message_list(request, sender=None, receiver=None):
    if request.method == "GET":
        messages = Messages.objects.filter(
            sender_name=sender, receiver_name=receiver, seen=False
        )
        serializer = MessageSerializer(
            messages, many=True, context={"request": request}
        )
        for message in messages:
            message.seen = True
            message.save()
        return JsonResponse(serializer.data, safe=False)

    elif request.method == "POST":
        data = JSONParser().parse(request)
        serializer = MessageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
