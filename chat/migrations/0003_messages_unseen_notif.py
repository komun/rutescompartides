# Generated by Django 3.2 on 2023-11-14 21:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0002_auto_20230111_1903'),
    ]

    operations = [
        migrations.AddField(
            model_name='messages',
            name='unseen_notif',
            field=models.BooleanField(default=False),
        ),
    ]
