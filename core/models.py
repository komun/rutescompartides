# -*- coding: utf-8 -*-
from django.db.models import Q
from django.core.validators import (
    MinValueValidator,
    FileExtensionValidator,
)
from django.db.models import Max
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.db.models import F
from django.contrib.auth.models import User
from django.shortcuts import reverse
from django.contrib.gis.db import models as gis_models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_extensions.db.models import TimeStampedModel
from taggit.managers import TaggableManager


# from django_project.settings import BASE_DIR

from .helpers import CharFieldWithTextarea, CharFieldWithBigTextarea
from core import gis_utils, coopcycle
from core.utils import send_notifications, _filter_town, pay, send_html_email
from operator import itemgetter



class AgreementType(models.TextChoices):
    FRIENDLY = "FRIENDLY", _("Amistós")
    COMMERCIAL = "COMMERCIAL", _("Comercial")

class LogisticNode(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(
        User,
        verbose_name=_("Autor/a"),
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    name = models.CharField(_("Nom"), max_length=200, null=False, blank=False)
    origin_location = gis_models.PointField(
        _("Origin Location"), null=False, blank=False, geography=True, srid=4326
    )
    origin_name = models.CharField(
        _("Localització"), max_length=300, null=True, blank=True
    )
    phone = models.CharField(_("Telèfon"), max_length=50, null=True, blank=True)
    email = models.EmailField(_("Correu electrònic"), max_length=100, null=True, blank=False)

    agreement_type = models.CharField(
        _("Acord per compartir l'ús de l'espai"),
        max_length=10,
        choices=AgreementType.choices,
        null=True, blank=True
    )
    # a cron job will deactive the past date requests
    deleted = models.BooleanField("Esborrat", default=False)
    active = models.BooleanField("Actiu", default=False)

    allows_parking = models.BooleanField(
        "Permet estacionament/aturada de vehicle davant la porta", null=True, blank=True
    )
    dimensions_dry_m2 = models.FloatField(
        _("m² Estocatge sec"),
        null=False,
        blank=False,
        default=0
    )
    dimensions_fresh_m2 = models.FloatField(
        _("m² Estocatge fresc (càmeres)"),
        null=False,
        blank=False,
        default=0
    )
    timetable = CharFieldWithTextarea(
        _("Horaris d'apertura. Contactes especials."),
        max_length=500,
        null=True,
        blank=True,
    )
    delivery_companies = models.CharField(
        _("Servei de distribució disponible des del Node"), max_length=300, null=True, blank=True,
        help_text="Indicar empreses amb qui s'ofereix o indicar-hi \"servei propi\"",
    )
    vehicle_types = CharFieldWithTextarea(
        _("Tipus de vehicles disponibles per a distribució del node"),
        max_length=500,
        null=True,
        blank=True,
    )
    has_isothermic = models.BooleanField(
        "Espai estanc aïllat isoterm", null=True, blank=True
    )
    has_frigo_room = models.BooleanField("Cambra frigorífica", null=True, blank=True)
    has_fridge = models.BooleanField("Nevera", null=True, blank=True)
    has_freezer = models.BooleanField(
        "Congelador", null=True, blank=True
    )
    has_cold_dry = models.BooleanField("Fred sense humitat", null=True, blank=True)
    has_toro = models.BooleanField(
        "Toro", null=True, blank=True
    )
    has_traspalet = models.BooleanField(
        "Traspalet", null=True, blank=True
    )
    has_loading_dock = models.BooleanField(
        "Moll de descàrrega camions", null=True, blank=True
    )
    price_per_dry_storage_day = models.FloatField(
        _("€ per cada dia d'emmagatzematge sec"),
        null=False,
        blank=False,
        default=0
    )
    price_per_fresh_storage_day = models.FloatField(
        _("€ per cada dia d'emmagatzematge fresc (càmeres)"),
        null=False,
        blank=False,
        default=0
    )
    comments = CharFieldWithTextarea(
        _("Notes / Comentaris"),
        max_length=500,
        null=True,
        blank=True,
    )

    delivery_areas = CharFieldWithTextarea(
        _("Zones de distribució (format .geojson"),
        max_length=500,
        null=True,
        blank=True,
    )
    api_url = models.URLField(_("Url de la instància Coopcycle"), max_length=100, null=True, blank=True,
        help_text="Copia directament l'enllaç de la pàgina, ja que només accepta el format complet de pàgina web https://...")

    api_client_id = models.CharField(
        _("Client ID d'accés per a l'API de la instància Coopcycle"), max_length=300, null=True, blank=True,
        help_text="Aquesta informació te la poden facilitar des de Coopcycle si els escrius un correu a contact@coopcycle.org o paul.iano@gmail.com"
    )
    api_client_secret = models.CharField(
        _("'Client Secret' d'accés per a l'API de la instància Coopcycle"), max_length=300, null=True, blank=True,
        help_text="Aquesta informació te la poden facilitar des de Coopcycle si els escrius un correu a contact@coopcycle.org o paul.iano@gmail.com"
    )
    checkout_form_url = models.CharField(
        _("Url del formulari de la botiga per a demanar un transport dins de Coopcycle"), max_length=300, null=True, blank=True,
        help_text="Haureu de crear un formulari, dins de Coopcycle, amb les tarifes que considereu oportunes i enganxar aquí l’enllaç."
    )

    @property
    def check_access_token(self):
        return coopcycle.get_access_token(self.api_url, self.api_client_id, self.api_client_secret)



@receiver(post_save, sender=LogisticNode)
def create_logistic_node(sender, instance, created, **kwargs):
    if created:
        print("creating LogisticNode", instance)
        # send email to approve to admins /admin /instance.id
        url = settings.DEFAULT_DOMAIN + "/admin/core/logisticnode/"+str(instance.pk)+"/change/"
        html = f'Un nou node logístic ha sigut creat. Fes clic <a href="{url}">ací</a> per a validarlo o eliminar-lo.'
        send_html_email(settings.LOGISTIC_NODE_ADMINS, "Nou punt logístic a validar", html)


class FrequencyType(models.TextChoices):
    ONCE = "ONCE", _("No es repeteix")
    DAILY = "DAILY", _("Diària")
    WEEKLY = "WEEKLY", _("Setmanal")
    BIWEEKLY = "BIWEEKLY", _("Bisetmanal")
    MONTHLY = "MONTHLY", _("Mensual")


def search_trip_requests(
    user,
    route,
    origin,
    destination,
    transport_isothermic,
    transport_cold,
    transport_freezer,
    transport_dry,
    time_pickup,
    tags,
    distance,
    exclude_frozen=True,
    exclude_user=True,
):

    if route is None and origin is not None and destination is not None:
        print("Requestig Route for origin=", origin, "destination=", destination)
        linestring = gis_utils.request_route([origin, destination])["linestring"]
        route = linestring.transform(3875, clone=True)
    trip_requests = TripRequest.objects.filter(active=True).order_by("min_time_arrival")
    if exclude_frozen:
        trip_requests = trip_requests.exclude(frozen=True)

    if not user.is_anonymous and exclude_user:
        trip_requests = trip_requests.exclude(client=user)
    if time_pickup:
        print("search_trip_requests time_pickup", time_pickup)
        trip_requests = trip_requests.filter(min_time_arrival__lte=time_pickup)
        trip_requests = trip_requests.filter(
            Q(max_time_arrival__isnull=True) | Q(max_time_arrival__gte=time_pickup)
        )
        print("trip_requests", trip_requests)

    if not transport_isothermic and not transport_cold:
        trip_requests = trip_requests.exclude(transport_isothermic=True)
    if not transport_cold:
        trip_requests = trip_requests.exclude(transport_cold=True)
    if not transport_freezer:
        trip_requests = trip_requests.exclude(transport_freezer=True)
    if not transport_dry:
        trip_requests = trip_requests.exclude(transport_dry=True)

    if tags:
        print("trip_requests", trip_requests)
        print("tags to filter triprequests", tags)

        trip_requests = trip_requests.filter(tags__slug__in=tags)
        print("trip_requests", trip_requests)
        # import ipdb
        #
        # ipdb.set_trace()
        print("trip_requests", len(trip_requests))

    if route:
        trip_requests = trip_requests.annotate(
            distance_origin=Distance("origin_location", route)
        )
        trip_requests = trip_requests.annotate(
            distance_destination=Distance("destination_location", route)
        )
        trip_requests = trip_requests.filter(distance_origin__lte=distance * 1000)
        trip_requests = trip_requests.filter(distance_destination__lte=distance * 1000)

        trip_oneway_origin = (
            trip_requests.annotate(
                distance_origin_destination=Distance("origin_location", destination)
            )
            .annotate(distance_origin_origin=Distance("origin_location", origin))
            .exclude(distance_origin_origin__gt=F("distance_origin_destination"))
        )
        trip_oneway_destination = (
            trip_requests.annotate(
                distance_destination_origin=Distance("destination_location", origin)
            )
            .annotate(
                distance_destination_destination=Distance(
                    "destination_location", destination
                )
            )
            .exclude(
                distance_destination_destination__gt=F("distance_destination_origin")
            )
        )

        trip_requests = trip_oneway_origin | trip_oneway_destination

    elif origin:
        trip_requests = trip_requests.annotate(
            distance_origin=Distance("origin_location", origin)
        )
        trip_requests = trip_requests.filter(distance_origin__lte=distance * 1000)
    elif destination:
        trip_requests = trip_requests.annotate(
            distance_destination=Distance("destination_location", destination)
        )
        trip_requests = trip_requests.filter(distance_destination__lte=distance * 1000)

    # for trip in trip_requests:
    #     print(trip.id, trip.origin_name)
    #     if hasattr(trip, "distance_origin"):
    #         print("km origin:", trip.distance_origin)
    #     if hasattr(trip, "distance_destination"):
    #         print("km_destination origin:", trip.distance_destination)
    #     if hasattr(trip, "distance_origin_origin"):
    #         print("distance_origin_origin:", trip.distance_origin_origin)
    #     if hasattr(trip, "distance_origin_destination"):
    #         print("distance_origin_destination:", trip.distance_origin_destination)

    return trip_requests.distinct()


def search_trip_offers(
    user,
    origin,
    destination,
    transport_isothermic,
    transport_cold,
    transport_freezer,
    transport_dry,
    min_time_arrival,
    max_time_arrival,
    tags,
    exclude_user=True,
):

    trip_results = TripOffer.objects.filter(active=True).order_by("time_depart")
    if not user.is_anonymous and exclude_user:
        trip_results = trip_results.exclude(driver=user)
    if min_time_arrival:
        trip_results = trip_results.filter(time_depart__gte=min_time_arrival)
    if max_time_arrival:
        trip_results = trip_results.filter(time_depart__lte=max_time_arrival)
    if transport_isothermic:
        trip_results = trip_results.filter(Q(transport_cold=True) | Q(transport_isothermic=True))
    if transport_cold:
        trip_results = trip_results.filter(transport_cold=True)
    if transport_freezer:
        trip_results = trip_results.filter(transport_freezer=True)
    if transport_dry:
        trip_results = trip_results.filter(transport_dry=True)

    if origin[0] not in ["null", "undefined"]:
        point_origin = Point([float(origin[0]), float(origin[1])], srid=4326).transform(
            3875, clone=True
        )
        trip_results = trip_results.annotate(
            distance_origin=Distance("route", point_origin)
        )
    else:
        point_origin = None

    if destination[0] not in ["null", "undefined"]:
        point_destination = Point(
            [float(destination[0]), float(destination[1])], srid=4326
        ).transform(3875, clone=True)
        trip_results = trip_results.annotate(
            distance_destination=Distance("route", point_destination)
        )
    else:
        point_destination = None

    if tags:
        trip_results = trip_results.filter(tags__slug__in=tags)

    if point_origin and point_destination:
        trip_oneway_origin = (
            trip_results.annotate(
                distance_origin_destination=Distance(
                    "origin_location", point_destination
                )
            )
            .annotate(distance_origin_origin=Distance("origin_location", point_origin))
            .exclude(distance_origin_origin__gt=F("distance_origin_destination"))
        )
        trip_oneway_destination = (
            trip_results.annotate(
                distance_destination_origin=Distance(
                    "destination_location", point_origin
                )
            )
            .annotate(
                distance_destination_destination=Distance(
                    "destination_location", point_destination
                )
            )
            .exclude(
                distance_destination_destination__gt=F("distance_destination_origin")
            )
        )

        trip_results = trip_oneway_origin | trip_oneway_destination

    # for offer in trip_results:
    #     print(offer.id, offer.origin_name)
    #     if hasattr(offer, "distance_origin"):
    #         print("km origin:", offer.distance_origin)
    #     if hasattr(offer, "distance_destination"):
    #         print("km_destination:", offer.distance_destination)
    #     if hasattr(offer, "distance_origin_origin"):
    #         print("distance_origin_origin:", offer.distance_origin_origin)
    #     if hasattr(offer, "distance_origin_destination"):
    #         print("distance_origin_destination:", offer.distance_origin_destination)

    if point_origin:
        trip_results = trip_results.filter(
            distance_origin__lte=F("max_detour_km") * 1000
        ).distinct()

    if point_destination:
        trip_results = trip_results.filter(
            distance_destination__lte=F("max_detour_km") * 1000
        ).distinct()

    return trip_results.select_related("driver")


class TripOffer(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    driver = models.ForeignKey(
        User,
        verbose_name=_("Driver"),
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    internal_name = models.CharField(
        _("Nom Intern"), max_length=200, null=True, blank=True
    )
    origin_location = gis_models.PointField(
        _("Origin Location"),
        null=False,
        blank=False,
        geography=True,
        srid=4326,
    )
    origin_name = models.CharField(_("Origen"), max_length=300, null=True, blank=True)
    step_location_1 = gis_models.PointField(
        _("Stepping 1 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_1 = models.CharField(
        _("Stepping 1 Name"), max_length=300, null=True, blank=True
    )
    step_location_2 = gis_models.PointField(
        _("Stepping 2 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_2 = models.CharField(
        _("Stepping 2 Name"), max_length=300, null=True, blank=True
    )

    step_location_2 = gis_models.PointField(
        _("Stepping 2 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_2 = models.CharField(
        _("Stepping 2 Name"), max_length=300, null=True, blank=True
    )

    step_location_3 = gis_models.PointField(
        _("Stepping 3 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_3 = models.CharField(
        _("Stepping 3 Name"), max_length=300, null=True, blank=True
    )

    step_location_4 = gis_models.PointField(
        _("Stepping 4 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_4 = models.CharField(
        _("Stepping 4 Name"), max_length=300, null=True, blank=True
    )

    step_location_5 = gis_models.PointField(
        _("Stepping 5 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_5 = models.CharField(
        _("Stepping 5 Name"), max_length=300, null=True, blank=True
    )

    step_location_6 = gis_models.PointField(
        _("Stepping 6 Location"), null=True, blank=True, geography=True, srid=4326
    )
    step_name_6 = models.CharField(
        _("Stepping 6 Name"), max_length=300, null=True, blank=True
    )
    destination_location = gis_models.PointField(
        _("Destination Location"), null=False, blank=False, geography=True, srid=4326
    )
    destination_name = models.CharField(
        _("Destinació"), max_length=300, null=True, blank=True
    )
    route = gis_models.LineStringField(_("Ruta calculada"), null=True, blank=True)
    time_depart = models.DateTimeField(_("Hora de sortida"))
    time_arrival = models.DateTimeField(_("Hora d'arribada"))
    comment = models.CharField(_("Comentaris"), max_length=2000, null=True, blank=True)
    # This will create during X months a copy of the object to be found in the app when searches.
    frequency = models.CharField(
        _("Freqüència de la ruta"),
        max_length=15,
        choices=FrequencyType.choices,
        default=FrequencyType.ONCE,
    )
    frequency_group_id = models.IntegerField(
        _("Id del grup de rutes freqüents"), null=True, blank=True
    )
    tags = TaggableManager(
        verbose_name=_("Tags"),
        help_text=_("Vehicle conditions or Hashtags"),
        blank=True,
    )
    # Nom projecte/empresa/entitat
    vehicle = models.CharField(
        _("Model de Vehicle/Furgoneta"),
        max_length=30,
        help_text=_("Ex. Berlingo, Peugeot Traveller, etc."),
        null=True,
        blank=True,
    )
    # Cost in euros per km
    cost_km = models.FloatField(
        _("Cost en € per cada KM recorregut"),
        null=True,
        blank=True,
    )
    max_detour_km = models.FloatField(
        _("Màxim desviament de la ruta (km)"),
        default=10,
        null=True,
        blank=True,
        validators=[MinValueValidator(1)],
        help_text="Quants km estic disposat/da a desviar-me de la meva ruta per recollir/entregar comandes d'altres.",
    )
    available_seats = models.PositiveIntegerField(
        _("Seients disponibles per compartir"),
        help_text=_(
            "Quants seients tinc disponibles i ofereixo perquè algú pugui venir (sigui per compartir una part del trajecte o la ruta sencera; segons encaixin les comandes)."
        ),
        default=0,
        blank=True,
        null=True,
    )
    available_space = models.CharField(
        _("Espai disponible al vehicle"), max_length=1000, null=True, blank=True
    )

    # a cron job will deactive the past date offers and send an email to rate the trip if any match
    active = models.BooleanField("Activa", default=True)
    transport_isothermic = models.BooleanField("Vehicle isoterm", null=True, blank=True)
    transport_cold = models.BooleanField("Vehicle refrigerat", null=True, blank=True)
    transport_freezer = models.BooleanField("Vehicle congelat", null=True, blank=True)
    transport_dry = models.BooleanField("Vehicle sense humitat", null=True, blank=True)
    telegram_published = models.DateTimeField(
        _("Date published in Telegram"), null=True, blank=True
    )
    origin_logisticnode = models.ForeignKey(LogisticNode,
        verbose_name=_("Logistic Node Origin"),
        related_name="tripoffer_as_origin",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    destination_logisticnode = models.ForeignKey(LogisticNode,
        verbose_name=_("Logistic Node Destination"),
        related_name="tripoffer_as_destination",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    origin_last_mile_location = gis_models.PointField(
        _("Origin Location of Logistic Node"), null=True, blank=True, geography=True, srid=4326
    )
    origin_last_mile_name = models.CharField(
        _("Punt de sortida Node Logístic"), max_length=300, null=True, blank=True
    )
    destination_last_mile_location = gis_models.PointField(
        _("Destination Location of Logistic Node"), null=True, blank=True, geography=True, srid=4326
    )
    destination_last_mile_name = models.CharField(
        _("Punt d'arribada Node Logístic"), max_length=300, null=True, blank=True
    )

    @classmethod
    def get_highest_frequency_group_id(cls):
        highest_group = cls.objects.aggregate(Max("frequency_group_id"))[
            "frequency_group_id__max"
        ]
        if highest_group:
            return highest_group
        else:
            return 0

    def get_frequency_name(self):
        for choice in FrequencyType.choices:
            if self.frequency == choice[0]:
                return choice[1]
        return "Unknown"

    def conditions(self):
        string = ""
        string += "I-" if self.transport_isothermic else "-"
        string += "R-" if self.transport_cold else "-"
        string += "C-" if self.transport_freezer else "-"
        string += "SH" if self.transport_dry else "-"
        return string

    def steps_name(self):
        steps_html = ""
        if self.step_name_1:
            steps_html += " ⟹ {}".format(_filter_town(self.step_name_1))
        if self.step_name_2:
            steps_html += " ⟹ {}".format(_filter_town(self.step_name_2))
        if self.step_name_3:
            steps_html += " ⟹ {}".format(_filter_town(self.step_name_3))
        if self.step_name_4:
            steps_html += " ⟹ {}".format(_filter_town(self.step_name_4))
        if self.step_name_5:
            steps_html += " ⟹ {}".format(_filter_town(self.step_name_5))
        if self.step_name_6:
            steps_html += " ⟹ {}".format(_filter_town(self.step_name_6))
        return steps_html

    def _get_coords(self):
        coords = [self.origin_location]
        for i in range(1, 7):
            attr_name = "step_location_" + str(i)
            if hasattr(self, attr_name) and getattr(self, attr_name):
                coords.append(getattr(self, attr_name))
        coords.append(self.destination_location)
        return coords

    def calculate_route(self):
        ln = None
        coords = self._get_coords()
        ln = gis_utils.request_route(coords)["linestring"]
        if ln:
            self.route = ln
            self.save()
        return ln

    def search_trip_results(self, user, exclude_frozen=True, exclude_user=False):
        return search_trip_requests(
            user,
            self.route,
            self.origin_location,
            self.destination_location,
            self.transport_isothermic,
            self.transport_cold,
            self.transport_freezer,
            self.transport_dry,
            self.time_arrival,
            [],  # list(self.tags.names()),
            self.max_detour_km,
            exclude_frozen=exclude_frozen,
            exclude_user=exclude_user,
        )

    @property
    def too_late_to_edit(self):
        return self.has_pending_requests and (
            self.time_depart
            - timezone.timedelta(minutes=settings.CAN_EDIT_WITH_MATCHES_MINUTES_MAX)
            < timezone.localtime()
        )

    @property
    def has_pending_requests(self):
        return self.matches.filter(
            status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
        ).exists()

    @property
    def has_completed_requests(self):
        return self.matches.filter(
            status__in=[MatchStatus.COMPLETED, MatchStatus.REVIEW_FROZEN]
        ).exists()

    def notify_trip_requests(self):
        print(f"Notify all matching trip requests for trip_offer #{self.id}")
        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": self.id}
        )
        trip_requests = self.search_trip_results(user=self.driver, exclude_frozen=False)
        pending_requests_ids = self.matches.filter(
            status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
        ).values_list("request__id", flat=True)
        trip_requests = trip_requests.exclude(id__in=pending_requests_ids).all()
        print("Trip Requests to notify", trip_requests)
        for trip_request in trip_requests:
            trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                "triprequest-detail", kwargs={"request_id": trip_request.id}
            )
            subject = "Nova ruta que coincideix amb la teva comanda."
            msg_notif = f"Nova <a href='{offer_url}'>ruta</a> que coincideix amb la teva <a href='{trip_request_url}'>comanda</a>."
            msg_body = (
                f"Hi ha <a href='{offer_url}'>una ruta</a> que coincideix amb la <a href='{trip_request_url}'>la teva comanda</a>.<br>"
                f"<br>En entrar a la ruta, trobaràs els detalls de la ruta i més informació sobre el/la transportista."
            )
            send_notifications(
                trip_request.client, trip_request.client, msg_notif, subject, msg_body
            )

    def possible_logistic_nodes(self):
        return gis_utils.detect_possible_logistic_nodes(self)


    def finalize(self):
        print(f"Finalizing offer: {self.id}")
        self.active = False
        self.save()
        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": self.id}
        )
        # Deactivating needs matched with that offer also.
        for match in self.matches.all():
            trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                "triprequest-detail", kwargs={"request_id": match.request.id}
            )
            print(f"match: {match.id} , status: {match.status}")
            if match.status == MatchStatus.PENDING:
                match.status = MatchStatus.REJECTED
                match.request.frozen = False
                match.request.save()
                match.save()
                subject = "La ruta amb la que havies demanat enviar la teva comanda ha finalitzat sense resposta."
                msg_notif = f"La <a href='{offer_url}'>ruta</a> per a la <a href='{trip_request_url}'>teva comanda</a> ha finalitzat sense resposta"
                msg_body = f"La <a href='{offer_url}'>ruta</a> amb la que havies demanat enviar la <a href='{trip_request_url}'>teva comanda</a> ha finalitzat sense resposta"
                send_notifications(
                    match.offer.driver,
                    match.request.client,
                    msg_notif,
                    subject,
                    msg_body,
                )
            if match.status == MatchStatus.ACCEPTED:
                match.status = MatchStatus.COMPLETED
                match.max_time_rating = timezone.localtime() + timezone.timedelta(
                    minutes=settings.MINUTES_AFTER_REVIEW_FREEZES
                )
                match.save()
                match.request.active = False
                match.request.save()
                match_review_url = settings.DEFAULT_DOMAIN + reverse(
                    "tripmatch-review", kwargs={"match_id": match.id}
                )
                subject_email = "Valora la ruta finalitzada"
                msg_notif = f"La <a href='{offer_url}'>ruta</a> ha finalitzat. Ja pots <a href='{match_review_url}'>valorar a l'altra part</a>."
                msg_body = f"La <a href='{offer_url}'>ruta</a> ha finalitzat. Ja pots <a href='{match_review_url}'>valorar a l'altra part</a>."
                send_notifications(
                    match.offer.driver,
                    match.request.client,
                    msg_notif,
                    subject_email,
                    msg_body,
                )
                send_notifications(
                    match.request.client,
                    match.offer.driver,
                    msg_notif,
                    subject_email,
                    msg_body,
                )



@receiver(post_save, sender=TripOffer)
def create_trip_offer(sender, instance, created, **kwargs):
    print("Saving offer", instance)
    if created:
        print("new trip offer, generating route", instance)
        print("coords", instance._get_coords())
        instance.calculate_route()



class TripRequest(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    client = models.ForeignKey(
        User,
        verbose_name=_("Requesting User"),
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    internal_name = models.CharField(
        _("Nom Intern"), max_length=200, null=True, blank=True
    )
    origin_location = gis_models.PointField(
        _("Origin Location"), null=False, blank=False, geography=True, srid=4326
    )
    origin_name = models.CharField(
        _("Punt de sortida"), max_length=300, null=True, blank=True
    )
    origin_last_mile_location = gis_models.PointField(
        _("Origin Location of Logistic Node"), null=True, blank=True, geography=True, srid=4326
    )
    origin_last_mile_name = models.CharField(
        _("Punt de sortida Node Logístic"), max_length=300, null=True, blank=True
    )
    origin_logisticnode = models.ForeignKey(LogisticNode,
        verbose_name=_("Logistic Node Origin"),
        related_name="triprequest_as_origin",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    destination_logisticnode = models.ForeignKey(LogisticNode,
        verbose_name=_("Logistic Node Destination"),
        related_name="triprequest_as_destination",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    destination_last_mile_location = gis_models.PointField(
        _("Destination Location of Logistic Node"), null=True, blank=True, geography=True, srid=4326
    )
    destination_last_mile_name = models.CharField(
        _("Punt d'arribada Node Logístic"), max_length=300, null=True, blank=True
    )

    destination_location = gis_models.PointField(
        _("Destination Location"), null=False, blank=False, geography=True, srid=4326
    )
    destination_name = models.CharField(
        _("Punt d'arribada"), max_length=300, null=True, blank=True
    )




    min_time_arrival = models.DateTimeField(_("Minimum Depart Time"))
    max_time_arrival = models.DateTimeField(
        _("Maximum Depart Time"), null=True, blank=True
    )
    wants_carpool = models.BooleanField(
        "Vull ocupar un seient buit al vehicle", default=False
    )
    wants_delivery_notification = models.BooleanField(
        "Vull rebre una notificació quan s'hagi entregat la comanda.", default=False
    )
    # Fixed Trip conditions in UI will be shown as checkboxes
    tags = TaggableManager(
        verbose_name=_("Tags"),
        help_text=_("Requested conditions or Hashtags"),
        blank=True,
    )
    comment = models.CharField(_("Comentaris"), max_length=2000, null=True, blank=True)
    delivery_contact = models.CharField(
        _("Contacte de recollida i/o entrega:"),
        max_length=2000,
        null=True,
        blank=True,
        help_text="Aquesta informació només es compartirà amb qui farà el transport en el moment en què accepti dur la teva comanda.",
    )
    delivery_note = models.FileField(
        upload_to="delivery_notes",
        null=True,
        blank=True,
        validators=[FileExtensionValidator(["pdf"])],
    )
    telegram_published = models.DateTimeField(
        _("Date published in Telegram"), null=True, blank=True
    )

    # a cron job will deactive the past date requests
    active = models.BooleanField("Activa", default=True)
    # A request is frozen if it is taken.
    frozen = models.BooleanField("Ocupada", default=False)
    transport_isothermic = models.BooleanField("Vehicle isoterm", null=True, blank=True)
    transport_cold = models.BooleanField("Vehicle refrigerat", null=True, blank=True)
    transport_freezer = models.BooleanField("Vehicle congelat", null=True, blank=True)
    transport_dry = models.BooleanField("Vehicle sense humitat", null=True, blank=True)

    packages_num = models.PositiveIntegerField(null=True, blank=False, default=1)
    packages_length = models.FloatField(null=True, blank=False, default=1)
    packages_width = models.FloatField(null=True, blank=False, default=1)
    packages_height = models.FloatField(null=True, blank=False, default=1)
    packages_weight = models.FloatField(null=True, blank=False, default=1)
    packages_fragile = models.BooleanField("És fràgil", default=False)
    route = gis_models.LineStringField(_("Ruta calculada"), null=True, blank=True)
    distance = models.FloatField(
        _("Distància en metres"), null=True, blank=True, default=0
    )
    co2_saved = models.FloatField(_("Co2 estalviat"), null=True, blank=True, default=0)
    

    def calculate_route(self):
        route_output = gis_utils.request_route(
            [self.origin_location, self.destination_location]
        )
        if route_output:
            self.route = route_output["linestring"]
            self.distance = route_output["distance"]
            self.co2_saved = gis_utils.calculate_co2(self.distance)
            print(f"La distància de la ruta calculada es de {self.distance}m")
            self.save()
        return route_output

    def possible_logistic_nodes(self):
        nodes = LogisticNode.objects.filter(active=True, deleted=False)
        nodes_results = []
        
        for node in nodes:
            print('DB LogisticNode', node)
            if node.delivery_areas:
                geojson_files = node.delivery_areas.split(",")
                print('GeoJSON Files', geojson_files)
                #TODO Loop with all the logistic nodes and their geojson zones
                result = [node]
                if self.origin_logisticnode: #if triprequest has chosen already an logistic node for origin
                    origin_location = None
                else:
                    origin_location = self.origin_location

                if self.destination_logisticnode: #if triprequest has chosen already an logistic node for origin
                    destination_location = None
                else:
                    destination_location = self.destination_location
                
                result.append(gis_utils.check_coordinates_inside_geojson_files(geojson_files, origin_location, destination_location))
                nodes_results.append(result)
        return nodes_results

    @property
    def get_co2_saved(self):
        # https://www.sunearthtools.com/tools/CO2-emissions-calculator.php 1km = 0.15kg Co2
        if not self.co2_saved and self.distance:
            self.co2_saved = gis_utils.calculate_co2(self.distance)

        return self.co2_saved

    @property
    def confirmed_drivers(self):
        pending_drivers_ids = self.matches.filter(
            status__in=[
                MatchStatus.PENDING,
                MatchStatus.ACCEPTED,
                MatchStatus.COMPLETED,
                MatchStatus.REVIEW_FROZEN,
            ]
        ).values_list("offer__driver", flat=True)
        print("pending_drivers_ids", pending_drivers_ids)
        return pending_drivers_ids

    @property
    def distance_km(self):
        if self.distance:
            return self.distance / 1000
        else:
            return 0

    def get_money_saved(self):
        amount = 0
        if self.distance:
            amount += (
                settings.AMOUNT_MONEY_FIRST_KM
                + (self.distance / 1000) * settings.AMOUNT_MONEY_EXTRA_KM
            )
        if self.transport_cold or self.transport_freezer:
            amount += settings.AMOUNT_MONEY_FREEZER_OR_COLD

        if self.packages_weight:
            amount += (
                int(self.packages_weight / 25) * settings.AMOUNT_MONEY_PER_EXTRA_25KG
            )

        return amount

    def conditions(self):
        string = ""
        string += "I-" if self.transport_isothermic else "-"
        string += "R-" if self.transport_cold else "-"
        string += "C-" if self.transport_freezer else "-"
        string += "SH" if self.transport_dry else "-"
        return string

    def search_trip_results(self, user):
        print("user is", user)
        return search_trip_offers(
            user,
            self.origin_location,
            self.destination_location,
            self.transport_isothermic,
            self.transport_cold,
            self.transport_freezer,
            self.transport_dry,
            self.min_time_arrival,
            self.max_time_arrival,
            [],  # list(self.tags.names()),
        )

    def notify_offers(self, action="new"):
        print(f"Notify all matching offers for trip_request #{self.id}")
        trip_request_url = settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": self.id}
        )
        trip_offers = self.search_trip_results(user=self.client)
        pending_offer = self.matches.filter(
            status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
        ).values_list("offer__id", "offer__frequency_group_id")
        frequency_group_ids = []
        pending_offers_ids = []

        for offer_id, frequency_group_id in pending_offer:
            pending_offers_ids.append(offer_id)
            if frequency_group_id:
                frequency_group_ids.append(frequency_group_id)
        trip_offers = (
            trip_offers.exclude(id__in=pending_offers_ids).order_by("id").all()
        )

        # Show only one notification for regular TripOffers, the smallest id(the nearest one in time)
        for trip_offer in trip_offers:
            if trip_offer.frequency_group_id not in frequency_group_ids:
                print("Offer to notify", trip_offer)
                frequency_group_ids.append(trip_offer.frequency_group_id)

                offer_url = settings.DEFAULT_DOMAIN + reverse(
                    "tripoffer-detail", kwargs={"offer_id": trip_offer.id}
                )
                if action == "update":
                    action_msg = "Actualitzada"
                else:
                    action_msg = "Nova"
                subject = f"{action_msg} comanda que coincideix amb la teva ruta."
                msg_notif = f"{action_msg} <a href='{trip_request_url}'>comanda</a> que coincideix amb <a href='{offer_url}'>la teva ruta</a>."
                msg_body = (
                    f"{action_msg} <a href='{trip_request_url}'>una comanda</a> que coincideix amb <a href='{offer_url}'>la teva ruta</a>."
                    f"<br>En entrar a la comanda, trobaràs els detalls de la comanda i més informació sobre l'usuari/a que vol enviar la comanda."
                )
                send_notifications(
                    trip_offer.driver, trip_offer.driver, msg_notif, subject, msg_body
                )

    @property
    def url(self):
        return settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": self.id}
        )

    @property
    def has_pending_requests(self):
        return self.matches.filter(
            status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
        ).exists()

    @property
    def is_review_frozen_or_completed(self):
        return self.matches.filter(
            status__in=[MatchStatus.COMPLETED, MatchStatus.REVIEW_FROZEN]
        ).exists()

    @property
    def accepted_offer(self):
        match = self.matches.filter(
            status__in=[
                MatchStatus.ACCEPTED,
                MatchStatus.REVIEW_FROZEN,
                MatchStatus.COMPLETED,
            ]
        ).first()
        if match:
            return match.offer
        else:
            return None

    def finalize(self):
        print(f"Finalizing trip_request: {self.id}")
        self.active = False
        self.save()
        # Deactivating needs matched with that offer also.
        for match in self.matches.all():
            print(f"match: {match.id} , status: {match.status}")
            if match.status in [
                MatchStatus.ACCEPTED,
                MatchStatus.COMPLETED,
                MatchStatus.REVIEW_FROZEN,
            ]:
                print(
                    f"Not canceling trip request {self.id} since it has been accepted and has future route."
                )
                return
            if match.status == MatchStatus.PENDING:
                match.status = MatchStatus.CANCELED
                match.save()
        subject = "La teva comanda ha expirat sense trobar cap ruta."
        msg_body = f"La <a href='{self.url}'>teva comanda</a> ha expirat sense trobar cap ruta."
        send_notifications(self.client, self.client, msg_body, subject, msg_body)


class Storage(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(
        User,
        verbose_name=_("Autor/a"),
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    name = models.CharField(_("Nom"), max_length=200, null=True, blank=True)
    origin_location = gis_models.PointField(
        _("Origin Location"), null=False, blank=False, geography=True, srid=4326
    )
    origin_name = models.CharField(
        _("Localització"), max_length=300, null=True, blank=True
    )

    # Fixed Trip conditions in UI will be shown as checkboxes
    tags = TaggableManager(
        verbose_name=_("Tags"),
        help_text=_("Requested conditions or Hashtags"),
        blank=True,
    )
    notes = CharFieldWithTextarea(
        _("Dades de contacte / informació"),
        max_length=500,
        null=True,
        blank=True,
        help_text="Condicions, contacte, horaris, etc.",
    )
    # a cron job will deactive the past date requests
    deleted = models.BooleanField("Esborrat", default=False)
    active = models.BooleanField("Actiu", default=True)
    # An storage is not private(public)
    is_public = models.BooleanField(
        "Punt habitual públic",
        default=False,
        help_text="Els punts habituals publics apareixeran disponibles per a poder utilitzar-se amb altres usuari/es per a carrega o descarrega de transports.",
    )
    transport_isothermic = models.BooleanField(
        "Apte per a isoterm", null=True, blank=True
    )
    transport_cold = models.BooleanField("Apte per a refrigerat", null=True, blank=True)
    transport_freezer = models.BooleanField(
        "Apte per a congelat", null=True, blank=True
    )
    transport_dry = models.BooleanField("Apte per sense humitat", null=True, blank=True)
    accepts_pallets = models.BooleanField(
        "Disposa de toro per a la descàrrega de palets", null=True, blank=True
    )




class MatchStatus(models.TextChoices):
    PENDING = "PENDING", _("Pendent")
    ACCEPTED = "ACCEPTED", _("Acceptada")
    REJECTED = "REJECTED", _("Declinada")
    CANCELED = "CANCELED", _("Cancel·lada")
    COMPLETED = "COMPLETED", _("Finalitzada")
    REVIEW_FROZEN = "REVIEW_FROZEN", _("Valoració congelada")


class TripMatch(TimeStampedModel, models.Model):
    status = models.CharField(
        _("Match Status"),
        max_length=15,
        choices=MatchStatus.choices,
        default=MatchStatus.PENDING,
    )
    request = models.ForeignKey(
        "TripRequest", on_delete=models.CASCADE, related_name="matches"
    )
    offer = models.ForeignKey(
        "TripOffer", on_delete=models.CASCADE, related_name="matches"
    )
    driver_comment = models.CharField(
        _("Comment from Driver"), max_length=2000, null=True, blank=True
    )
    driver_rating = models.PositiveIntegerField(
        "Rating from Driver",
        blank=True,
        null=True,
    )
    driver_rating_date = models.DateTimeField(
        "Data valoració del transportista", null=True, blank=True
    )
    client_comment = models.CharField(
        _("Comment from Client"), max_length=2000, null=True, blank=True
    )
    client_rating = models.PositiveIntegerField(
        "Rating from Client",
        blank=True,
        null=True,
    )
    client_rating_date = models.DateTimeField(
        "Data valoració del client", null=True, blank=True
    )
    max_time_rating = models.DateTimeField(
        "Temps de valoració màxim", null=True, blank=True
    )
    times_reminded_rate = models.PositiveIntegerField(
        "Vegades recordat per a valorar", blank=True, null=True, default=0
    )
    client_failed = models.BooleanField(
        _("Transportista afirma que NO li van entregar la comanda"),
        null=True,
        blank=True,
        default=False,
    )
    driver_failed = models.BooleanField(
        _("Usuaria afirma que NO li van recollir la comanda"),
        null=True,
        blank=True,
        default=False,
    )
    delivery_confirmed = models.BooleanField(
        _("Entrega confirmada pel transportista"),
        null=True,
        blank=True,
        default=False,
    )
    delivery_photo = models.FileField(
        upload_to="delivery_photo",
        null=True,
        blank=True,
        validators=[FileExtensionValidator(["jpg", "jpeg", "png", "tiff", "pdf"])],
    )
    delivery_signature = models.FileField(
        upload_to="delivery_signature",
        null=True,
        blank=True,
        validators=[FileExtensionValidator(["jpg", "jpeg", "png", "tiff", "pdf"])],
    )
    delivery_date = models.DateTimeField(
        "Data de confirmació de l'entrega", null=True, blank=True
    )
    delivery_comment = models.CharField(
        _("Comment when delivered by driver"), max_length=2000, null=True, blank=True
    )

    def send_review_reminder(self):
        self.times_reminded_rate = 1
        self.save()
        trip_request_url = settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": self.request.id}
        )
        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": self.offer.id}
        )
        match_review_url = settings.DEFAULT_DOMAIN + reverse(
            "tripmatch-review", kwargs={"match_id": self.id}
        )
        if not self.driver_comment:
            client = self.request.client
            reviewer_url = settings.DEFAULT_DOMAIN + reverse(
                "profile-view", kwargs={"username": client}
            )
            msg = f"Recorda valorar el transport de la comanda de {client}"
            msg_notif = f"Recorda <a href='{match_review_url}'>valorar</a> la <a href='{trip_request_url}'>petició de transport</a> de <a href='{reviewer_url}'>{client}</a>."
            msg_details = (
                f"Recorda <a href='{match_review_url}'>valorar</a> la <a href='{trip_request_url}'>petició de transport</a> de <a href='{reviewer_url}'>{client}</a>."
                f"<br>Data màxima: {self.max_time_rating}"
            )

            send_notifications(
                self.offer.driver, self.offer.driver, msg_notif, msg, msg_details
            )

        if not self.client_comment:
            driver = self.offer.driver
            reviewer_url = settings.DEFAULT_DOMAIN + reverse(
                "profile-view", kwargs={"username": driver}
            )
            msg = f"Recorda valorar la ruta de {driver}"
            msg_notif = f"Recorda <a href='{match_review_url}'>valorar</a> la <a href='{offer_url}'>ruta</a> de <a href='{reviewer_url}'>{driver}</a>."
            msg_details = (
                f"Recorda <a href='{match_review_url}'>valorar</a> la <a href='{offer_url}'>ruta</a> de <a href='{reviewer_url}'>{driver}</a>."
                f"<br>Data màxima: {self.max_time_rating}"
            )

            send_notifications(
                self.request.client, self.request.client, msg_notif, msg, msg_details
            )

    def freeze_reviews(self):
        self.status = MatchStatus.REVIEW_FROZEN
        self.save()
        trip_request_url = settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": self.request.id}
        )
        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": self.offer.id}
        )
        username_url = settings.DEFAULT_DOMAIN + reverse(
            "profile-view", kwargs={"username": "me"}
        )

        if self.driver_comment:
            driver = self.offer.driver
            reviewer_url = settings.DEFAULT_DOMAIN + reverse(
                "profile-view", kwargs={"username": driver}
            )
            msg = f"{driver} ha puntuat la teva petició de transport ({self.driver_rating}/5)"
            msg_notif = f"<a href='{reviewer_url}'>{driver}</a> ha <a href='{username_url}'>puntuat</a> la <a href='{trip_request_url}'>teva petició de transport</a>."
            msg_details = (
                f"El/la <a href='{username_url}'>transportista ({driver})</a> ha puntuat la <a href='{trip_request_url}'>teva petició de transport</a>:"
                f"<br><br>Puntuació: {self.driver_rating}/5<br> Comentari: {self.driver_comment}"
            )
            self.request.client.userprofile.avg_rating = (
                self.request.client.userprofile.rating["avg_received"]
            )
            self.request.client.userprofile.save()
            send_notifications(driver, self.request.client, msg_notif, msg, msg_details)

        if self.client_comment:
            client = self.request.client
            reviewer_url = settings.DEFAULT_DOMAIN + reverse(
                "profile-view", kwargs={"username": client}
            )
            msg = f"{client} ha puntuat la teva ruta ({self.driver_rating}/5)"
            msg_notif = f"<a href='{reviewer_url}'>{client}</a> ha <a href='{username_url}'>puntuat</a> la <a href='{offer_url}'>teva ruta</a>."
            msg_details = (
                f"El/la <a href='{username_url}'>usuari/a ({client})</a> ha puntuat la <a href='{offer_url}'>teva ruta</a>:"
                f"<br><br>Puntuació: {self.driver_rating}/5<br> Comentari: {self.client_comment}"
            )
            self.offer.driver.userprofile.avg_rating = (
                self.request.client.userprofile.rating["avg_received"]
            )
            self.offer.driver.userprofile.save()
            send_notifications(client, self.offer.driver, msg_notif, msg, msg_details)


class UserProfile(models.Model):

    user = models.OneToOneField(
        User,
        primary_key=True,
        on_delete=models.CASCADE,
    )
    language = models.CharField(
        _("Idioma de la app"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.DEFAULT_LANGUAGE,
    )
    notes = CharFieldWithTextarea(
        _("Notes públiques"), max_length=500, null=True, blank=True
    )
    picture = models.ImageField(
        upload_to="user_profiles",
        null=True,
        blank=True,
        verbose_name=_("Image de perfil"),
        help_text=_("Minimum dimension: 200 x 100px."),
        default="user_profiles/default.png",
    )
    points = models.PositiveIntegerField(_("Punts"), null=True, blank=True, default=0)
    avg_rating = models.FloatField(null=True, blank=True)
    # Nom projecte/empresa/entitat
    organization = models.CharField(
        _("Organització"), max_length=300, null=True, blank=True
    )
    phone = models.CharField(_("Telèfon"), max_length=15, null=True, blank=True)
    # Xarxes socials i Opció Whattsap/Telegram/Signal/Email/FB
    extra = models.JSONField(_("Informació adicional"), null=True, blank=True)
    newsletter = models.BooleanField(
        _("Newsletter"),
        null=False,
        blank=True,
        default=False,
        help_text="Accepto que es tractin les meves dades per a rebre novetats sobre aquesta web-app, sobre logística i sobre agroecologia",
    )
    stop_email_notifications = models.BooleanField(
        _("Deixar de rebre notificacions de la app via email"),
        null=False,
        blank=False,
        default=False,
    )
    # consent_date = models.BooleanField(
    #     _("Accepte el consentiment dades"),
    #     null=True,
    #     blank=False,
    #     default=False,
    #     help_text=_(
    #         "Acceptes que el teu perfil es puga vore en la web (només a qui hagi iniciat sessió) i està d'acord amb la política de privacitat de dades."
    #     ),
    # )

    class Meta:
        verbose_name = _("User profile")
        verbose_name_plural = _("User profiles")
        ordering = ["user"]

    def __str__(self):
        if self.avg_rating is not None:
            return f"{self.user.username}({('%f' % self.avg_rating).rstrip('0').rstrip('.')}/{5})"
        else:
            return f"{self.user.username}"

    @property
    def contact_details(self):
        return f"{self.user.first_name} {self.user.last_name} Tlf: {self.phone}"

    @property
    def co2_saved(self):
        total_co2_saved = 0
        user_offers_ids = self.user.tripoffer_set.values_list("id", flat=True)
        print("user_offers_ids", user_offers_ids)
        driver_matches = TripMatch.objects.filter(
            offer__in=user_offers_ids,
            status=MatchStatus.REVIEW_FROZEN,
            driver_failed=False,
            client_failed=False,
        )
        for match in driver_matches:
            print("match.request completat", match.id)
            if match.request.co2_saved is not None:
                total_co2_saved += match.request.co2_saved
                print("Sumant total_co2_saved", match.request.co2_saved)
        return total_co2_saved

    @property
    def rating(self):
        user_requests_ids = self.user.triprequest_set.filter(
            active=False, frozen=True
        ).values_list("id", flat=True)
        user_offers_ids = self.user.tripoffer_set.filter(active=False).values_list(
            "id", flat=True
        )

        driver_ratings = TripMatch.objects.filter(
            request__in=user_requests_ids, status=MatchStatus.REVIEW_FROZEN
        ).order_by("-driver_rating_date")
        client_ratings = TripMatch.objects.filter(
            offer__in=user_offers_ids, status=MatchStatus.REVIEW_FROZEN
        ).order_by("-client_rating_date")
        all_points_received = []
        all_reviews_received = []
        all_reviews_sent = []
        all_points_sent = []
        for rating in driver_ratings:
            if rating.driver_rating is not None:
                if not rating.driver_rating_date:
                    rating.driver_rating_date = rating.offer.time_arrival
                    rating.save()
                all_points_received.append(rating.driver_rating)
                all_reviews_received.append(
                    {
                        "reviewer": rating.offer.driver,
                        "points": rating.driver_rating,
                        "comment": rating.driver_comment,
                        "date": rating.driver_rating_date,
                    }
                )
            if rating.client_rating is not None:
                if not rating.client_rating_date:
                    rating.client_rating_date = rating.offer.time_arrival
                    rating.save()
                all_points_sent.append(rating.client_rating)
                all_reviews_sent.append(
                    {
                        "reviewer": rating.offer.driver,
                        "points": rating.client_rating,
                        "comment": rating.client_comment,
                        "date": rating.client_rating_date,
                    }
                )

        for rating in client_ratings:
            if rating.driver_rating is not None:
                if not rating.driver_rating_date:
                    rating.driver_rating_date = rating.offer.time_arrival
                    rating.save()
                all_points_sent.append(rating.driver_rating)
                all_reviews_sent.append(
                    {
                        "reviewer": rating.request.client,
                        "points": rating.driver_rating,
                        "comment": rating.driver_comment,
                        "date": rating.driver_rating_date,
                    }
                )
            if rating.client_rating is not None:
                if not rating.client_rating_date:
                    rating.client_rating_date = rating.offer.time_arrival
                    rating.save()
                all_points_received.append(rating.client_rating)
                all_reviews_received.append(
                    {
                        "reviewer": rating.request.client,
                        "points": rating.client_rating,
                        "comment": rating.client_comment,
                        "date": rating.client_rating_date,
                    }
                )

        avg_received = 0
        if all_points_received:
            avg_received = sum(all_points_received) / len(all_points_received)
        avg_sent = 0
        if all_points_sent:
            avg_sent = sum(all_points_sent) / len(all_points_sent)
        result = {
            "total_received": len(all_points_received),
            "avg_received": round(avg_received, 1),
            "reviews_received": sorted(
                all_reviews_received, key=itemgetter("date"), reverse=True
            ),
            "total_sent": len(all_points_sent),
            "avg_sent": round(avg_sent, 1),
            "reviews_sent": sorted(
                all_reviews_sent, key=itemgetter("date"), reverse=True
            ),
        }
        return result

    @property
    def show_rating(self):
        user_rating = self.rating()
        print(self.username, "user_rating", user_rating)
        if user_rating["total"]:
            return f"({user_rating['avg']}/5)"
        else:
            return ""

    def registration_reward(self):
        reward = settings.POINTS_REGISTRATION
        pay(recipient=self.user, amount=reward, msg_notif="Registre")


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        print("create_user_profile", instance)

        profile = UserProfile.objects.create(user=instance)
        instance.userprofile = profile
        instance.save()
        profile.registration_reward()


class Page(models.Model):
    url = models.CharField(
        "URL",
        help_text="Es trobarà com a https://elteudomini.com/page/[URL]",
        max_length=50,
        null=False,
        blank=False,
    )

    content = CharFieldWithBigTextarea(
        "Contingut HTML",
        help_text="Compatible amb jquery 3.5, FontAwesome 5.2 i Bootstrap 4.5",
        null=False,
        blank=False,
    )

    class Meta:
        verbose_name = "Pàgina"
        verbose_name_plural = "Pàgines"
        ordering = ["url"]

    def __str__(self):
        return self.url
