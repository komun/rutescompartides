from leaflet.admin import LeafletGeoAdminMixin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse, path
from django.utils.html import format_html

from .models import TripOffer, TripRequest, TripMatch, UserProfile, Page, Storage, LogisticNode


class UserProfileInline(admin.StackedInline):
    model = UserProfile


@admin.action(description="Recalcular mitjana de valoracions")
def update_ratings(UserWithProfileAdmin, request, queryset):
    for i in queryset:
        rating = i.userprofile.rating
        if rating["total_received"]:
            avg_received = rating["avg_received"]
            print(f"Saving Average Recived for {i.username} to {avg_received}")
            i.userprofile.avg_rating = avg_received
            i.userprofile.save()


class UserWithProfileAdmin(UserAdmin):
    inlines = [UserProfileInline]
    actions = [update_ratings]
    list_display = (
        "userprofile",
        "email",
        "date_joined",
        "get_newsletter",
        "last_login",
    )
    list_filter = ("is_superuser", "is_staff")

    def get_newsletter(self, obj):
        return obj.userprofile.newsletter

    get_newsletter.admin_order_field = "userprofile__newsletter"
    get_newsletter.short_description = "Newsletter"


admin.site.unregister(User)
admin.site.register(User, UserWithProfileAdmin)


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ("url",)


class TripMatchInline(admin.TabularInline):
    model = TripMatch
    verbose_name = "Trip Match"
    verbose_name_plural = "Trip Matches"
    extra = 1


# class PackageInline(admin.TabularInline):
#     model = Package
#     verbose_name = u"Package"
#     verbose_name_plural = u"Packages"
#     extra = 1


@admin.register(TripOffer)
class TripOfferAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_display = (
        "id",
        "created_at",
        "driver",
        "frequency",
        "origin_name",
        "destination_name",
        "time_depart",
        "time_arrival",
        "conditions",
        "active",
    )
    list_filter = ("active",)
    inlines = [
        TripMatchInline,
    ]
    readonly_fields = ["custom_actions"]

    def get_conditions(self):
        return self.conditions

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "<int:offer_id>/update_route/",
                self.admin_site.admin_view(self.calculate_route),
                name="update-offer-route",
            ),
            path(
                "<int:offer_id>/finish_route/",
                self.admin_site.admin_view(self.finish_route),
                name="update-finish-route",
            ),
        ]

        return custom_urls + urls

    def calculate_route(self, request, offer_id):
        offer = TripOffer.objects.get(id=offer_id)
        offer.calculate_route()
        return HttpResponseRedirect(
            reverse("admin:core_tripoffer_change", args=(offer_id,))
        )

    def finish_route(self, request, offer_id):
        offer = TripOffer.objects.get(id=offer_id)
        offer.finalize()
        return HttpResponseRedirect(
            reverse("admin:core_tripoffer_change", args=(offer_id,))
        )

    def custom_actions(self, obj):
        if obj.id:
            return format_html(
                ' <a class="button" href="{}">{}</a> - <a class="button" href="{}">{}</a>'.format(
                    reverse("admin:update-offer-route", args=[obj.id]),
                    _("Recalcular ruta"),
                    reverse("admin:update-finish-route", args=[obj.id]),
                    _("Finalitzar ruta"),
                )
            )

    custom_actions.short_description = _("Accions")


@admin.register(TripRequest)
class TripRequestAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_display = (
        "id",
        "created_at",
        "client",
        "origin_name",
        "destination_name",
        "min_time_arrival",
        "max_time_arrival",
        "conditions",
        "active",
        "frozen",
    )
    list_filter = ("active", "frozen")
    inlines = [
        TripMatchInline,
    ]

    def get_conditions(self):
        return self.conditions


@admin.register(TripMatch)
class TripMatchAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_display = ("id", "offer", "request", "status", "max_time_rating")
    verbose_name = "Trip Match"
    verbose_name_plural = "Trip Matches"
    list_filter = ("status",)


@admin.register(Storage)
class StorageAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_display = (
        "id",
        "created_at",
        "creator",
        "origin_name",
        "is_public",
        "active",
        "deleted",
    )
    list_filter = ("active", "deleted")

@admin.register(LogisticNode)
class LogisticNodeAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_display = (
        "id",
        "email",
        "created_at",
        "creator",
        "origin_name",
        "active",
        "deleted",
    )
    list_filter = ("active", "deleted")
