from django.shortcuts import get_object_or_404
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from geojson import LineString, Point as GeoJsonPoint
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib import messages
from django.db.models import Q, Max
from django import template
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.shortcuts import render, reverse
from django.contrib.gis.geos import Point
from django.views.decorators.csrf import csrf_exempt
from django.utils.text import slugify

import os
import zipfile
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseBadRequest


from django.views.generic import CreateView, UpdateView, DetailView
from core.models import (
    LogisticNode
)
from core.forms import (
    LogisticNodeForm
)
from chat.views import get_unseen_messages_count

@login_required
def user_logisticnodes_view(request):
    user = request.user

    logisticnodes = user.logisticnode_set.filter(deleted=False).order_by("-updated_at")

    context = {
        "chat_unread_count": get_unseen_messages_count(request.user.pk),
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "logisticnodes": logisticnodes,
    }

    return render(request, "frontend/user_logisticnodes_view.html", context)

from django.shortcuts import redirect

@method_decorator([never_cache, login_required], name="dispatch")
class LogisticNodeUpdateView(UpdateView):
    model = LogisticNode
    form_class = LogisticNodeForm
    template_name = "frontend/logisticnode_add_form.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("logisticnode_id")
        return get_object_or_404(LogisticNode, id=object_id)

    def get_context_data(self, **kwargs):
        object_id = self.kwargs.get("logisticnode_id")
        object = LogisticNode.objects.get(id=object_id)

        ctx = super().get_context_data(**kwargs)
        ctx["origin_location"] = object.origin_location
        # ctx["origin_name"] = object.origin_name
        # ctx["transport_isothermic"] = object.transport_isothermic
        # ctx["transport_cold"] = object.transport_cold
        # ctx["transport_freezer"] = object.transport_freezer
        # ctx["transport_dry"] = object.transport_dry
        # ctx["notes"] = object.notes

        # if self.request.method == "GET":
        #     ctx["time_depart"] = self.request.GET.get("time_depart", None)

        # if self.request.method == "POST":
        #     ctx["is_public"] = self.request.POST.get("is_public", False)
        #ctx["delivery_areas_file"] = self.request.POST.get("delivery_areas")
        #ctx["delivery_areas"] = "barcelona_small.geojson"
        print("ctx", ctx)
        return ctx



    def form_valid(self, form):
        logisticnode = form.instance
        response = super().form_valid(form)
        delivery_areas_file = self.request.FILES.get('delivery_areas_file')
        
        if delivery_areas_file:
            filename = delivery_areas_file.name
            
            if filename.endswith('.zip'):
                # Guarda el archivo ZIP en la carpeta MEDIA
                fs = FileSystemStorage(location=settings.MEDIA_GEOJSON)
                zip_path = fs.save(delivery_areas_file.name, delivery_areas_file)
                zip_file = os.path.join(settings.MEDIA_GEOJSON, zip_path)

                # Extrae los archivos .geojson del archivo ZIP
                geojson_files = []
                with zipfile.ZipFile(zip_file, 'r') as zip_ref:
                    for file_info in zip_ref.infolist():
                        if file_info.filename.endswith('.geojson'):
                            name_without_extension = file_info.filename[:-8]
                            # Create a slug from the name without the extension
                            slug = slugify(name_without_extension)
                            # Use the slug as the file name instead of the original name
                            slug_filename = f"{slug}.geojson"
                            # Extract the file with its original name
                            zip_ref.extract(file_info, path=settings.MEDIA_GEOJSON)
                            # Get the full path to the extracted file
                            extracted_file_path = os.path.join(settings.MEDIA_GEOJSON, file_info.filename)
                            # Rename the extracted file to the desired slug filename
                            os.rename(extracted_file_path, os.path.join(settings.MEDIA_GEOJSON, slug_filename))
                            geojson_files.append(slug_filename)
                
                # Actualiza el campo delivery_areas con la lista de archivos .geojson
                logisticnode.delivery_areas = ",".join(geojson_files)
                
                # Guarda el objeto logisticnode
                logisticnode.save()
                
                # Elimina el archivo ZIP después de procesarlo
                print('Eliminando', zip_file)
                os.remove(zip_file)
            elif filename.endswith('.geojson'):
                # Crea un slug a partir del nombre del archivo
                name_without_extension = filename[:-8]
                # Crea un slug a partir del nombre sin la extensión
                slug = slugify(name_without_extension)
                
                # Guarda el archivo .geojson en la carpeta MEDIA con el nombre slugificado
                fs = FileSystemStorage(location=settings.MEDIA_GEOJSON)
                slug_filename = f"{slug}.geojson"
                fs.save(slug_filename, delivery_areas_file)
                # Actualiza el campo delivery_areas con el nombre del archivo .geojson
                logisticnode.delivery_areas = slug_filename
                
                # Guarda el objeto logisticnode
                logisticnode.save()
            else:
                return HttpResponseBadRequest('El archivo no es un archivo .zip o .geojson.')
        
            
        return response


    def get_success_url(self):
        return reverse("logisticnode-detail", kwargs={"logisticnode_id": self.object.pk})


@method_decorator([never_cache, login_required], name="dispatch")
class LogisticNodeAddView(CreateView):
    model = LogisticNode
    form_class = LogisticNodeForm
    template_name = "frontend/logisticnode_add_form.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # ctx["navbar"] = "request_add"
        print("self.request.method", self.request.method)

        if self.request.method == "POST":
            ctx["origin_location"] = self.request.POST.get("origin_location", None)


        return ctx

    def form_valid(self, form):
        print("LogisticNodeAddView form.cleaned_data", form.cleaned_data)
        logisticnode = form.instance
        sender = self.request.user
        response = super().form_valid(form)
        logisticnode.creator = sender

        logisticnode.save()
        return response

    def form_invalid(self, form):
        print("form_invalid", form.errors)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse("logisticnode-detail", kwargs={"logisticnode_id": self.object.pk})


@method_decorator([never_cache], name="dispatch")
class LogisticNodeDetailView(DetailView):
    model = LogisticNode
    form_class = LogisticNodeForm
    template_name = "frontend/logisticnode_view_form.html"


    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # ctx["navbar"] = "request_add"
        print("self.request.method", self.request.method)
        object_id = self.kwargs.get("logisticnode_id")
        object = get_object_or_404(LogisticNode, id=object_id)
        # Split the delivery_areas field and pass it to the template context
        if object.delivery_areas:
            delivery_areas_list = object.delivery_areas.split(",")
        else:
            delivery_areas_list = []

        # Construct the context dictionary
        ctx['delivery_areas_list'] = delivery_areas_list
        return ctx

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("logisticnode_id")
        object = get_object_or_404(LogisticNode, id=object_id)
        return object
        






def get_all_logisticnode_geojson(request):
    json = []
    print("request.user", request.user)
    if request.user.is_authenticated:
        logisticnodes = LogisticNode.objects.filter(active=True, deleted=False)
        for logisticnode in logisticnodes:
            body = f"Localització: <b>{logisticnode.origin_name}</b>"
            if logisticnode.timetable:
                body += f"<br>Horaris: {logisticnode.timetable}"

            json.append(
                {
                    "id": logisticnode.id,
                    "location": GeoJsonPoint(logisticnode.origin_location),
                    "name": logisticnode.name,
                    "body": body,
                }
            )
    return JsonResponse(json, safe=False)


def get_logisticnode_geojson(request, logisticnode_id):
    logisticnode = LogisticNode.objects.get(id=logisticnode_id)
    json = {
        "id": logisticnode.id,
        "origin_name": logisticnode.name,
        "origin": GeoJsonPoint(logisticnode.origin_location),
    }
    return JsonResponse(json)


@csrf_exempt
def update_logisticnode(request, object_id):
    can_update = False
    reason = None
    if request.method == "POST":
        action = request.POST["action"]
        logged_user = request.user
        object = LogisticNode.objects.filter(id=object_id).first()
        if object:
            can_update = logged_user.is_superuser or logged_user == object.creator

        if can_update:
            if action == "delete":
                object.deleted = True
            elif action == "inactive":
                object.active = False
            object.save()

    return JsonResponse({"status": can_update, "reason": reason})


# End LogisticNode Views
