# from datetime import datetime
import pytz
import csv
from dateutil.relativedelta import relativedelta
from math import floor

# from wkhtmltopdf import render_to_pdf
# import pdfkit
from io import BytesIO
from xhtml2pdf import pisa
from notifications.signals import notify
from chat.views import get_unseen_messages_count
from django.db.models import Q, Max
from django import template
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.shortcuts import render, reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from django.core.serializers import serialize


from django.contrib.auth.views import LoginView
from core.forms import EmailAuthenticationForm  # Create a custom form


# from django_registration.backends.one_step.views import RegistrationView as BaseRegistrationView
from django_registration.backends.activation.views import (
    ActivationView as BaseActivationView,
    RegistrationView as BaseRegistrationView,
)
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from geojson import LineString, Point as GeoJsonPoint
from django.http import JsonResponse


from django.contrib.gis.geos import Point
from django.views.decorators.csrf import csrf_exempt

from django.views.generic import CreateView, UpdateView, DetailView
import core.coopcycle as coopcycle
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from core.serializers import (
    TripOfferSerializer,
    TripRequestSerializer,
    TripMatchSerializer,
    UserSerializer,
)

from core.models import (
    Page,
    UserProfile,
    TripOffer,
    FrequencyType,
    TripRequest,
    TripMatch,
    MatchStatus,
    search_trip_requests,
    search_trip_offers,
    Storage,
)
from core.forms import (
    UserProfileForm,
    TripOfferForm,
    TripRequestForm,
    StorageForm,
    ConfirmDeliveryForm,
)
from core.utils import (
    bot,
    pay,
    chat_id,
    send_notifications,
    send_html_email,
    create_hashtags,
    get_day_month_name,
    get_nextmonth_by_weekday,
)


from core.models import LogisticNode


def render_manifest(request):
    html_data = open("core/static/manifest.webmanifest", "rb").read()
    return HttpResponse(html_data, content_type="application/json")


def get_user_storage_list(logged_user):
    user_storage_list = (
        Storage.objects.filter(active=True, deleted=False)
        .filter(Q(creator=logged_user) | Q(is_public=True))
        .order_by("name", "is_public")
    )
    return user_storage_list


from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters


class RegistrationView(BaseRegistrationView):
    """
    Registration overwritten to allow django admin access to specific group

    """

    def register(self, request, **cleaned_data):
        new_user = super().register(request)
        if "newsletter" in request.data and request.data["newsletter"] == "on":
            new_user.userprofile.newsletter = True

        if "firstname" in request.data:
            new_user.first_name = request.data["firstname"]

        if "lastname" in request.data:
            new_user.last_name = request.data["lastname"]

        if "phone" in request.data:
            new_user.userprofile.phone = request.data["phone"]

        new_user.userprofile.save()
        new_user.save()

        return new_user

    def get_success_url(self, user):
        messages.success(
            self.request,
            f"El teu compte {user} està en espera de confirmació. Consulta la teva bustia de correu electrònic.",
        )
        return reverse("index")


from django_registration.forms import RegistrationForm


class RegistrationAPIView(APIView):
    def post(self, request, format=None):
        form = RegistrationForm(request.data)
        if form.is_valid():
            new_user = form.save()
            return Response(
                {"success": True, "user_id": new_user.id},
                status=status.HTTP_201_CREATED,
            )
        else:
            return Response(
                {"success": False, "errors": form.errors},
                status=status.HTTP_400_BAD_REQUEST,
            )


class ActivationView(BaseActivationView):
    def get_success_url(self, request, user):
        # Enviar mail de bienvenida
        subject = "Benvingut/da a RutesCompartides.cat"
        login_url = settings.DEFAULT_DOMAIN + reverse("login")
        msg_html = (
            f"Hola, volem donar-te la benvinguda a <a href='{settings.DEFAULT_DOMAIN}'>RutesCompartides.cat"
            f"<br>Pots <a href='{login_url}'>iniciar sessió</a> per a poder crear rutes o comandes."
        )

        print("Sending welcome mail to", user.email)
        send_html_email([user.email], subject, msg_html)
        return ("registration_activation_complete", (), {})


@method_decorator([never_cache, login_required], name="dispatch")
class TripOfferUpdateView(UpdateView):
    # fields = ['name']
    # template_name_suffix = '_update_form'
    model = TripOffer
    form_class = TripOfferForm
    template_name = "frontend/offer_add_form.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("offer_id")
        return get_object_or_404(TripOffer, id=object_id)

    def get_context_data(self, **kwargs):
        print("TripOfferUpdateView get_context_data")
        object_id = self.kwargs.get("offer_id")
        object = TripOffer.objects.get(id=object_id)

        ctx = super().get_context_data(**kwargs)
        logged_user = self.request.user

        ctx["user_storage_list"] = get_user_storage_list(logged_user)
        if self.request.method == "GET":
            ctx["origin_location"] = object.origin_location
            ctx["destination_location"] = object.destination_location
        else:
            ctx["origin_location"] = self.request.POST.get("origin_location", "")
            ctx["destination_location"] = self.request.POST.get(
                "destination_location", ""
            )

        ctx["step_location_1"] = (
            object.step_location_1 if object.step_location_1 else ""
        )
        ctx["step_location_2"] = (
            object.step_location_2 if object.step_location_2 else ""
        )
        ctx["step_location_3"] = (
            object.step_location_3 if object.step_location_3 else ""
        )
        ctx["step_location_4"] = (
            object.step_location_4 if object.step_location_4 else ""
        )
        ctx["step_location_5"] = (
            object.step_location_5 if object.step_location_5 else ""
        )
        ctx["step_location_6"] = (
            object.step_location_6 if object.step_location_6 else ""
        )
        ctx["step_name_1"] = object.step_name_1 if object.step_name_1 else ""
        ctx["step_name_2"] = object.step_name_2 if object.step_name_2 else ""
        ctx["step_name_3"] = object.step_name_3 if object.step_name_3 else ""
        ctx["step_name_4"] = object.step_name_4 if object.step_name_4 else ""
        ctx["step_name_5"] = object.step_name_5 if object.step_name_5 else ""
        ctx["step_name_6"] = object.step_name_6 if object.step_name_6 else ""

        ctx["transport_isothermic"] = object.transport_isothermic
        ctx["transport_cold"] = object.transport_cold
        ctx["transport_freezer"] = object.transport_freezer
        ctx["transport_dry"] = object.transport_dry
        ctx["frequency"] = object.frequency

        tags = list(object.tags.all())

        if tags:
            ctx["tags"] = ",".join([tag.name for tag in tags])
        else:
            ctx["tags"] = ""
        return ctx

    def form_valid(self, form):
        trip_offer = form.instance
        response = super().form_valid(form)

        print("TripOfferUpdateView form.cleaned_data", form.cleaned_data)

        trip_offer.driver = self.request.user
        save_scope = form.cleaned_data.get("save_scope", "")

        tags = form.cleaned_data.get("tags", "").split(",")
        for tag in trip_offer.tags.all():
            tag.delete()
        for tag in tags:
            trip_offer.tags.add(tag)
        trip_offer.save()

        if save_scope == "all":
            all_linked_offers = (
                TripOffer.objects.filter(
                    frequency_group_id=trip_offer.frequency_group_id, active=True
                )
                .exclude(id=trip_offer.id)
                .order_by("time_depart")
            )
            original_time_depart = form.cleaned_data.get("original_time_depart")
            original_time_arrival = form.cleaned_data.get("original_time_arrival")
            original_internal_name = ""
            modified_time_depart = trip_offer.time_depart
            modified_time_arrival = trip_offer.time_arrival
            diff_time_depart = modified_time_depart - original_time_depart
            diff_time_arrival = modified_time_arrival - original_time_arrival
            if diff_time_depart:
                print("Changed time_depart by", diff_time_depart)

                internal_name = trip_offer.internal_name
                if internal_name and "-" in internal_name:
                    original_internal_name = internal_name[: internal_name.find("-")]
                    day, month = get_day_month_name(trip_offer.time_depart)
                    trip_offer.internal_name = f"{original_internal_name}-{day}{month}"

            if diff_time_arrival:
                print("Changed time_arrival by", diff_time_arrival)

            for next_offer in all_linked_offers.all():
                if diff_time_depart:
                    next_offer.time_depart += diff_time_depart

                    if original_internal_name:
                        day, month = get_day_month_name(next_offer.time_depart)
                        next_offer.internal_name = (
                            f"{original_internal_name}-{day}{month}"
                        )
                if diff_time_arrival:
                    next_offer.time_arrival += diff_time_arrival

                if (
                    next_offer.origin_location != trip_offer.origin_location
                    or next_offer.destination_location
                    != trip_offer.destination_location
                ):
                    next_offer.calculate_route()

                next_offer.origin_name = trip_offer.origin_name
                next_offer.origin_location = trip_offer.origin_location
                next_offer.destination_name = trip_offer.destination_name
                next_offer.destination_location = trip_offer.destination_location

                next_offer.route = trip_offer.route
                # next_offer.internal_name = trip_offer.internal_name

                next_offer.comment = trip_offer.comment
                next_offer.tags = trip_offer.tags
                next_offer.vehicle = trip_offer.vehicle
                next_offer.cost_km = trip_offer.cost_km
                next_offer.max_detour_km = trip_offer.max_detour_km
                next_offer.available_seats = trip_offer.available_seats
                next_offer.available_space = trip_offer.available_space
                next_offer.transport_isothermic = trip_offer.transport_isothermic
                next_offer.transport_cold = trip_offer.transport_cold
                next_offer.transport_freezer = trip_offer.transport_freezer
                next_offer.transport_dry = trip_offer.transport_dry
                next_offer.save()
        else:
            trip_offer.frequency_group_id = None
            trip_offer.frequency = "ONCE"
            trip_offer.save()

        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": trip_offer.id}
        )
        username_url = settings.DEFAULT_DOMAIN + reverse(
            "profile-view", kwargs={"username": trip_offer.driver.username}
        )
        if not settings.DEV and trip_offer.telegram_published:
            t = template.loader.get_template("frontend/bot_notification_offer.md")
            c = {
                "trip_offer": trip_offer,
                "edited": True,
                "offer_url": offer_url,
                "username_url": username_url,
                "hashtags": create_hashtags(trip_offer),
            }
            bot_msg = t.render(c)
            bot_msg = bot_msg.replace("&#x27;", "'")
            bot.send_message(chat_id, bot_msg)

        print("edited trip offer, regenerating route", trip_offer)
        print("coords", trip_offer._get_coords())
        trip_offer.calculate_route()

        # Send edit notification to trip request clients

        pending_matches = trip_offer.matches.filter(
            status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
        )
        for match in pending_matches:
            trip_request = match.request
            trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                "triprequest-detail", kwargs={"request_id": trip_request.id}
            )
            subject = f"S'ha editat la ruta associada a la teva comanda."
            msg_notif = f"S'ha editat <a href='{offer_url}'>ruta</a> associada a la <a href='{trip_request_url}'>teva comanda</a>"
            msg_body = (
                f"S'ha editat <a href='{offer_url}'>ruta</a> associada a la <a href='{trip_request_url}'>teva comanda.</a>"
                f"<br>De: {trip_offer.time_depart}: {trip_offer.origin_name}"
                f"<br>A:  {trip_offer.time_arrival}: {trip_offer.destination_name}"
            )
            send_notifications(
                trip_request.client, trip_request.client, msg_notif, subject, msg_body
            )

        trip_offer.notify_trip_requests()
        return response

    def get_success_url(self):
        return reverse("tripoffer-detail", kwargs={"offer_id": self.object.pk})


@login_required
def export_summary_offer(request, format, offer_id):
    object = TripOffer.objects.get(id=offer_id)
    t = template.loader.get_template("frontend/summary_offer.html")
    # c = template.RequestContext(request, {"object": object})
    context = {}
    total_packages = total_weight = total_volume = 0
    accepted_matches = object.matches.filter(
        status__in=[
            MatchStatus.ACCEPTED,
            MatchStatus.COMPLETED,
            MatchStatus.REVIEW_FROZEN,
        ]
    ).order_by("-modified")
    context["accepted_matches"] = accepted_matches
    for match in accepted_matches:
        total_packages += match.request.packages_num
        total_weight += match.request.packages_weight
        total_volume += (
            match.request.packages_length
            * match.request.packages_height
            * match.request.packages_width
        )
    context["object"] = object
    context["total_packages"] = total_packages
    context["total_weight"] = total_weight
    # Currently in cm3, in case it is less than 0,001 m3 we round it up to it.
    if total_volume < 1000:
        total_volume = 1000
    total_volume = total_volume / 1000000
    print("total_volume", total_volume)
    context["total_volume"] = total_volume
    context["DEFAULT_DOMAIN"] = settings.DEFAULT_DOMAIN
    if object.internal_name:
        filename = f"informe_ruta_{offer_id}_{object.internal_name}"
    else:
        filename = f"informe_ruta_{offer_id}"

    html = t.render(context)

    if format == "pdf":
        # pdf_data = pdfkit.from_string(html, output_path=False)
        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        if not pdf.err:
            response = HttpResponse(result.getvalue(), content_type="application/pdf")
            response["Content-Disposition"] = f'attachment; filename="{filename}.pdf"'
            return response
    elif format == "csv":
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = f'attachment; filename="{filename}.csv"'
        writer = csv.writer(response)
        writer.writerow(
            [
                "Informe de ruta",
                "#ID",
                "Origen",
                "Destinació",
                "Data Sortida",
                "Data Arribada",
                "Pes total dels paquets (kg)",
                "Volum total dels paquets (m3)",
                "N. de paquets",
                "Isoterm",
                "Refrigerat",
                "Congelat",
                "Sense humitat",
                "Seients disponibles",
                "Alabarà adjunt",
                "Comentaris",
                "Dades de contacte",
            ]
        )
        writer.writerow(
            [
                "",
                offer_id,
                object.origin_name,
                object.destination_name,
                object.time_depart.astimezone(
                    pytz.timezone(settings.TIME_ZONE)
                ).strftime("%d/%m/%Y %H:%M"),
                object.time_arrival.astimezone(
                    pytz.timezone(settings.TIME_ZONE)
                ).strftime("%d/%m/%Y %H:%M"),
                "{:.2f}".format(total_weight).replace(".", ","),
                "{:.3f}".format(total_volume).replace(".", ","),
                total_packages,
                "SI" if object.transport_isothermic else "NO",
                "SI" if object.transport_cold else "NO",
                "SI" if object.transport_freezer else "NO",
                "SI" if object.transport_dry else "NO",
                object.available_seats,
                "",
                object.comment,
                "",
            ]
        )
        writer.writerow([""])
        writer.writerow(
            [
                "Comandes",
            ]
        )

        for match in accepted_matches:
            triprequest_volume = (
                match.request.packages_length
                * match.request.packages_height
                * match.request.packages_width
            )
            if triprequest_volume < 1000:
                triprequest_volume = 1000
            triprequest_volume = triprequest_volume / 1000000
            triprequest_packages = match.request.packages_num
            if match.request.max_time_arrival:
                max_time_arrival = match.request.max_time_arrival.astimezone(
                    pytz.timezone(settings.TIME_ZONE)
                ).strftime("%d/%m/%Y %H:%M")
            else:
                max_time_arrival = "Flexible"
            writer.writerow(
                [
                    "Comanda",
                    match.request.pk,
                    match.request.origin_name,
                    match.request.destination_name,
                    match.request.min_time_arrival.astimezone(
                        pytz.timezone(settings.TIME_ZONE)
                    ).strftime("%d/%m/%Y %H:%M"),
                    max_time_arrival,
                    "{:.2f}".format(match.request.packages_weight).replace(".", ","),
                    "{:.3f}".format(triprequest_volume).replace(".", ","),
                    triprequest_packages,
                    "SI" if match.request.transport_isothermic else "NO",
                    "SI" if match.request.transport_cold else "NO",
                    "SI" if match.request.transport_freezer else "NO",
                    "SI" if match.request.transport_dry else "NO",
                    "Vol Seient" if match.request.wants_carpool else "No necessita",
                    "SI" if match.request.delivery_note else "NO",
                    match.request.comment,
                    match.request.delivery_contact,
                ]
            )

        return response
    else:
        return HttpResponse(
            html,
            status=200,
        )


@method_decorator([never_cache, login_required], name="dispatch")
class TripOfferAddView(CreateView):
    model = TripOffer
    form_class = TripOfferForm
    template_name = "frontend/offer_add_form.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["navbar"] = "offer_add"
        logged_user = self.request.user
        print("self.request.method", self.request.method)
        if self.request.method == "GET":
            ctx["user_storage_list"] = get_user_storage_list(logged_user)
            copy_from = self.request.GET.get("copy_from", None)
            if copy_from:
                trip_offer = TripOffer.objects.filter(id=copy_from).first()
                if trip_offer and trip_offer.driver == logged_user:
                    ctx["origin_location"] = trip_offer.origin_location
                    ctx["destination_location"] = trip_offer.destination_location
                    ctx["origin_name"] = trip_offer.origin_name
                    ctx["destination_name"] = trip_offer.destination_name
                    ctx["transport_isothermic"] = trip_offer.transport_isothermic
                    ctx["transport_cold"] = trip_offer.transport_cold
                    ctx["transport_freezer"] = trip_offer.transport_freezer
                    ctx["transport_dry"] = trip_offer.transport_dry
                    ctx["time_depart"] = trip_offer.time_depart.strftime(
                        "%d/%m/%Y %H:%M"
                    )
                    ctx["time_arrival"] = trip_offer.time_arrival.strftime(
                        "%d/%m/%Y %H:%M"
                    )
                    ctx["step_location_1"] = (
                        trip_offer.step_location_1 if trip_offer.step_location_1 else ""
                    )
                    ctx["step_location_2"] = (
                        trip_offer.step_location_2 if trip_offer.step_location_2 else ""
                    )
                    ctx["step_location_3"] = (
                        trip_offer.step_location_3 if trip_offer.step_location_3 else ""
                    )
                    ctx["step_location_4"] = (
                        trip_offer.step_location_4 if trip_offer.step_location_4 else ""
                    )
                    ctx["step_location_5"] = (
                        trip_offer.step_location_5 if trip_offer.step_location_5 else ""
                    )
                    ctx["step_location_6"] = (
                        trip_offer.step_location_6 if trip_offer.step_location_6 else ""
                    )
                    ctx["step_name_1"] = (
                        trip_offer.step_name_1 if trip_offer.step_name_1 else ""
                    )
                    ctx["step_name_2"] = (
                        trip_offer.step_name_2 if trip_offer.step_name_2 else ""
                    )
                    ctx["step_name_3"] = (
                        trip_offer.step_name_3 if trip_offer.step_name_3 else ""
                    )
                    ctx["step_name_4"] = (
                        trip_offer.step_name_4 if trip_offer.step_name_4 else ""
                    )
                    ctx["step_name_5"] = (
                        trip_offer.step_name_5 if trip_offer.step_name_5 else ""
                    )
                    ctx["step_name_6"] = (
                        trip_offer.step_name_6 if trip_offer.step_name_6 else ""
                    )
                    ctx["internal_name"] = trip_offer.internal_name

                    ctx["comment"] = trip_offer.comment
                    ctx["max_detour_km"] = trip_offer.max_detour_km
                    ctx["available_seats"] = trip_offer.available_seats
                    ctx["available_space"] = trip_offer.available_space
                    ctx["cost_km"] = trip_offer.cost_km
                    ctx["vehicle"] = trip_offer.vehicle
                    tags = list(trip_offer.tags.all())
                    if tags:
                        ctx["tags"] = ",".join([tag.name for tag in tags])

            else:
                location = self.request.GET.get("origin", None)
                if location and location != "undefined,undefined":
                    location = location.split(",")
                    ctx["origin_location"] = (
                        f"SRID=4326;POINT( {location[0]} {location[1]})"
                    )
                ctx["origin_name"] = self.request.GET.get("origin_name", None)
                location = self.request.GET.get("destination", None)
                if location and location != "undefined,undefined":
                    location = location.split(",")
                    ctx["destination_location"] = (
                        f"SRID=4326;POINT( {location[0]} {location[1]})"
                    )
                ctx["destination_name"] = self.request.GET.get("destination_name", None)
                ctx["time_depart"] = self.request.GET.get("time_depart", None)
                ctx["time_arrival"] = self.request.GET.get("time_arrival", None)
                ctx["frequency"] = self.request.GET.get("frequency", None)
                ctx["transport_isothermic"] = (
                    self.request.GET.get("isothermic", False) == "true"
                )
                ctx["transport_cold"] = self.request.GET.get("cold", False) == "true"
                ctx["transport_freezer"] = (
                    self.request.GET.get("freezer", False) == "true"
                )
                ctx["transport_dry"] = self.request.GET.get("dry", False) == "true"
                match_object = self.request.GET.get("match_object", None)
                ctx["match_object"] = match_object

                if (
                    match_object
                    and "origin_location" not in ctx
                    and "destination_location" not in ctx
                ):
                    # Autofill offer data to create the offer
                    trip_request = TripRequest.objects.filter(id=match_object).first()
                    print("getting data from trip_request", trip_request)
                    ctx["origin_location"] = trip_request.origin_location
                    ctx["destination_location"] = trip_request.destination_location
                    ctx["origin_name"] = trip_request.origin_name
                    ctx["destination_name"] = trip_request.destination_name
                    ctx["transport_isothermic"] = trip_request.transport_isothermic
                    ctx["transport_cold"] = trip_request.transport_cold
                    ctx["transport_freezer"] = trip_request.transport_freezer
                    ctx["transport_dry"] = trip_request.transport_dry
                else:
                    # New route
                    pass
                    # max_future_date = timezone.now().date() + relativedelta(
                    #     months=settings.MAX_MONTHS_FREQUENT
                    # )
                    # ctx["repeat_until"] = max_future_date.strftime("%d/%m/%Y")

        if self.request.method == "POST":
            ctx["origin_location"] = self.request.POST.get("origin_location", None)
            ctx["destination_location"] = self.request.POST.get(
                "destination_location", None
            )

            ctx["step_location_1"] = self.request.POST.get("step_location_1", "")
            ctx["step_location_2"] = self.request.POST.get("step_location_2", "")
            ctx["step_location_3"] = self.request.POST.get("step_location_3", "")
            ctx["step_location_4"] = self.request.POST.get("step_location_4", "")
            ctx["step_location_5"] = self.request.POST.get("step_location_5", "")
            ctx["step_location_6"] = self.request.POST.get("step_location_6", "")
            ctx["step_name_1"] = self.request.POST.get("step_name_1", "")
            ctx["step_name_2"] = self.request.POST.get("step_name_2", "")
            ctx["step_name_3"] = self.request.POST.get("step_name_3", "")
            ctx["step_name_4"] = self.request.POST.get("step_name_4", "")
            ctx["step_name_5"] = self.request.POST.get("step_name_5", "")
            ctx["step_name_6"] = self.request.POST.get("step_name_6", "")
            ctx["delivery_contact"] = self.request.POST.get("delivery_contact", "")
            ctx["transport_isothermic"] = self.request.POST.get(
                "transport_isothermic", False
            )
            ctx["transport_cold"] = self.request.POST.get("transport_cold", False)
            ctx["transport_freezer"] = self.request.POST.get("transport_freezer", False)
            ctx["transport_dry"] = self.request.POST.get("transport_dry", False)
            ctx["match_object"] = self.request.POST.get("match_object", None)
            ctx["repeat_until"] = self.request.POST.get("repeat_until", None)
            ctx["frequency"] = self.request.POST.get("frequency", None)

        print("ctx TripOfferAddView", ctx)
        return ctx

    def form_valid(self, form):
        trip_offer = form.instance
        response = super().form_valid(form)
        print("TripOfferAddView form.cleaned_data", form.cleaned_data)

        trip_offer.driver = self.request.user

        tags = form.cleaned_data.get("tags", "").split(",")
        for tag in trip_offer.tags.all():
            tag.delete()
        for tag in tags:
            trip_offer.tags.add(tag)
        trip_offer.save()

        frequency = form.cleaned_data.get("frequency", "")
        repeat_until = form.cleaned_data.get("repeat_until", "")
        # repeat_until = timezone.datetime.strptime(repeat_until, "%d/%m/%Y")

        days_till_next_repeat = 0
        if frequency == FrequencyType.MONTHLY:
            days_till_next_repeat = 30
        elif frequency == FrequencyType.WEEKLY:
            days_till_next_repeat = 7
        elif frequency == FrequencyType.BIWEEKLY:
            days_till_next_repeat = 14
        elif frequency == FrequencyType.DAILY:
            days_till_next_repeat = 1

        frequency_group_id = 0
        if days_till_next_repeat:
            # in case the offer is regular
            next_trip_offer = trip_offer
            next_time_depart = trip_offer.time_depart
            original_weeknumber = floor(next_time_depart.day / 7)
            while next_time_depart.date() < repeat_until:

                if frequency_group_id == 0:  # if first iteration

                    original_internal_name = form.cleaned_data.get("internal_name", "")
                    frequency_group_id = TripOffer.get_highest_frequency_group_id() + 1
                    next_trip_offer.frequency_group_id = frequency_group_id
                    next_trip_offer.frequency = frequency
                else:
                    next_trip_offer.pk = None  # way to clone a model instance in django

                if original_internal_name:
                    day, month = get_day_month_name(next_trip_offer.time_depart)
                    next_trip_offer.internal_name = (
                        f"{original_internal_name}-{day}{month}"
                    )

                next_time_depart = next_trip_offer.time_depart
                next_time_arrival = next_trip_offer.time_arrival

                next_trip_offer.save()

                if days_till_next_repeat == 30:
                    # ex: 1st wednesday of the month (not 30 days actually)
                    next_time_depart = get_nextmonth_by_weekday(
                        next_time_depart, original_weeknumber
                    )
                    next_time_arrival = get_nextmonth_by_weekday(
                        next_time_arrival, original_weeknumber
                    )
                else:
                    next_time_depart += timezone.timedelta(days=days_till_next_repeat)
                    next_time_arrival += timezone.timedelta(days=days_till_next_repeat)
                next_trip_offer.time_depart = next_time_depart
                next_trip_offer.time_arrival = next_time_arrival
                print("next_trip_offer.time_depart", next_trip_offer.time_depart)
                print("next_trip_offer.time_arrival", next_trip_offer.time_arrival)

                print(
                    "next_time_depart <= repeat_until",
                    next_time_depart.date(),
                    "<=",
                    repeat_until,
                )

        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": trip_offer.id}
        )
        username_url = settings.DEFAULT_DOMAIN + reverse(
            "profile-view", kwargs={"username": trip_offer.driver.username}
        )

        sender = self.request.user
        match_object = form.cleaned_data.get("match_object", "")
        if match_object:
            trip_request = TripRequest.objects.filter(id=match_object).first()
            add_match(sender, trip_offer, trip_request, role="driver")

        trip_offer.notify_trip_requests()
        return HttpResponseRedirect(self.get_success_url(frequency_group_id))

    def form_invalid(self, form):
        print("form_invalid", form.errors)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self, frequency_group_id=0):
        if frequency_group_id == 0:
            return reverse("tripoffer-detail", kwargs={"offer_id": self.object.pk})
        else:
            return reverse(
                "tripoffer-frequent-view",
                kwargs={"frequency_group_id": frequency_group_id},
            )


@require_http_methods(["GET"])
def database_export(request):
    # Obtén los TripOffers ordenados por el campo time_arrival de forma descendente (más nuevo a más viejo).
    trip_offers = TripOffer.objects.only(
        "id",
        "time_depart",
        "time_arrival",
        "origin_location",
        "destination_location",
        "origin_name",
        "destination_name",
        "vehicle",
        "transport_isothermic",
        "transport_cold",
        "transport_freezer",
        "transport_dry",
    ).order_by("-time_arrival")

    # Formatea los datos según tus requisitos
    formatted_trip_offers = []
    for trip_offer in trip_offers:
        geojson_url = reverse(
            "tripoffer-geojson", args=[trip_offer.id]
        )  # Obtén la URL del GeoJSON
        geojson_url = f"{settings.DEFAULT_DOMAIN}{geojson_url}"

        formatted_offer = {
            "time_depart": trip_offer.time_depart,
            "time_arrival": trip_offer.time_arrival,
            "origin_name": trip_offer.origin_name,
            "destination_name": trip_offer.destination_name,
            "geojson_url": geojson_url,
            "vehicle_name": trip_offer.vehicle,
            "transport_conditions": {
                "isothermic": trip_offer.transport_isothermic,
                "cold": trip_offer.transport_cold,
                "freezer": trip_offer.transport_freezer,
                "dry": trip_offer.transport_dry,
            },
        }
        formatted_trip_offers.append(formatted_offer)

    # Crea una respuesta JSON con los datos formateados
    response_data = {"TripOffers": formatted_trip_offers}

    return JsonResponse(response_data, json_dumps_params={"indent": 2})


@csrf_exempt
def duplicate_tripoffer(request, offer_id):
    return HttpResponseRedirect(reverse("tripoffer-add") + f"?copy_from={offer_id}")


# http://0.0.0.0:8000/offer/add/?time_depart=2021-11-09%2009:23:00+00:00time_arrival=2021-11-10%2009:23:00+00:00
# http://0.0.0.0:8000/offer/add/?match_object=65&isothermic=false&cold=false&freezer=false&dry=false&time_depart=08/12/2021%2019:52&origin_name=%20Xeraco,%20Val%C3%A8ncia&origin=-0.2162596,39.033098&destination_name=Val%C3%A8ncia&destination=-0.3762881,39.4699075&search=trip_requests


@method_decorator([never_cache], name="dispatch")
class TripOfferMapView(DetailView):
    model = TripOffer
    form_class = TripOfferForm
    template_name = "frontend/offer_map_view.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("offer_id")
        return get_object_or_404(TripOffer, id=object_id)


class TripOfferContextDataMixin:
    def get_context_data(self, **kwargs):
        instance = self.get_object(self.kwargs.get("offer_id"))
        context = {}
        context["now"] = timezone.localtime()
        if not self.request.user.is_anonymous:
            if self.request.user == instance.driver:
                nodes_result = instance.possible_logistic_nodes()
                coopcycle_only_origin_covered = []
                coopcycle_only_destination_covered = []

                for result in nodes_result:
                    logistic_node, (
                        coopcycle_origin_covered,
                        coopcycle_destination_covered,
                    ) = result
                    if coopcycle_origin_covered:
                        cost = coopcycle.get_retail_prices(
                            logistic_node.api_url,
                            logistic_node.check_access_token,
                            instance.origin_name,
                            logistic_node.origin_name,
                        )
                        coopcycle_only_origin_covered.append(
                            {"logistic_node": logistic_node, "cost": cost}
                        )
                    if coopcycle_destination_covered:
                        cost = coopcycle.get_retail_prices(
                            logistic_node.api_url,
                            logistic_node.check_access_token,
                            logistic_node.origin_name,
                            instance.destination_name,
                        )
                        coopcycle_only_destination_covered.append(
                            {"logistic_node": logistic_node, "cost": cost}
                        )

                trip_results = instance.search_trip_results(user=self.request.user)
                trip_results_ids = instance.search_trip_results(
                    user=self.request.user, exclude_frozen=False
                ).values_list("id", flat=True)

                user_active_requests = (
                    self.request.user.triprequest_set.filter(active=True)
                    .filter(id__in=trip_results_ids)
                    .all()
                )
                user_active_requests = user_active_requests.exclude(frozen=True)

                context = {
                    "trip_results": trip_results,
                    "user_trip_requests": user_active_requests,
                    "trip_matches": instance.matches.all().order_by("-modified"),
                    "too_late_to_edit": instance.too_late_to_edit
                    or instance.has_completed_requests,
                    "coopcycle_only_origin_covered": coopcycle_only_origin_covered,
                    "coopcycle_only_destination_covered": coopcycle_only_destination_covered,
                }

        return context


@method_decorator([never_cache], name="dispatch")
class TripOfferDetailView(TripOfferContextDataMixin, DetailView):
    model = TripOffer
    form_class = TripOfferForm

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("offer_id")
        return get_object_or_404(TripOffer, id=object_id)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        instance = self.get_object()
        context["object"] = instance
        return render(request, "frontend/offer_view_form.html", context)


@method_decorator([never_cache], name="dispatch")
class TripOfferAPIView(TripOfferContextDataMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [AllowAny]

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("offer_id")
        return get_object_or_404(TripOffer, id=object_id)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        context = self.get_context_data(**kwargs)
        serializer = TripOfferSerializer(instance)
        if "trip_matches" in context:
            trip_matches_serialized = TripMatchSerializer(
                context["trip_matches"], many=True
            ).data
            context["trip_matches"] = trip_matches_serialized

        return Response({**serializer.data, **context})


@method_decorator([never_cache], name="dispatch")
class TripRequestMapView(DetailView):
    model = TripRequest
    form_class = TripRequestForm
    template_name = "frontend/request_map_view.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("request_id")
        return get_object_or_404(TripRequest, id=object_id)


class TripRequestContextDataMixin:
    def get_context_data(self, **kwargs):
        context = {}
        context["now"] = timezone.localtime()
        instance = self.get_object(self.kwargs.get("request_id"))
        if not self.request.user.is_anonymous:
            if self.request.user == instance.client:
                nodes_result = instance.possible_logistic_nodes()
                context["coopcycle_only_origin_covered"] = []
                context["coopcycle_only_destination_covered"] = []
                if instance.origin_logisticnode:
                    context["logistic_node_checkout_form_url"] = (
                        instance.origin_logisticnode.checkout_form_url
                    )
                elif instance.destination_logisticnode:
                    context["logistic_node_checkout_form_url"] = (
                        instance.destination_logisticnode.checkout_form_url
                    )

                for result in nodes_result:
                    logistic_node, (
                        coopcycle_origin_covered,
                        coopcycle_destination_covered,
                    ) = result
                    if coopcycle_origin_covered:
                        cost = coopcycle.get_retail_prices(
                            logistic_node.api_url,
                            logistic_node.check_access_token,
                            instance.origin_name,
                            logistic_node.origin_name,
                        )
                        context["coopcycle_only_origin_covered"].append(
                            {"logistic_node": logistic_node, "cost": cost}
                        )
                    if coopcycle_destination_covered:
                        cost = coopcycle.get_retail_prices(
                            logistic_node.api_url,
                            logistic_node.check_access_token,
                            logistic_node.origin_name,
                            instance.destination_name,
                        )
                        context["coopcycle_only_destination_covered"].append(
                            {"logistic_node": logistic_node, "cost": cost}
                        )

            trip_results = instance.search_trip_results(user=self.request.user)
            trip_results_ids = instance.search_trip_results(
                user=self.request.user
            ).values_list("id", flat=True)
            user_active_offers = (
                self.request.user.tripoffer_set.filter(active=True)
                .filter(id__in=trip_results_ids)
                .all()
            )
            context["trip_results"] = trip_results
            context["user_trip_offers"] = user_active_offers
            context["trip_matches"] = instance.matches.all().order_by("-modified")

            if instance.frozen:
                pending_match = instance.matches.filter(
                    status__in=[
                        MatchStatus.PENDING,
                        MatchStatus.ACCEPTED,
                        MatchStatus.COMPLETED,
                    ]
                ).first()
                if pending_match:
                    offer_asked = pending_match.offer
                    context["match"] = pending_match
                    context["offer_asked"] = offer_asked
                    context["driver_asked"] = offer_asked.driver

        return context


@method_decorator([never_cache], name="dispatch")
class TripRequestDetailView(TripRequestContextDataMixin, DetailView):
    model = TripRequest
    form_class = TripRequestForm

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("request_id")
        return get_object_or_404(TripRequest, id=object_id)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        instance = self.get_object()
        context["object"] = instance
        return render(request, "frontend/request_view_form.html", context)


@method_decorator([never_cache], name="dispatch")
class TripRequestAPIView(TripRequestContextDataMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [AllowAny]

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("request_id")
        return get_object_or_404(TripRequest, id=object_id)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        context = self.get_context_data(**kwargs)
        serializer = TripRequestSerializer(instance)
        if "trip_matches" in context:
            trip_matches_serialized = TripMatchSerializer(
                context["trip_matches"], many=True
            ).data
            context["trip_matches"] = trip_matches_serialized

        return Response({**serializer.data, **context})


class CustomLoginView(LoginView):
    authentication_form = EmailAuthenticationForm


@method_decorator([never_cache, login_required], name="dispatch")
class TripRequestUpdateView(UpdateView):
    model = TripRequest
    form_class = TripRequestForm
    template_name = "frontend/request_add_form.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("request_id")
        return get_object_or_404(TripRequest, id=object_id)

    def get_context_data(self, **kwargs):
        object_id = self.kwargs.get("request_id")
        object = TripRequest.objects.get(id=object_id)

        ctx = super().get_context_data(**kwargs)
        logged_user = self.request.user
        ctx["user_storage_list"] = get_user_storage_list(logged_user)
        ctx["origin_location"] = object.origin_location
        ctx["destination_location"] = object.destination_location
        ctx["transport_isothermic"] = object.transport_isothermic
        ctx["transport_cold"] = object.transport_cold
        ctx["transport_freezer"] = object.transport_freezer
        ctx["transport_dry"] = object.transport_dry
        ctx["comment"] = object.comment
        ctx["delivery_note"] = object.delivery_note
        ctx["delivery_contact"] = object.delivery_note
        ctx["contact_from_profile"] = object.client.userprofile.contact_details
        print("object.max_time_arrival ", object.max_time_arrival)
        if object.max_time_arrival is None:
            ctx["flexible_arrival"] = True

        if self.request.method == "GET":

            ctx["time_depart"] = self.request.GET.get("time_depart", None)

        if self.request.method == "POST":
            ctx["flexible_arrival"] = self.request.POST.get("flexible_arrival", False)

        print("ctx", ctx)

        tags = list(object.tags.all())

        if tags:
            ctx["tags"] = ",".join([tag.name for tag in tags])
        else:
            ctx["tags"] = ""
        return ctx

    def form_valid(self, form):
        trip_request = form.instance
        sender = self.request.user
        response = super().form_valid(form)
        trip_request.client = sender

        tags = form.cleaned_data.get("tags", "").split(",")
        for tag in trip_request.tags.all():
            tag.delete()
        for tag in tags:
            trip_request.tags.add(tag)

        trip_request.save()

        # Send edit notification to trip offer drivers
        trip_request_url = settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": trip_request.id}
        )
        username_url = settings.DEFAULT_DOMAIN + reverse(
            "profile-view", kwargs={"username": trip_request.client.username}
        )
        if not settings.DEV and trip_request.telegram_published:
            t = template.loader.get_template("frontend/bot_notification_request.md")
            c = {
                "trip_request": trip_request,
                "edited": True,
                "trip_request_url": trip_request_url,
                "username_url": username_url,
                "hashtags": create_hashtags(trip_request),
            }
            bot_msg = t.render(c)
            bot_msg = bot_msg.replace("&#x27;", "'")
            bot.send_message(chat_id, bot_msg)

        pending_matches = trip_request.matches.filter(
            status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
        )
        for match in pending_matches:
            trip_offer = match.offer
            offer_url = settings.DEFAULT_DOMAIN + reverse(
                "tripoffer-detail", kwargs={"offer_id": trip_offer.id}
            )

            subject = "S'ha editat la comanda associada a la teva ruta."
            msg_notif = f"S'ha editat <a href='{trip_request_url}'>la comanda</a> associada a <a href='{offer_url}'>la teva ruta</a>"
            msg_body = (
                f"S'ha editat <a href='{trip_request_url}'>la comanda</a> associada a <a href='{offer_url}'>la teva ruta</a>."
                f"<br>De: {trip_request.min_time_arrival}: {trip_request.origin_name}"
                f"<br>A:  {trip_request.max_time_arrival}: {trip_request.destination_name}"
            )
            send_notifications(
                trip_offer.driver, trip_offer.driver, msg_notif, subject, msg_body
            )

        trip_request.notify_offers(action="update")
        return response

    def get_success_url(self):
        return reverse("triprequest-detail", kwargs={"request_id": self.object.pk})


@method_decorator([never_cache, login_required], name="dispatch")
class TripRequestAddView(CreateView):
    model = TripRequest
    form_class = TripRequestForm
    template_name = "frontend/request_add_form.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["navbar"] = "request_add"
        print("self.request.method", self.request.method)
        logged_user = self.request.user
        ctx["contact_from_profile"] = logged_user.userprofile.contact_details
        if self.request.method == "GET":
            ctx["user_storage_list"] = get_user_storage_list(logged_user)
            location = self.request.GET.get("origin", None)
            if location and location != "undefined,undefined":
                location = location.split(",")
                ctx["origin_location"] = (
                    f"SRID=4326;POINT( {location[0]} {location[1]})"
                )
            ctx["origin_name"] = self.request.GET.get("origin_name", None)
            location = self.request.GET.get("destination", None)
            if location and location != "undefined,undefined":
                location = location.split(",")
                ctx["destination_location"] = (
                    f"SRID=4326;POINT( {location[0]} {location[1]})"
                )
            ctx["origin_name"] = self.request.GET.get("origin_name", None)
            ctx["destination_name"] = self.request.GET.get("destination_name", None)
            ctx["time_depart"] = self.request.GET.get("time_depart", None)
            ctx["transport_isothermic"] = (
                self.request.GET.get("isothermic", False) == "true"
            )
            ctx["transport_cold"] = self.request.GET.get("cold", False) == "true"
            ctx["transport_freezer"] = self.request.GET.get("freezer", False) == "true"
            ctx["transport_dry"] = self.request.GET.get("dry", False) == "true"
            match_object = self.request.GET.get("match_object", None)
            ctx["match_object"] = match_object
            if (
                match_object
                and "origin_location" not in ctx
                and "destination_location" not in ctx
            ):
                # Autofill offer data to create the offer
                trip_offer = TripOffer.objects.filter(id=match_object).first()
                print("getting data from trip_offer", trip_offer)
                ctx["origin_location"] = trip_offer.origin_location
                ctx["destination_location"] = trip_offer.destination_location
                ctx["origin_name"] = trip_offer.origin_name
                ctx["destination_name"] = trip_offer.destination_name
                time_depart = trip_offer.time_depart.astimezone(
                    pytz.timezone(settings.TIME_ZONE)
                )
                ctx["time_depart"] = time_depart.strftime(
                    "%d/%m/%Y %H:%M"
                )  # trip_offer.time_depart.strftime("%d/%m/%Y %H:%M")
                ctx["transport_isothermic"] = trip_offer.transport_isothermic
                ctx["transport_cold"] = trip_offer.transport_cold
                ctx["transport_freezer"] = trip_offer.transport_freezer
                ctx["transport_dry"] = trip_offer.transport_dry

        if self.request.method == "POST":
            ctx["origin_location"] = self.request.POST.get("origin_location", None)
            ctx["destination_location"] = self.request.POST.get(
                "destination_location", None
            )
            ctx["match_object"] = self.request.POST.get("match_object", None)
            ctx["transport_isothermic"] = self.request.POST.get(
                "transport_isothermic", False
            )
            ctx["transport_cold"] = self.request.POST.get("transport_cold", False)
            ctx["transport_freezer"] = self.request.POST.get("transport_freezer", False)
            ctx["transport_dry"] = self.request.POST.get("transport_dry", False)
            ctx["flexible_arrival"] = self.request.POST.get("flexible_arrival", False)

            print("POST Request", ctx)

        return ctx

    def form_valid(self, form):
        print("TripRequestAddView form.cleaned_data", form.cleaned_data)
        trip_request = form.instance
        sender = self.request.user
        response = super().form_valid(form)
        trip_request.client = sender

        tags = form.cleaned_data.get("tags", "").split(",")
        for tag in trip_request.tags.all():
            tag.delete()
        for tag in tags:
            trip_request.tags.add(tag)

        trip_request.calculate_route()

        trip_request.save()

        trip_request_url = settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": trip_request.id}
        )
        username_url = settings.DEFAULT_DOMAIN + reverse(
            "profile-view", kwargs={"username": trip_request.client.username}
        )

        match_object = form.cleaned_data.get("match_object", "")
        if match_object:
            trip_offer = TripOffer.objects.filter(id=match_object).first()
            add_match(sender, trip_offer, trip_request, role="client")

        trip_request.notify_offers()
        return response

    def form_invalid(self, form):
        print("form_invalid", form.errors)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse("triprequest-detail", kwargs={"request_id": self.object.pk})


@method_decorator([never_cache, login_required], name="dispatch")
class UserProfileDetailView(DetailView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = "frontend/profile_view.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["navbar"] = "myprofile"
        profile_viewer = self.request.user
        profile_user = self.object.user
        if self.request.method == "GET":
            if profile_viewer != profile_user:
                viewer_offers_ids = profile_viewer.tripoffer_set.filter(
                    active=True
                ).values_list("id", flat=True)
                trip_requests = TripMatch.objects.filter(
                    offer__in=viewer_offers_ids,
                    status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED],
                ).values_list("request")
                has_common_offers = trip_requests.filter(
                    request__client=profile_user
                ).exists()

                viewer_requests_ids = profile_viewer.triprequest_set.filter(
                    active=True
                ).values_list("id", flat=True)
                trip_offers = TripMatch.objects.filter(
                    request__in=viewer_requests_ids, status=MatchStatus.ACCEPTED
                ).values_list("offer")
                print("trip_offers", trip_offers)
                has_common_requests = trip_offers.filter(
                    offer__driver=profile_user
                ).exists()
                ctx["can_see_private_details"] = (
                    has_common_requests or has_common_offers
                )
            else:
                ctx["can_see_private_details"] = True

        return ctx

    def get_object(self, queryset=None):
        username = self.kwargs.get("username")
        if username == "me":
            username = self.request.user.username
        return get_object_or_404(UserProfile, user__username=username)


@method_decorator([never_cache, login_required], name="dispatch")
class UserProfileUpdateView(UpdateView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = "frontend/profile_update_form.html"

    def get_object(self, queryset=None):
        username = self.kwargs.get("username")
        if username == "me":
            username = self.request.user.username
        return get_object_or_404(UserProfile, user__username=username)

    def get_success_url(self):
        return reverse("profile-view", kwargs={"username": self.request.user.username})


def get_all_tripoffer_geojson(request):
    json = []
    now = timezone.localtime()
    trip_results = TripOffer.objects.filter(active=True).filter(time_depart__gte=now)
    for offer in trip_results:
        if offer.route:
            json.append(
                {
                    "id": offer.id,
                    "origin": GeoJsonPoint(offer.origin_location),
                    "destination": GeoJsonPoint(offer.destination_location),
                }
            )
    return JsonResponse(json, safe=False)


def get_tripoffer_geojson(request, offer_id):
    offer = TripOffer.objects.get(id=offer_id)
    json = {
        "id": offer.id,
        "origin_name": offer.origin_name,
        "destination_name": offer.destination_name,
        "time_depart": offer.time_depart,
        "time_arrival": offer.time_arrival,
        "origin": GeoJsonPoint(offer.origin_location),
        "destination": GeoJsonPoint(offer.destination_location),
        "route": LineString(offer.route.coords),
    }
    return JsonResponse(json)


def get_all_triprequest_geojson(request):
    json = []
    now = timezone.localtime()

    trip_results = TripRequest.objects.filter(active=True).filter(
        Q(max_time_arrival__isnull=True) | Q(max_time_arrival__gte=now)
    )
    for trip_request in trip_results:
        if trip_request.origin_location and trip_request.destination_location:
            json.append(
                {
                    "id": trip_request.id,
                    "origin": GeoJsonPoint(trip_request.origin_location),
                    "destination": GeoJsonPoint(trip_request.destination_location),
                }
            )
    return JsonResponse(json, safe=False)


def get_triprequest_geojson(request, request_id):
    trip_request = TripRequest.objects.get(id=request_id)
    if not trip_request.route:
        print("Calculating route from trip request:", trip_request.id)
        trip_request.calculate_route()

    json = {
        "origin": GeoJsonPoint(trip_request.origin_location),
        "destination": GeoJsonPoint(trip_request.destination_location),
        "route": LineString(trip_request.route.coords),
    }
    return JsonResponse(json)


@csrf_exempt
def update_triprequest(request, request_id):
    can_update = False
    reason = None
    if request.method == "POST":
        action = request.POST["action"]
        logged_user = request.user
        trip_request = TripRequest.objects.filter(id=request_id).first()
        if trip_request:
            can_update = logged_user.is_superuser or logged_user == trip_request.client

        if can_update:
            if action == "delete":
                if trip_request.has_pending_requests:
                    reason = "PENDING_REQUESTS"
                    can_update = False
                else:
                    trip_request.delete()
            elif action == "inactive":
                trip_request.active = False
                trip_request.save()

    return JsonResponse({"status": can_update, "reason": reason})


@csrf_exempt
def update_tripoffer(request, offer_id):
    can_update = False
    to_delete = False

    if request.method == "POST":
        action = request.POST["action"]
        if action == "delete" or action == "deleteall":
            to_delete = True

        logged_user = request.user
        trip_offer = TripOffer.objects.filter(id=offer_id).first()
        all_deleting_offers = [trip_offer]

        if trip_offer:
            can_update = logged_user.is_superuser or logged_user == trip_offer.driver
            if action == "deleteall":
                # we add for deletion all the tripoffers in the same frequency group and still active
                all_deleting_offers = TripOffer.objects.filter(
                    frequency_group_id=trip_offer.frequency_group_id, active=True
                ).order_by("time_depart")

        print("all_deleting_offers", all_deleting_offers)
        if can_update and to_delete:
            for trip_offer in all_deleting_offers:
                offer_url = settings.DEFAULT_DOMAIN + reverse(
                    "tripoffer-detail", kwargs={"offer_id": trip_offer.id}
                )
                for match in trip_offer.matches.all():
                    trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                        "triprequest-detail", kwargs={"request_id": match.request.id}
                    )
                    subject = (
                        "S'ha eliminat la ruta associada a la teva petició de transport"
                    )
                    msg_body = f"La <a href='{offer_url}'>ruta</a> associada a la <a href='{trip_request_url}'>teva petició</a> de transport s'ha eliminat."

                    if match.status == MatchStatus.PENDING:
                        match.status = MatchStatus.REJECTED
                        match.save()
                        match.request.frozen = False
                        match.request.save()
                        send_notifications(
                            match.request.client,
                            match.request.client,
                            msg_body,
                            subject,
                            msg_body,
                        )

                    if match.status == MatchStatus.ACCEPTED:
                        match.status = MatchStatus.REJECTED
                        match.save()
                        match.request.frozen = False
                        match.request.save()
                        send_notifications(
                            match.request.client,
                            match.request.client,
                            msg_body,
                            subject,
                            msg_body,
                        )
                trip_offer.delete()

    return JsonResponse({"status": can_update})


def frontend_view(request):
    if request.user.is_authenticated:
        context = {
            "chat_unread_count": get_unseen_messages_count(request.user.pk),
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "navbar": "home",
            # "trip_results": TripOffer.objects.filter(active=True).prefetch_related('driver__userprofile'),
        }
    else:
        context = {"navbar": "home"}
        # "trip_results": TripOffer.objects.filter(active=True).prefetch_related('driver__userprofile')}

    response = render(request, "frontend/index.html", context)
    return response


def frequent_offers_view(request, frequency_group_id):
    offers = TripOffer.objects.filter(
        active=True, frequency_group_id=frequency_group_id
    ).order_by("time_depart")
    original_internal_name = ""
    if len(offers):
        o = offers.first()
        original_internal_name = o.internal_name[: o.internal_name.find("-")]

    context = {
        "original_internal_name": original_internal_name,
        "trip_results": offers,
        "frequency_group_id": frequency_group_id,
    }

    if not request.user.is_anonymous:
        trip_offers_ids = request.user.tripoffer_set.values_list("id", flat=True)
        trip_matches = TripMatch.objects.filter(offer__in=trip_offers_ids).order_by(
            "-modified"
        )
        context["trip_matches"] = trip_matches

    return render(request, "frontend/offers_frequent_view.html", context)


@login_required
def user_offers_view(request):
    user = request.user
    current_filter = request.GET.get("current_filter", "active")
    if current_filter != "inactive":
        sort_order = "time_depart"
        show_active = True
    else:
        sort_order = "-time_depart"
        show_active = False

    offers = user.tripoffer_set.filter(active=show_active).order_by(sort_order)
    if current_filter == "frequent":
        offers = offers.filter(frequency_group_id__isnull=False)
    trip_offers_ids = request.user.tripoffer_set.values_list("id", flat=True)
    trip_matches = TripMatch.objects.filter(offer__in=trip_offers_ids).order_by(
        "-modified"
    )

    context = {
        "chat_unread_count": get_unseen_messages_count(request.user.pk),
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "trip_results": offers,
        "trip_matches": trip_matches,
        "current_filter": current_filter,
    }

    return render(request, "frontend/user_offers_view.html", context)


@login_required
def user_requests_view(request):
    user = request.user
    show_active = (
        "show_active" not in request.GET or request.GET.get("show_active") == "True"
    )
    if show_active:
        trip_requests = (
            user.triprequest_set.filter(active=True)
            .annotate(max_date=Max("matches__modified"))
            .order_by("-max_date")
        )
    else:
        trip_requests = user.triprequest_set.filter(active=False).order_by(
            "-min_time_arrival"
        )

    trip_requests_ids = user.triprequest_set.values_list("id", flat=True)
    trip_matches = TripMatch.objects.filter(request__in=trip_requests_ids).order_by(
        "-modified"
    )

    context = {
        "chat_unread_count": get_unseen_messages_count(request.user.pk),
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "trip_results": trip_requests,
        "trip_matches": trip_matches,
        "show_active": show_active,
    }

    return render(request, "frontend/user_requests_view.html", context)


def first_requests_view(request):
    now = timezone.localtime()
    trip_results = (
        TripRequest.objects.filter(active=True)
        .filter(Q(max_time_arrival__isnull=True) | Q(max_time_arrival__gte=now))
        .order_by("-id")[:5]
    )
    context = {"trip_results": trip_results, "model": "trip_request"}
    context["now"] = timezone.localtime()
    # print("context", context)
    return render(request, "frontend/trip_results.html", context)


def first_offers_view(request):
    now = timezone.localtime()
    trip_results = (
        TripOffer.objects.filter(active=True)
        .filter(time_depart__gte=now)
        .order_by("time_depart")[:5]
    )
    context = {"trip_results": trip_results, "model": "trip_offer"}
    # print("context", context)
    return render(request, "frontend/trip_results.html", context)


def search_requests_view(request):
    context = {}
    route = None
    user = request.user
    if request.method == "GET":
        origin = request.GET["origin"].split(",")
        destination = request.GET["destination"].split(",")
        if origin[0] not in ["null", "undefined"]:
            point_origin = Point([float(origin[0]), float(origin[1])], srid=4326)
        else:
            point_origin = None
        if destination[0] not in ["null", "undefined"]:
            point_destination = Point(
                [float(destination[0]), float(destination[1])], srid=4326
            )
        else:
            point_destination = None

        transport_isothermic = (
            request.GET.get("transport_isothermic", "false") == "true"
        )
        transport_cold = request.GET.get("transport_cold", "false") == "true"
        transport_freezer = request.GET.get("transport_freezer", "false") == "true"
        transport_dry = request.GET.get("transport_dry", "false") == "true"

        id_time_depart = request.GET["id_time_depart"]
        if id_time_depart == "":
            time_pickup = None
        else:
            time_pickup = timezone.datetime.strptime(id_time_depart, "%d/%m/%Y %H:%M")
            time_pickup = pytz.timezone(settings.TIME_ZONE).localize(
                time_pickup, is_dst=True
            )
        tags = request.GET["tags"].split(",")
        if tags == [""]:
            tags = []

        # search 20km by deafult
        trip_results = search_trip_requests(
            user,
            route,
            point_origin,
            point_destination,
            transport_isothermic,
            transport_cold,
            transport_freezer,
            transport_dry,
            time_pickup,
            tags,
            20,
            exclude_user=False,
        )

        context = {"trip_results": trip_results, "model": "trip_request"}
        context["now"] = timezone.localtime()
        # print("context", context)
        return render(request, "frontend/trip_results.html", context)


def search_offers_view(request):
    context = {}
    user = request.user
    if request.method == "GET":
        print(request.GET)
        origin = request.GET["origin"].split(",")
        destination = request.GET["destination"].split(",")
        transport_isothermic = (
            request.GET.get("transport_isothermic", "false") == "true"
        )
        transport_cold = request.GET.get("transport_cold", "false") == "true"
        transport_freezer = request.GET.get("transport_freezer", "false") == "true"
        transport_dry = request.GET.get("transport_dry", "false") == "true"
        tags = request.GET["tags"].split(",")
        if tags == [""]:
            tags = []

        id_time_depart = request.GET["id_time_depart"]
        if id_time_depart == "":
            time_pickup = timezone.localtime()
        else:
            time_pickup = timezone.datetime.strptime(id_time_depart, "%d/%m/%Y %H:%M")
            time_pickup = pytz.timezone(settings.TIME_ZONE).localize(
                time_pickup, is_dst=True
            )

        trip_results = search_trip_offers(
            user,
            origin,
            destination,
            transport_isothermic,
            transport_cold,
            transport_freezer,
            transport_dry,
            time_pickup,
            None,
            tags,
            exclude_user=False,
        )

        context = {"trip_results": trip_results, "model": "trip_offer"}
        print("context", context)
        return render(request, "frontend/trip_results.html", context)


@login_required
def user_storages_view(request):
    user = request.user

    storages = user.storage_set.filter(deleted=False).order_by("-updated_at")

    context = {
        "chat_unread_count": get_unseen_messages_count(request.user.pk),
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "storages": storages,
    }

    return render(request, "frontend/user_storages_view.html", context)


@method_decorator([never_cache, login_required], name="dispatch")
class StorageUpdateView(UpdateView):
    model = Storage
    form_class = StorageForm
    template_name = "frontend/storage_add_form.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("storage_id")
        return get_object_or_404(Storage, id=object_id)

    def get_context_data(self, **kwargs):
        object_id = self.kwargs.get("storage_id")
        object = Storage.objects.get(id=object_id)

        ctx = super().get_context_data(**kwargs)
        ctx["origin_location"] = object.origin_location
        ctx["origin_name"] = object.origin_name
        ctx["transport_isothermic"] = object.transport_isothermic
        ctx["transport_cold"] = object.transport_cold
        ctx["transport_freezer"] = object.transport_freezer
        ctx["transport_dry"] = object.transport_dry
        ctx["notes"] = object.notes

        if self.request.method == "GET":
            ctx["time_depart"] = self.request.GET.get("time_depart", None)

        if self.request.method == "POST":
            ctx["is_public"] = self.request.POST.get("is_public", False)

        print("ctx", ctx)

        tags = list(object.tags.all())

        if tags:
            ctx["tags"] = ",".join([tag.name for tag in tags])
        else:
            ctx["tags"] = ""
        return ctx

    def form_valid(self, form):
        storage = form.instance
        response = super().form_valid(form)

        tags = form.cleaned_data.get("tags", "").split(",")
        for tag in storage.tags.all():
            tag.delete()
        for tag in tags:
            storage.tags.add(tag)

        storage.save()

        return response

    def get_success_url(self):
        return reverse("storage-detail", kwargs={"storage_id": self.object.pk})


@method_decorator([never_cache, login_required], name="dispatch")
class StorageAddView(CreateView):
    model = Storage
    form_class = StorageForm
    template_name = "frontend/storage_add_form.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # ctx["navbar"] = "request_add"
        print("self.request.method", self.request.method)

        if self.request.method == "POST":
            ctx["origin_location"] = self.request.POST.get("origin_location", None)
            ctx["destination_location"] = self.request.POST.get(
                "destination_location", None
            )
            ctx["match_object"] = self.request.POST.get("match_object", None)
            ctx["transport_isothermic"] = self.request.POST.get(
                "transport_isothermic", False
            )
            ctx["transport_cold"] = self.request.POST.get("transport_cold", False)
            ctx["transport_freezer"] = self.request.POST.get("transport_freezer", False)
            ctx["transport_dry"] = self.request.POST.get("transport_dry", False)
            ctx["accepts_pallets"] = self.request.POST.get("accepts_pallets", False)
            ctx["is_public"] = self.request.POST.get("is_public", False)

            print("POST Storage", ctx)

        return ctx

    def form_valid(self, form):
        print("StorageAddView form.cleaned_data", form.cleaned_data)
        storage = form.instance
        sender = self.request.user
        response = super().form_valid(form)
        storage.creator = sender

        tags = form.cleaned_data.get("tags", "").split(",")
        for tag in storage.tags.all():
            tag.delete()
        for tag in tags:
            storage.tags.add(tag)

        storage.save()
        return response

    def form_invalid(self, form):
        print("form_invalid", form.errors)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse("storage-detail", kwargs={"storage_id": self.object.pk})


@method_decorator([never_cache], name="dispatch")
class StorageDetailView(DetailView):
    model = Storage
    form_class = StorageForm
    template_name = "frontend/storage_view_form.html"

    def get_object(self, queryset=None):
        object_id = self.kwargs.get("storage_id")
        return get_object_or_404(Storage, id=object_id)


def get_all_storage_geojson(request):
    json = []
    print("request.user", request.user)
    if request.user.is_authenticated:
        storages = Storage.objects.filter(active=True, deleted=False).filter(
            Q(is_public=True) | Q(is_public=False, creator=request.user)
        )
        for storage in storages:
            body = icons_html = ""
            if storage.transport_isothermic:
                icons_html += '<span title="Isoterm"><i class="fas fa-temperature-high" aria-hidden="true"></i></span>'
            if storage.transport_cold:
                icons_html += '<span title="Refrigerat"><i class="fas fa-thermometer-quarter" aria-hidden="true"></i></span>'
            if storage.transport_freezer:
                icons_html += '<span title="Congelat"><i class="fas fa-snowflake" aria-hidden="true"></i></span>'
            if storage.transport_dry:
                icons_html += '<span title="Sense humitat"><i class="fas fa-sun" aria-hidden="true"></i></span>'
            if storage.accepts_pallets:
                icons_html += '<span title="Disposa de toro per a la descàrrega de palets"><i class="fa fa-arrow-down" aria-hidden="true"></i></span>'
            if storage.notes:
                body += f"<b>{storage.origin_name}</b><br>{storage.notes}"

            json.append(
                {
                    "id": storage.id,
                    "location": GeoJsonPoint(storage.origin_location),
                    "name": storage.name,
                    "is_public": storage.is_public,
                    "icons_html": icons_html,
                    "body": body,
                }
            )
    return JsonResponse(json, safe=False)


def get_storage_geojson(request, storage_id):
    storage = Storage.objects.get(id=storage_id)
    json = {
        "id": storage.id,
        "origin_name": storage.name,
        "origin": GeoJsonPoint(storage.origin_location),
    }
    return JsonResponse(json)


@csrf_exempt
def update_storage(request, object_id):
    can_update = False
    reason = None
    if request.method == "POST":
        action = request.POST["action"]
        logged_user = request.user
        object = Storage.objects.filter(id=object_id).first()
        if object:
            can_update = logged_user.is_superuser or logged_user == object.creator

        if can_update:
            if action == "delete":
                object.deleted = True
            elif action == "inactive":
                object.active = False
            object.save()

    return JsonResponse({"status": can_update, "reason": reason})


# End Storage Views


def make_notification(request):
    notify.send(
        actor=None,
        sender=request.user,
        recipient=request.user,
        verb="test de notificació",
    )
    return HttpResponse(status=200)


@csrf_exempt
def update_match_status(request, match_id):
    if request.method == "POST":
        sender = request.user
        new_status = request.POST["status"]
        comment = request.POST.get("comment", None)
        match = TripMatch.objects.get(id=match_id)
        match.status = new_status
        match.save()
        trip_request_url = settings.DEFAULT_DOMAIN + reverse(
            "triprequest-detail", kwargs={"request_id": match.request.id}
        )
        offer_url = settings.DEFAULT_DOMAIN + reverse(
            "tripoffer-detail", kwargs={"offer_id": match.offer.id}
        )
        username_url = settings.DEFAULT_DOMAIN + reverse(
            "profile-view", kwargs={"username": sender.username}
        )

        if new_status == MatchStatus.ACCEPTED:
            match.request.frozen = True
            match.request.save()

            subject = f"{sender.username} ha acceptat la teva petició de transport"
            msg_notif = f"La <a href='{offer_url}'>ruta</a> ha acceptat la <a href='{trip_request_url}'>teva petició de transport</a>."
            msg_body = (
                f"El/la <a href='{username_url}'>transportista ({sender.username})</a> de la <a href='{offer_url}'>ruta</a> "
                f"ha acceptat la <a href='{trip_request_url}'>teva petició</a> de transport."
            )
            send_notifications(
                sender, match.request.client, msg_notif, subject, msg_body
            )

        elif new_status == MatchStatus.REJECTED:
            match.request.frozen = False
            match.request.save()
            if comment:
                match.driver_comment = comment
                match.save()
            msg = f"({sender.username}) ha declinat la teva petició de transport"
            msg_notif = f"La <a href='{offer_url}'>ruta</a> ha declinat la <a href='{trip_request_url}'>teva petició de transport</a>."
            msg_details = (
                f"El/la <a href='{username_url}'>transportista ({sender.username})</a> de la <a href='{offer_url}'>ruta</a>"
                f" ha declinat la <a href='{trip_request_url}'>teva petició de transport</a>:<br>"
                f"<br><b>Comentari</b>: {comment}"
            )

            send_notifications(
                sender, match.request.client, msg_notif, msg, msg_details
            )
        elif new_status == MatchStatus.CANCELED:
            match.request.frozen = False
            match.request.save()
            if comment:
                match.client_comment = comment
                match.save()
            subject = (
                f"{sender.username} ha cancel·lat la seva petició amb la teva ruta"
            )
            msg_notif = f"{sender.username} ha cancel·lat la seva comanda amb la <a href='{offer_url}'>teva ruta</a>."
            msg_body = (
                f"<a href='{username_url}'>{sender.username}</a>"
                f" ha cancel·lat la <a href='{trip_request_url}'>seva petició</a> de transport"
                f" amb la <a href='{offer_url}'>teva ruta</a>."
            )
            send_notifications(sender, match.offer.driver, msg_notif, subject, msg_body)

        print("Match status changed to {}".format(new_status))

        return HttpResponse(status=200)
    else:
        return HttpResponse(status=400)


@login_required
def tripoffer_take_logisticnode(request, offer_id, logisticnode_id, point):
    logisticnode = get_object_or_404(LogisticNode, id=logisticnode_id)
    object = get_object_or_404(TripOffer, id=offer_id)
    if not object or not logisticnode:
        return HttpResponse(status=400)

    if point == "origin":
        # We replace the origin with the location of the logisticnode while keeping the original origin in the last_mile
        object.origin_logisticnode = logisticnode
        object.origin_last_mile_name = object.origin_name
        object.origin_last_mile_location = object.origin_location
        object.origin_name = logisticnode.origin_name
        object.origin_location = logisticnode.origin_location
        object.save()

        return HttpResponseRedirect(
            reverse("tripoffer-detail", kwargs={"offer_id": offer_id})
        )

    elif point == "destination":
        # We replace the destination with the location of the logisticnode while keeping the original destination in the last_mile
        object.destination_logisticnode = logisticnode
        object.destination_last_mile_name = object.destination_name
        object.destination_last_mile_location = object.destination_location
        object.destination_name = logisticnode.origin_name
        object.destination_location = logisticnode.origin_location
        object.save()

        return HttpResponseRedirect(
            reverse("tripoffer-detail", kwargs={"offer_id": offer_id})
        )
    else:
        return HttpResponse(status=400)


@login_required
def triprequest_take_logisticnode(request, request_id, logisticnode_id, point):
    # Attempt to retrieve the logistic node
    logisticnode = LogisticNode.objects.filter(id=logisticnode_id).first()
    if logisticnode is None:
        return HttpResponse("LogisticNode not found.", status=404)

    # Attempt to retrieve the trip request
    triprequest = TripRequest.objects.filter(id=request_id).first()
    if triprequest is None:
        return HttpResponse("TripRequest not found.", status=404)

    if point == "origin":
        # We replace the origin with the location of the logisticnode while keeping the original origin in the last_mile
        triprequest.origin_logisticnode = logisticnode
        triprequest.origin_last_mile_name = triprequest.origin_name
        triprequest.origin_last_mile_location = triprequest.origin_location
        triprequest.origin_name = logisticnode.origin_name
        triprequest.origin_location = logisticnode.origin_location
        triprequest.save()

        return HttpResponseRedirect(
            reverse("triprequest-detail", kwargs={"request_id": request_id})
        )

    elif point == "destination":
        # We replace the destination with the location of the logisticnode while keeping the original destination in the last_mile
        triprequest.destination_logisticnode = logisticnode
        triprequest.destination_last_mile_name = triprequest.destination_name
        triprequest.destination_last_mile_location = triprequest.destination_location
        triprequest.destination_name = logisticnode.origin_name
        triprequest.destination_location = logisticnode.origin_location
        triprequest.save()

        return HttpResponseRedirect(
            reverse("triprequest-detail", kwargs={"request_id": request_id})
        )
    else:
        return HttpResponse(status=400)


@login_required
def convert_request_to_offer(request, request_id):
    if request.method == "GET":
        obj = get_object_or_404(TripRequest, id=request_id)
        if obj.has_pending_requests:
            trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                "triprequest-detail", kwargs={"request_id": request_id}
            )
            return HttpResponse(
                f"S'han de cancel·lar les <a href='{trip_request_url}'>rutes demanades</a> abans d'eliminar la comanda (de convertir-la en ruta).",
                status=403,
            )
        now = timezone.localtime()
        print(f"Converting trip request {obj.id} to offer.")
        if obj.min_time_arrival < now:
            time_depart = now + timezone.timedelta(days=1)
            time_arrival = (
                now + timezone.timedelta(days=1) + timezone.timedelta(minutes=1 * 60)
            )
        else:
            time_depart = obj.min_time_arrival
            time_arrival = obj.min_time_arrival + timezone.timedelta(minutes=1 * 60)

        trip_offer = TripOffer.objects.create(
            origin_name=obj.origin_name,
            origin_location=obj.origin_location,
            destination_name=obj.destination_name,
            destination_location=obj.destination_location,
            internal_name=obj.internal_name,
            driver=obj.client,
            tags=obj.tags,
            comment=obj.comment,
            time_depart=time_depart,
            time_arrival=time_arrival,
            transport_isothermic=obj.transport_isothermic,
            transport_dry=obj.transport_dry,
            transport_cold=obj.transport_cold,
            transport_freezer=obj.transport_freezer,
        )

        obj.delete()

        return HttpResponseRedirect(
            reverse("tripoffer-edit", kwargs={"offer_id": trip_offer.id})
        )


@csrf_exempt
@login_required
@never_cache
def view_points(request, username):
    logged_user = request.user
    sender = logged_user.username
    userprofile = UserProfile.objects.get(user__username=username)
    logged_user.userprofile.refresh_from_db()
    sender_points = logged_user.userprofile.points

    if request.method == "POST":
        print("request.POST", request.POST)

        try:
            amount = int(request.POST.get("amount_to_send"))
            if sender_points >= amount:
                username_url = settings.DEFAULT_DOMAIN + reverse(
                    "profile-view", kwargs={"username": username}
                )
                pay(
                    logged_user,
                    -1 * amount,
                    f"Pagament enviat a <a href='{username_url}'>{username}</a>",
                )

                sender_url = settings.DEFAULT_DOMAIN + reverse(
                    "profile-view", kwargs={"username": sender}
                )
                pay(
                    userprofile.user,
                    amount,
                    f"Pagament rebut de <a href='{sender_url}'>{sender}</a>",
                )
                messages.success(
                    request,
                    f"Pagament de {amount} RUCS a {username} efectuat correctament.",
                )
            else:
                messages.error(
                    request,
                    f"No tens suficients RUCs per a fer el pagament.",
                )
        except Exception as e:
            print(e)
            messages.error(
                request,
                f"Error amb la quantitat a pagar.",
            )

    logged_user.userprofile.refresh_from_db()
    sender_points = logged_user.userprofile.points
    payment_notifs = userprofile.user.notifications.filter(data__isnull=False)
    context = {
        "chat_unread_count": get_unseen_messages_count(request.user.pk),
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "same_user": sender == username,
        "username": username,
        "user_points": userprofile.points,
        "sender_points": sender_points,
        "transactions": payment_notifs,
    }

    return render(request, "frontend/points_view.html", context)


@login_required
def update_confirmation_delivery(request, match_id):
    sender = request.user
    match = TripMatch.objects.get(id=match_id)
    print("match", match)
    print("request.method", request.method)
    if request.method == "GET":

        if not (sender == match.offer.driver or sender == match.request.client):
            return HttpResponse("Sense permís.", status=403)

        form = ConfirmDeliveryForm()
        context = {
            "chat_unread_count": get_unseen_messages_count(request.user.pk),
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "match": match,
            "form": form,
        }

        return render(request, "frontend/confirm_delivery_form.html", context)

    elif request.method == "POST":
        context = {}
        context = {
            "chat_unread_count": get_unseen_messages_count(request.user.pk),
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "match": match,
        }
        if not match.delivery_confirmed:
            if match.offer.driver == sender:
                form = ConfirmDeliveryForm(request.POST, request.FILES)
                print("form", form)
                print("request.POST", request.POST)
                print("request.FILES", request.FILES)
                print("form.is_valid()", form.is_valid())

                if form.is_valid():
                    print("Match delivered by driver")
                    context["form"] = form
                    match.delivery_comment = request.POST.get("delivery_comment", None)
                    match.delivery_signature = request.FILES.get(
                        "delivery_signature", None
                    )
                    match.delivery_photo = request.FILES.get("delivery_photo", None)
                    match.delivery_date = timezone.localtime()
                    match.delivery_confirmed = True
                    match.status = MatchStatus.COMPLETED
                    match.request.active = False
                    match.request.save()
                    match.save()

                    trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                        "triprequest-detail", kwargs={"request_id": match.request.id}
                    )
                    driver = match.offer.driver.username
                    msg = f"{driver} ha confirmat l'entrega de la teva petició de transport."
                    msg_notif = f"{driver} ha confirmat l'entrega de <a href='{trip_request_url}'>la teva petició de transport</a>"

                    msg_body = (
                        f"{driver} ha confirmat l'entrega de <a href='{trip_request_url}'>la teva petició de transport</a>"
                        f"<br><br>Hora: {match.delivery_date}<br>"
                    )
                    if match.delivery_signature:
                        msg_body += f'<br><label>Signatura del/de la receptor/a:</label> <a target="_blank" href="{settings.DEFAULT_DOMAIN}/media/{match.delivery_signature}">{match.delivery_signature}</a>'
                    if match.delivery_photo:
                        msg_body += f'<br><label>Foto de l\'entrega:</label> <a target="_blank" href="{settings.DEFAULT_DOMAIN}/media/{match.delivery_photo}">{match.delivery_photo}</a>'

                    send_notifications(
                        sender, match.request.client, msg_notif, msg, msg_body
                    )

                return render(request, "frontend/confirm_delivery_form.html", context)
        else:
            return render(request, "frontend/confirm_delivery_form.html", context)

    else:
        return HttpResponse("Sense permís", status=403)


@login_required
def update_match_review(request, match_id):
    sender = request.user
    print("request.method", request.method)
    if request.method == "GET":
        match = TripMatch.objects.get(id=match_id)
        if match.status == MatchStatus.COMPLETED:
            if sender == match.offer.driver:
                role = "driver"
                reviewed_user = match.request.client
            elif sender == match.request.client:
                role = "client"
                reviewed_user = match.offer.driver
            else:
                return HttpResponse(
                    "El teu usuari no té permís per a valorar aquesta ruta.", status=403
                )
        else:
            return HttpResponse(
                "Ja ha finalitzat el termini per fer la valoració.", status=403
            )

        context = {
            "chat_unread_count": get_unseen_messages_count(request.user.pk),
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "match": match,
            "role": role,
            "reviewed_user": reviewed_user,
        }

        return render(request, "frontend/review_match_view.html", context)

    elif request.method == "POST":
        print("request.POST", request.POST)
        match = TripMatch.objects.get(id=match_id)
        comment = request.POST["comment"]
        rating = request.POST["rating"]
        role = request.POST["role"]
        client_failed = False
        driver_failed = False
        if "client_failed" in request.POST and request.POST["client_failed"] == "on":
            client_failed = True
        if "driver_failed" in request.POST and request.POST["driver_failed"] == "on":
            driver_failed = True

        if match.offer.driver == sender and role == "driver":
            print("Match reviewed by driver")
            match.driver_comment = comment
            match.driver_rating = rating
            match.driver_rating_date = timezone.localtime()
            match.client_failed = client_failed
            match.save()

        if match.request.client == sender and role == "client":
            print("Match reviewed by client")
            match.client_comment = comment
            match.client_rating = rating
            match.client_rating_date = timezone.localtime()
            match.driver_failed = driver_failed
            match.save()

        if match.driver_comment and match.client_comment:
            print(f"Congelant les valoracions del match {match.id}")
            match.freeze_reviews()

        if role == "driver":
            return HttpResponseRedirect(
                reverse("tripoffer-user-view") + "?show_active=False"
            )
        if role == "client":
            return HttpResponseRedirect(
                reverse("triprequest-user-view") + "?show_active=False"
            )


def add_match(sender, trip_offer, trip_request, role):
    can_match = False
    reason = ""
    offer_url = settings.DEFAULT_DOMAIN + reverse(
        "tripoffer-detail", kwargs={"offer_id": trip_offer.id}
    )
    trip_request_url = settings.DEFAULT_DOMAIN + reverse(
        "triprequest-detail", kwargs={"request_id": trip_request.id}
    )
    match = TripMatch.objects.filter(request=trip_request, offer=trip_offer).filter(
        Q(status=MatchStatus.PENDING) | Q(status=MatchStatus.ACCEPTED)
    )
    if match:
        can_match = False
        reason = "ALREADY_MATCHED"
    else:
        if role == "driver" and sender == trip_offer.driver:
            can_match = True
            new_status = MatchStatus.ACCEPTED
            msg = f"{sender.username} ha acceptat la teva petició de transport."
            msg_notif = f"{sender.username} ha acceptat <a href='{offer_url}'>la teva petició de transport</a>"
            msg_body = (
                f"{sender.username} ha acceptat <a href='{offer_url}'>la teva petició de transport</a>"
                f"<br><br>De: {trip_offer.time_depart}: {trip_offer.origin_name}<br>"
                f"A:  {trip_offer.time_arrival}: {trip_offer.destination_name}"
            )

            send_notifications(sender, trip_request.client, msg_notif, msg, msg_body)
        elif role == "client" and sender == trip_request.client:
            can_match = True
            new_status = MatchStatus.PENDING
            msg = (
                f"{sender.username} ha fet una petició de transport per a la teva ruta"
            )
            msg_notif = f"{sender.username} ha fet <a href='{trip_request_url}'>una petició de transport</a> per a <a href='{offer_url}'>la teva ruta</a>"
            msg_body = (
                f"{sender.username} ha fet <a href='{trip_request_url}'>una petició de transport</a> per a <a href='{offer_url}'>la teva ruta</a>:"
                f"<br><br>De: {trip_offer.time_depart}: {trip_offer.origin_name}<br>"
                f"A:  {trip_offer.time_arrival}: {trip_offer.destination_name}"
            )
            send_notifications(sender, trip_offer.driver, msg_notif, msg, msg_body)

    if can_match:
        TripMatch.objects.create(
            request=trip_request, offer=trip_offer, status=new_status
        )
        trip_request.frozen = True
        trip_request.save()

    return can_match, reason


@csrf_exempt
def add_match_view(request):
    can_match = False
    reason = ""
    if request.method == "GET":
        sender = request.user
        print("request.GET", request.GET)
        offer_id = request.GET["offer_id"]
        request_id = request.GET["request_id"]
        role = request.GET["role"]
        trip_offer = TripOffer.objects.get(id=offer_id)
        trip_request = TripRequest.objects.get(id=request_id)
        can_match, reason = add_match(sender, trip_offer, trip_request, role)

    return JsonResponse({"status": can_match, "reason": reason})


@csrf_exempt
def page_endpoints(request, pagename):
    # TEMPLATE_DIR = os.path.join(settings.BASE_DIR, "templates")
    context = {}
    page = Page.objects.filter(url=pagename)
    if not page:
        return HttpResponse("Pàgina no trobada.")
    else:
        template_name = "frontend/basepage.html"

        context = {
            "content": page.first().content,
            "meta_opengraph": "Metadata info",
            "navbar": pagename,
        }

        if request.user.is_authenticated:
            context["chat_unread_count"] = get_unseen_messages_count(request.user.pk)
            context["unread_count"] = request.user.notifications.unread().count()
            context["notifications"] = request.user.notifications.all()

        return render(request, template_name, context)
