# -*- coding: utf-8 -*-

from django.db import models
from django import forms
from django.contrib.postgres.fields import ArrayField


class CharFieldWithBigTextarea(models.TextField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 50, "cols": 100})})
        return super().formfield(**kwargs)


class CharFieldWithTextarea(models.CharField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 3, "cols": 100})})
        return super(CharFieldWithTextarea, self).formfield(**kwargs)


class ChoiceArrayField(ArrayField):
    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
            "label": self.base_field.verbose_name,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)
