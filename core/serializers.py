from rest_framework import serializers
from .models import TripOffer, TripRequest, TripMatch
from django.contrib.auth.models import User


class TripOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripOffer
        fields = "__all__"  # You can specify the fields you want to serialize here


class TripRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripRequest
        fields = "__all__"  # You can specify the fields you want to serialize here


class TripMatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripMatch
        fields = (
            "__all__"  # O especifica los campos que deseas incluir en el serializador
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email", "password", "first_name", "last_name")
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user
