from django.conf import settings


def globals_processor(request):
    globals = {"DEV": settings.DEV, "APP_VERSION": settings.APP_VERSION}
    return {"globals": globals}
