from django import template
from django.utils.html import escapejs
from core.models import TripOffer, TripRequest

register = template.Library()


@register.simple_tag
def update_total_matches():
    """Allows to update existing variable in template"""
    total_matches = 1
    return total_matches


#Direction
TO_ORIGIN = 0
TO_DESTINATION = 1
TO_BOTH = 2

@register.simple_tag
def format_tripobject_to_coopcycle_parameters(obj, node, direction=TO_ORIGIN):
    comment = obj.comment if obj.comment else ''
    packages_weight = 1
    packages_num = 1

    if type(obj) is TripOffer:
        user = obj.driver
        delivery_details = comment
    elif type(obj) is TripRequest:
        user = obj.client
        delivery_contact = obj.delivery_contact if obj.delivery_contact else ''
        delivery_details = " ".join([comment, delivery_contact])
        packages_weight = round(obj.packages_weight)
        packages_num = obj.packages_num
    email = user.email
    name = user.first_name + ' ' + user.last_name
    phone = user.userprofile.phone if user.userprofile.phone else ''
    print('format_tripobject_to_coopcycle_parameters', obj, node, direction)
    if direction == TO_ORIGIN:
        origin_name = obj.origin_last_mile_name
        origin_location = obj.origin_last_mile_location
        destination_name = obj.origin_name
        destination_location = obj.origin_location
    elif direction == TO_DESTINATION:
        origin_name = obj.destination_name
        origin_location = obj.destination_location
        destination_name = obj.destination_last_mile_name
        destination_location = obj.destination_last_mile_location
    elif direction == TO_BOTH:
        origin_name = obj.origin_name
        origin_location = obj.origin_location
        destination_name = obj.destination_name
        destination_location = obj.destination_location
        
    params = [
        origin_name,
        origin_location.y,
        origin_location.x,
        destination_name,
        destination_location.y,
        destination_location.x,
        delivery_details,
        packages_weight,
        packages_num,
        name,
        email,
        phone
    ]
    return ",".join(f"'{escapejs(str(item))}'" for item in params)
