from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.measure import Distance
from django.contrib.gis.geos import Point, LineString, fromstr

#https://wiki.openstreetmap.org/wiki/Routing/online_routers
import os
os.environ["GMAPS_KEY"] = "xxxxxxxxxxxxxxxxxxxx"

#https://routing.openstreetmap.de/routed-car/route/v1/driving/-0.2152033,39.0331946;2.1774322,41.3828939;0.6267842,41.6147605?overview=full&alternatives=false&steps=false&geometries=geojson

#https://geojsonlint.com/
import route_json

#
# Google Earth is in a Geographic coordinate system with the wgs84 datum. (EPSG: 4326)
#
# Google Maps is in a projected coordinate system that is based on the wgs84 datum. (EPSG 3857)
#
# The data in Open Street Map database is stored in a gcs with units decimal degrees & datum of wgs84. (EPSG: 4326)
#
# The Open Street Map tiles and the WMS webservice, are in the projected coordinate system that is based on the wgs84 datum. (EPSG 3857)

class Command(BaseCommand):

    def handle(self, *args, **options):
        p1 = Point([41.382955, 2.180049], srid=4326).transform(3875, clone=True)
        #p1 = fromstr('POINT(39.02625 -0.21477)', srid=4326).transform(900913, clone=True)
        #p2 = fromstr('POINT(39.02842 -0.21595)', srid=4326).transform(900913, clone=True)
        route2 = []
        for r in route_json.route:
            route2.append([r[1], r[0]])
        ln = LineString(route2, srid=4326).transform(3875, clone=True)
        lleida = Point([41.61741947976564, 0.6286720935843313], srid=4326).transform(3875, clone=True)
        # p3 = fromstr('POINT(-0.214891 39.027523)', srid=4326).transform(900913, clone=True)
        print("Distància a lleida", ln.distance(lleida))
        print("Distància entre origen, fin", ln.distance(p1))
        # print("Distància entre origen, intermedio", p2.distance(p3))
