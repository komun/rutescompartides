from django.conf import settings
from core.models import TripOffer, TripRequest, TripMatch, MatchStatus
from django.utils import timezone
from django import template
from django.shortcuts import reverse

from core.utils import bot, chat_id, create_hashtags, send_html_email
from chat.models import Messages
import datetime


MINUTES_NOTIFY_UNREAD_CHATS = 25
def mail_notification_unread_chats():
    """
    Send a mail to those user that has unread chat messages.
    """
    # Get unread messages older than 15 minutes
    unread_messages = Messages.objects.filter(
        seen=False,
        unseen_notif=False,
        timestamp__lte=timezone.now() - datetime.timedelta(minutes=MINUTES_NOTIFY_UNREAD_CHATS)
    )

    # Collect unique email addresses of receivers
    unique_receivers = set(message.receiver_name.email for message in unread_messages)

    if unique_receivers:
        # Compose the summary email body
        subject = "Algú t'ha escrit a RutesCompartides.cat"
        html = 'Tens missatges de Xat a RutesCompartides.cat:\n\n'
        html += f'Pots vore-los a la secció de Xat pots veure’ls a la secció de Xat clicant <a href="{settings.DEFAULT_DOMAIN}/chat/">aquí</a>.'

        # Mark all messages as seen to avoid sending multiple emails
        unread_messages.update(unseen_notif=True)
        receivers = list(unique_receivers)
        send_html_email(receivers, subject, html)

        print(f'Successfully sent notif chat email to {", ".join(receivers)}')
    else:
        print('No unread messages to notify about.')



def autopublish_telegram():
    """
    Autopublish trip offers and trip requests to telegram
    """
    if not settings.DEV:
        # Calcula la fecha mínima para el intervalo de dos semanas
        now = timezone.now()
        two_weeks_from_now = now + timezone.timedelta(weeks=2)
        two_weeks_before_now = now - timezone.timedelta(weeks=2)

        # Filtra los registros de TripOffer que cumplen con la condición
        tripoffers = TripOffer.objects.filter(telegram_published__isnull=True, time_depart__range=(now, two_weeks_from_now)).order_by('time_depart')

        # Itera sobre los registros filtrados y realiza las operaciones necesarias
        print('Checking Tripoffers notifications...', tripoffers)
        for tripoffer in tripoffers:
            tripoffer.telegram_published = now
            tripoffer.save()
            username_url = settings.DEFAULT_DOMAIN + reverse(
                "profile-view", kwargs={"username": tripoffer.driver.username}
            )
            # Realiza las acciones deseadas para cada registro
            print("TripOffer ID:", tripoffer.id, "- Depart Time:", tripoffer.time_depart)
            offer_url = settings.DEFAULT_DOMAIN + reverse(
                "tripoffer-detail", kwargs={"offer_id": tripoffer.id}
            )
            t = template.loader.get_template("frontend/bot_notification_offer.md")
            c = {
                "trip_offer": tripoffer,
                "edited": False,
                "offer_url": offer_url,
                "username_url": username_url,
                "hashtags": create_hashtags(tripoffer),
            }
            bot_msg = t.render(c)
            bot_msg = bot_msg.replace("&#x27;", "'")
            bot.send_message(chat_id, bot_msg)

        # Filtra los registros de TripRequests que cumplen con la condición
        triprequests = TripRequest.objects.filter(telegram_published__isnull=True, min_time_arrival__range=(now, two_weeks_from_now)).order_by('min_time_arrival')
        # Itera sobre los registros filtrados y realiza las operaciones necesarias
        print('Checking TripRequest notifications...', triprequests)
        for triprequest in triprequests:
            triprequest.telegram_published = now
            triprequest.save()
            # Realiza las acciones deseadas para cada registro
            trip_request_url = settings.DEFAULT_DOMAIN + reverse(
                    "triprequest-detail", kwargs={"request_id": triprequest.id}
                )
            username_url = settings.DEFAULT_DOMAIN + reverse(
                "profile-view", kwargs={"username": triprequest.client.username}
            )
            print("TripRequest ID:", triprequest.id, "- Minimum Depart Time:", triprequest.min_time_arrival)
            t = template.loader.get_template("frontend/bot_notification_request.md")
            c = {
                "trip_request": triprequest,
                "edited": False,
                "trip_request_url": trip_request_url,
                "username_url": username_url,
                "hashtags": create_hashtags(triprequest),
            }
            bot_msg = t.render(c)
            bot_msg = bot_msg.replace("&#x27;", "'")
            bot.send_message(chat_id, bot_msg)



def autofrozen_job():
    # tripoffer_total = TripOffer.objects.filter(active=True).count()
    # print(f'There are active {tripoffer_total} offers.')
    # triprequest_total = TripRequest.objects.filter(active=True).count()
    # print(f'There are active {triprequest_total} requests.')

    tripmatches = TripMatch.objects.filter(status=MatchStatus.COMPLETED)
    print(f"There are completed {tripmatches.count()} matches.")
    now = timezone.localtime()
    for match in tripmatches:
        print(
            "match.max_time_rating",
            match.id,
            match.max_time_rating,
            match.times_reminded_rate,
        )
        if match.max_time_rating:
            if match.max_time_rating < now:
                print(f"Congelant les valoracions del match {match.id}")
                match.freeze_reviews()
            elif match.times_reminded_rate == 0 and (
                match.max_time_rating
                - timezone.timedelta(
                    minutes=settings.MINUTES_BEFORE_REVIEW_FREEZES_REMINDER
                )
                < now
            ):
                print(f"Enviant recordatori per a les valoracions del match {match.id}")
                match.send_review_reminder()


def autofinalize_job():
    now = timezone.localtime()

    tripoffers = TripOffer.objects.filter(active=True)
    for offer in tripoffers:
        print(f"Offer {offer.id} time_arrive {offer.time_arrival}")
        print("now", now)
        print(
            "offer.time_arrival + timezone.timedelta(minutes=settings.MINUTES_AFTER_OFFERS_FINALIZES)",
            offer.time_arrival
            + timezone.timedelta(minutes=settings.MINUTES_AFTER_OFFERS_FINALIZES),
        )
        if (
            offer.time_arrival
            + timezone.timedelta(minutes=settings.MINUTES_AFTER_OFFERS_FINALIZES)
            < now
        ):
            offer.finalize()

    triprequests = TripRequest.objects.filter(active=True)
    for trip_request in triprequests:
        print(
            f"Comanda {trip_request.id}, min_time_arrival {trip_request.min_time_arrival},  max_time_arrival {trip_request.max_time_arrival}"
        )
        if (
            trip_request.max_time_arrival
            and trip_request.max_time_arrival
            + timezone.timedelta(minutes=settings.MINUTES_AFTER_OFFERS_FINALIZES)
            < now
        ):
            trip_request.finalize()
        elif (
            not trip_request.max_time_arrival
            and trip_request.min_time_arrival
            + timezone.timedelta(minutes=settings.REQUESTS_EXPIRE_MINUTES_MAX)
            < now
        ):
            trip_request.finalize()
        else:
            accepted_offer = trip_request.accepted_offer
            print("accepted_offer", accepted_offer)
            if accepted_offer and not accepted_offer.active:
                trip_request.finalize()
