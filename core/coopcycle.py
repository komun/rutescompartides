import base64
import requests


def get_retail_prices(domain, access_token, origin, destination):
    if access_token:
        print("El access_token es:", access_token)
    else:
        print(
            "No se pudo obtener el access_token. Verifica las credenciales y asegúrate de que el servidor está accesible."
        )
        b

    url = f"{domain}/api/retail_prices/calculate"
    payload = {
        "pickup": {
            "address": origin,
            # "before": "tomorrow 10:30"
        },
        "dropoff": {
            "address": destination,
            # "before": "tomorrow 13:30"
        },
    }

    headers = {
        "Accept": "application/ld+json",
        "Content-Type": "application/ld+json",
        "Authorization": f"Bearer {access_token}",
    }

    print("payload", payload)
    print("headers", headers)
    response = requests.post(url, json=payload, headers=headers)
    print("Status code", response.status_code)
    if response.status_code == 200:
        # Successful request
        result = response.json()
        # Process the response data
        print(result)
        amount = int(result["amount"])
        return amount / 100
    else:
        # Error handling
        print("Error:", response.status_code)
        print(response.text)
        return -1


def get_access_token(domain, client_id, client_secret):
    url = f"{domain}/oauth2/token"
    data = {
        "grant_type": "client_credentials",
        "client_id": client_id,
        "client_secret": client_secret,
        "scope": "deliveries tasks",
    }

    headers = {
        "Authorization": "Basic "
        + base64.b64encode(f"{client_id}:{client_secret}".encode()).decode()
    }
    try:
        response = requests.post(url, data=data, headers=headers)
        print("response:", response)
        if response.status_code == 200:
            access_token = response.json()["access_token"]
            return access_token
        else:
            return None
    except:
        return None
