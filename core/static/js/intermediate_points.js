var last_intermediate = 1;
var all_points = [];
var MAXIMUM_POINTS = 6;
Array.prototype.remove = function(v) { this.splice(this.indexOf(v) == -1 ? this.length : this.indexOf(v), 1); }

$(document).ready(function() {
      $('#form_new_offer').submit(function() {
        $.each(all_points, function( index, value ) {
            step_number = index + 1
            $("#step_location_"+value).attr('name', "step_location_" + step_number);
            $("#step_name_"+value).attr('name', "step_name_" + step_number);
        });
      });//end submit

      for (var i = 1; i <= MAXIMUM_POINTS; i++) {
        $step_location = $('#edit_step_location_'+i).val();
        $step_name = $('#edit_step_name_'+i).val();
        // console.log($step_location)
        if($step_location){
          newIntermediatePoint($step_location, $step_name)
        }
      }
  }
);//end ready function


function deleteIntermediatePoint(step_number){
  $("#div_step_"+step_number).remove();
  all_points.remove(step_number);
  if(all_points.length < MAXIMUM_POINTS){
    $("#button_new_point").prop("disabled", false)
  }
}

function newIntermediatePoint(location, name){
  all_points.push(last_intermediate);
  if(all_points.length == MAXIMUM_POINTS){
    $("#button_new_point").prop("disabled", true)
  }
  $html_new_row = `
    <div id="div_step_`+last_intermediate+`" class="form-group form-inline" style="margin-bottom:0px">
          <input type="hidden" id="step_location_`+last_intermediate+`" name="step_location_`+last_intermediate+`" value="`+location+`">
          <input type="text" value="`+name+`" id="step_name_`+last_intermediate+`"  name="step_name_`+last_intermediate+`" class="col-sm-11 textinput textInput form-control pac-target-input" style="margin-bottom:10px" data-geocomplete="street address" placeholder="Punt intermedi" maxlength="300"  autocomplete="off" ">
          <button type="button" onclick="deleteIntermediatePoint(`+last_intermediate+`)" class="btn btn-danger btn-sm" style="margin-left:10px; margin-bottom:10px">-</button>
    </div>`
  $("#intermediate_points").append($html_new_row)

  //Geolocation for intermediate text inputs
  var geocode_input = "#step_name_"+last_intermediate
  $(geocode_input).attr("position", last_intermediate)
  $(geocode_input).geocomplete({
      // types: ['(regions)'],
      componentRestrictions: {country: "es"},
      onChange: function (name, result, last_intermediate) {
        // console.log(name, result)
        origin_lat = result.geometry.location.lat()
        origin_lon = result.geometry.location.lng()
        position = this.attr("position")
        $("#step_location_"+position).val("SRID=4326;POINT("+origin_lon+" "+origin_lat+")")
        formatted_address = formatted_address = extract_street_and_locality(result)
        $(geocode_input).val(formatted_address);

      },
      onNoResult: function (name) {
        console.log("Could not find a result for " + name)
      }
  });

  last_intermediate += 1;
}
