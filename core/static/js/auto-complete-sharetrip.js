var origin_lat, origin_lon;
var destination_lat, destination_lon;

$("#id_origin_name").attr("placeholder","Punt de sortida");
$("#id_origin_name").geocomplete({
    // types: ['(regions)'],
    componentRestrictions: {country: "es"},
    onChange: function (name, result) {
      //console.log(name, result)
      origin_lat = result.geometry.location.lat()
      origin_lon = result.geometry.location.lng()
      $("#origin_location").val("SRID=4326;POINT("+origin_lon+" "+origin_lat+")")

      formatted_address = extract_street_and_locality(result)
      //removing postal code from name
      
      $("#id_origin_name").val(formatted_address);
    },
    onNoResult: function (name) {
      console.log("Could not find a result for " + name)
      origin_lat = null;
      origin_lon = null;
    }
});
$('#id_origin_name').keyup(function() {
  origin_lat = null;
  origin_lon = null;
});

$("#id_destination_name").attr("placeholder","Punt d'arribada");
$("#id_destination_name").geocomplete({
    componentRestrictions: {country: "es"},
    onChange: function (name, result) {
      //console.log(name, result)
      destination_lat = result.geometry.location.lat()
      destination_lon = result.geometry.location.lng()
      $("#destination_location").val("SRID=4326;POINT("+destination_lon+" "+destination_lat+")")
      formatted_address = extract_street_and_locality(result)
      $("#id_destination_name").val(formatted_address);
    },
    onNoResult: function (name) {
      console.log("Could not find a result for " + name)
      destination_lat = null;
      destination_lon = null;
    }
});
$('#id_destination_name').keyup(function() {
  destination_lat = null;
  destination_lon = null;
});
