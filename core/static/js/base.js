$filter_help_text_need = `Seleccionar el tipus de transport que NECESSITO per aquesta ruta farà que només m'apareguin les ofertes de rutes que es fan amb transport d'aquestes característiques especials (si no és imprescindible, millor no seleccionar-lo, per no limitar opcions que sí podrien encaixar). Cal acabar de confirmar les condicions amb la persona que faci el transport, abans de quedar:<br><br>`
$filter_help_text_offer = `Seleccionar el tipus de transport que OFEREIXO per aquesta ruta farà que a més a més d'aparèixer a les cerques de transport general, també aparegui a les cerques d'usuàries que cerquen transport amb característiques especials:<br><br>`
$filter_help_text_search = `En seleccionar el tipus de transport, se't mostraran només els vehicles i les comandes (necessitats de transport) que compleixin aquestes característiques:<br><br>`
$filter_help_text_content = `<b>ISOTERM-</b> vehicle amb parets aïllants, manté la temperatura.
<br><b>REFRIGERAT-</b> vehicle amb font de fred, temperatures de 4 a 12º (cal confirmar temperatura amb qui ofereixi el transport).
<br><b>CONGELAT-</b> vehicle amb font de fred, temperatures inferiors a 0º (cal confirmar temperatura amb qui ofereixi el transport).
<br><b>SENSE HUMITAT-</b> no es transporten verdures o altres productes que produeixin humitat (vehicle compatible per al transport de pa, llavors, pasta, etc.).`

$filter_help_text_content_need = `<b>ISOTERM</b>: imprescindible que sigui vehicle amb parets aïllants que manté la temperatura<br>
<b>REFRIGERAT</b>: imprescindible vehicle amb font de fred, temperatures de 4 a 12º (cal confirmar temperatura directament amb qui m'ofereixi fer el transport).<br>
<b>CONGELAT</b>: imprescindible vehicle amb font de fred que mantingui a temperatures inferiors a 0º (cal confirmar temperatura directament amb qui m'ofereixi fer el transport).<br>
<b>SENSE HUMITAT</b>: vehicle on no es transporten verdures o altres productes que produeixin humitat (m'asseguren que és un vehicle compatible per al transport de pa, llavors, pasta, etc.).
<b>DESCÀRREGA PALETS</b>: el magatzem o centre de logística ha d'estar preparat per a la descarrega de palets.
`
$filter_help_text_logisticnode = `Seleccionar les característiques del node logístic, ja que cada tipus de producte té unes necessitats específiques.<br><br>`
$filter_help_text_content_logisticnode = `ISOTERM: Node logístic amb disponibilitat de caixes isotèrmiques o algun altre sistema per mantenir la temperatura.<br>
<b>REFRIGERAT</b>: Node logístic amb espai refrigerat, amb font de fred. Temperatura entre 4 i 12º.<br>
<b>CONGELAT</b>: Node logístic amb congelador, amb font de fred. Temperatura per sota de 0º.<br>
<b>SENSE HUMITAT</b>: Node logístic sense humitat ni productes que transmetin humitat (compatibilitat per pa, llavors, pasta... assegurada).<br>
<b>DESCÀRREGA PALETS</b>: Node logístic dotat de toro mecànic o similar per a poder carregar/descarregar palets de vehicles.`

$tags_help_text = "En cas de formar part d'un grup, pot ser útil publicar amb una determinada etiqueta (NOM DEL GRUP) per trobar més fàcilment les ofertes/demandes del grup. En cas de possibilitat de cobrar en moneda social o intercanvi de productes, pot ser útil utilitzar l'etiqueta INTERCANVI, etc."
$fragile_help_text = "Indica a qui faci la ruta, que transporta quelcom que es trenca fàcilment o que no és apilable."
$max_arrival_help_text = "Indicar la data màxima en què ha d'arribar la teva comanda."
$min_arrival_help_text = "Indicar a partir de quin moment podries tenir llesta la teva comanda per iniciar el transport."
$convert_request_help_text = "Si no pots afegir la teva comanda a cap ruta existent i vols fer tu mateix/a la ruta, clica aquí per convertir la teva comanda en ruta."


function initMap(callback) {

    a = 1;
    //loaded api
}

function extract_street_and_locality(gaddress_result) {
    let formatted_address = "";
    //location_name = location_name.replace(", Espanya", "")
    gaddress_result.address_components.forEach(function (c) {
        if (c.types.includes("street_number") && c.short_name) {
            formatted_address += c.short_name + " ";
        }
        if (c.types.includes("route") && c.short_name) {
            formatted_address += c.short_name + ", ";
        }
        if (c.types.includes("locality") && c.short_name) {
            formatted_address += c.short_name;
        }
    });

    return formatted_address;
}


function fill_notification_list2(data) {
    var menus = document.getElementsByClassName(notify_menu_class);
    if (menus) {
        var messages = data.unread_list.map(function (item) {
            var message = "";
            // if(typeof item.actor !== 'undefined'){
            //     message = item.actor;
            // }
            if(item.verb == '' && item.description !== null){
                message = message + " " + item.description;
            }else if(typeof item.verb !== 'undefined'){
                  message = message + " " + item.verb;
            }
            if(typeof item.target !== 'undefined'){
                message = message + " " + item.target;
            }
            // if(typeof item.timestamp !== 'undefined'){
            //     message = message + " " + item.timestamp;
            // }
            return '<li><small>' + message + '</small></li>';
        }).join('')

        for (var i = 0; i < menus.length; i++){
            menus[i].innerHTML = messages;
        }
    }
}
