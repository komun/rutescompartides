//geocoder = new google.maps.Geocoder();

function initialize_search_form(){
  show_first_offers();
  show_first_requests();

  origin_name = $.query.get('origin_name')
  destination_name = $.query.get('destination_name')
  if(origin_name){
    $("#transport_isothermic").prop('checked', $.query.get('isothermic') == "true")
    $("#transport_cold").prop('checked', $.query.get('cold') == "true")
    $("#transport_freezer").prop('checked', $.query.get('freezer') == "true")
    $("#transport_dry").prop('checked', !($.query.get('dry') == "true"))
    $("#id_time_depart").val($.query.get('time_depart'))
    $.query.get('tags').split(",").forEach(function(item) {
      $('#tags').tagsinput('add', item);
    });


    $("#id_origin_name").val(origin_name)
    $("#id_destination_name").val(destination_name)
    origin_lon =  $.query.get('origin').split(",")[0]
    origin_lat =  $.query.get('origin').split(",")[1]
    destination_lon =  $.query.get('destination').split(",")[0]
    destination_lat =  $.query.get('destination').split(",")[1]
      // geocoder.geocode( { 'address': origin_name + ", Spain"}, function(results, status) {
      //   if (status == 'OK') {
      //     position = results[0].geometry.location;
      //     console.log(origin_name, position.toString())
      //     origin_lat = position.lat()
      //     origin_lon = position.lng()
      //   } else {
      //     alert('Geocode was not successful for the following reason: ' + status);
      //   }
      // });

    var search_method = $.query.get('search') || "trip_offers"
    if (search_method == "trip_offers") search_trip_offers();
    else search_trip_requests();
  }
}

function get_url_params(search_method){
  var newUrl = "isothermic="+ String($("#transport_isothermic").prop('checked'))
  newUrl += "&cold="+String($("#transport_cold").prop('checked'))
  newUrl += "&freezer="+String($("#transport_freezer").prop('checked'))
  newUrl += "&dry="+String($("#transport_dry").prop('checked'))
  if($("#tags").val()){
    newUrl += "&tags="+$("#tags").val()
  }
  newUrl += "&time_depart="+get_date_depart()
  newUrl += "&origin_name="+$("#id_origin_name").val()
  newUrl += "&origin="+origin_lon + "," + origin_lat
  newUrl += "&destination_name="+$("#id_destination_name").val()
  newUrl += "&destination="+destination_lon + "," + destination_lat
  newUrl += "&search="+search_method
  return newUrl;
}

function search_trips(){
  // var id_date_depart = $("#id_date_depart").val();
  // if(id_date_depart == ""){alert("S'ha de completar la data de sortida..."); return null;}
  if(!$("#tags").val() && origin_lat == null && destination_lat == null){
    alert("S'ha de completar al menys el punt de sortida o d'arribada. Selecciona una localitat de la llista.");
    return null;
  }
  else{
    search_trip_offers();
    search_trip_requests();
  }
}

function get_date_depart(){
  var date = $("#id_date_depart").val();
  if(! date) return date;
  var time = $("#id_time_depart").val();
  if(! time){ time = '00:00'}
  date = date + ' ' + time;
  return date;
}

function show_first_offers(){
      $.get( "/offer/first/", {},
      function(data, status) {
        header_html = '<h3 class="headline">Properes RUTES que s\'ofereixen <span style="background:#D36A35; color:white">(R)</span></h3><br>'
        $("#search_results_offers").html(header_html + data);
      });
}
function show_first_requests(){
      $.get( "/request/first/", {},
      function(data, status) {
        header_html = '<h3 class="headline">Properes COMANDES que necessiten transport <span style="background:#24ad21; color:white">(C)</span></h3><br>'
        $("#search_results_requests").html(header_html + data);
      });
}

function search_trip_offers(){
      var search_url = get_url_params("trip_offers");
      //document.location.href = newUrl.toString();
      $.get( "/offer/search/", {
                                  transport_isothermic: $("#transport_isothermic").prop('checked'),
                                  transport_cold: $("#transport_cold").prop('checked'),
                                  transport_freezer: $("#transport_freezer").prop('checked'),
                                  transport_dry: $("#transport_dry").prop('checked'),
                                  tags: $("#tags").val(),
                                  id_time_depart: get_date_depart(),
                                  origin: origin_lon + "," + origin_lat,
                                  destination: destination_lon + "," + destination_lat,
                                },
      function(data, status) {
        header_html = '<h3 class="headline">Rutes trobades <a href="/?'+search_url+'"><i class="fas fa-link"></i></a></h3><br>'
        $("#search_results_offers").html(header_html + data);
        $('html, body').animate({scrollTop: $("#search_results_offers").offset().top}, 2000);

        $( 'a[id^=result_offer_]' ).each(function(el, element){
            element.href = element.href + "?" + search_url;
        });
      });

}

function search_trip_requests(){
  var search_url = get_url_params("trip_requests");
  $.get( "/request/search/", {
          transport_isothermic: $("#transport_isothermic").prop('checked'),
          transport_cold: $("#transport_cold").prop('checked'),
          transport_freezer: $("#transport_freezer").prop('checked'),
          transport_dry: $("#transport_dry").prop('checked'),
          tags: $("#tags").val(),
          id_time_depart: get_date_depart(),
          origin: origin_lon + "," + origin_lat,
          destination: destination_lon + "," + destination_lat,
                            },
  function(data, status) {
    header_html = '<h3 class="headline">Comandes trobades <a href="/?'+search_url+'"><i class="fas fa-link"></i></a></h3><br>'
    $("#search_results_requests").html(header_html + data);

    $( 'a[id^=result_request_]' ).each(function(el, element){
        element.href = element.href + "?" + search_url;
    });
  });
};
