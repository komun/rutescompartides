function update_match_status(match_id, status) {
  var comment = null;

  if (status == 'REJECTED') {
    var comment = prompt("Indica una raó per a informar a qui ha fet la petició de transport de la comanda");
    if (comment == null) return;
  }
  if (status == 'CANCELED') {
    var comment = prompt("Indica una raó per a informar a qui farà la ruta");
    if (comment == null) return;
  }

  $.post("/match/update/" + match_id + "/", { status: status, comment: comment },
    function () {
      console.log("Success upading match:" + match_id);
      window.location.reload(true);
    });

};

function update_trip_request(trip_id, action) {

  $.post("/request/update/" + trip_id + "/", { action: action },
    function (data) {
      if (data.status) {
        console.log("Successfully executed action " + action + " on: " + trip_id);
        $(location).attr("href", "/request/user/me/");
      } else {
        if (data.reason == 'PENDING_REQUESTS') alert("No es possible dur a terme l'operació ja que existeix ja una petició en curs o acceptada. S'ha de cancel·lar abans.");
      }

    });

};

function post_update_trip_offer(trip_id, action) {
  if (action) {
    $.post("/offer/update/" + trip_id + "/", { action: action },
      function (data) {
        if (data.status) {
          console.log("Successfully executed action " + action + " on: " + trip_id);
        } else {
          console.log("Unsuccessfully executed action " + action + " on: " + trip_id);
        }
        $(location).attr("href", "/offer/user/me/");
      });
  }
}

function show_scope_modal_dialog(frequency = 'ONCE') {
  $form = $("#form_new_offer")
  if (frequency != 'ONCE') {
    $("#dialog-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      open: function (event, ui) {
        $(event.target).parent().css('position', 'fixed');
        $(event.target).parent().css('top', '40%');
        $(event.target).parent().css('left', '40%');
      },
      buttons: {
        "Totes": function () {
          $(this).dialog("close");
          $form.append('<input type="hidden" id="save_scope" name="save_scope" value="all">');
          $form.submit()
        },
        "Només aquesta": function () {
          $(this).dialog("close");
          $form.append('<input type="hidden" id="save_scope" name="save_scope" value="this">');
          $form.submit()
        }
        ,
        Cancel: function () {
          $(this).dialog("close");
        }
      }
    });
  } else {
    $form.submit()
  }
}

function update_trip_offer(trip_id, action, frequency = 'ONCE') {
  if (frequency != 'ONCE') {
    $("#dialog-confirm").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      open: function (event, ui) {
        $(event.target).parent().css('position', 'fixed');
        $(event.target).parent().css('top', '40%');
        $(event.target).parent().css('left', '40%');
      },
      buttons: {
        "Totes": function () {
          post_update_trip_offer(trip_id, action + "all");
          $(this).dialog("close");
        },
        "Només aquesta": function () {
          post_update_trip_offer(trip_id, action);
          $(this).dialog("close");
        },
        Cancel: function () {
          $(this).dialog("close");
        }
      }
    });
  } else {
    post_update_trip_offer(trip_id, action);
  }
};

function update_storage(object_id, action) {

  $.post("/storage/update/" + object_id + "/", { action: action },
    function (data) {
      if (data.status) {
        console.log("Successfully executed action " + action + " on: " + object_id);
      } else {
        console.log("Unsuccessfully executed action " + action + " on: " + object_id);
      }
      $(location).attr("href", "/storage/user/me/");
    });

};

//
// modal_html = `<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
//   <div class="modal-dialog" role="document">
//     <div class="modal-content">
//       <div class="modal-header">
//         <h5 class="modal-title" id="exampleModalLabel">Valorar experiència</h5>
//         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//           <span aria-hidden="true">&times;</span>
//         </button>
//       </div>
//       <div class="modal-body">
//         <div class="rating">
//         <label for="cars">Puntuació:</label>
//
//           <select name="review_rating" id="review_rating"">
//             <option value="0">0</option>
//             <option value="1">1</option>
//             <option value="2">2</option>
//             <option value="3">3</option>
//             <option value="4">4</option>
//             <option value="5">5</option>
//           </select>
//
//         </div>
//         <div class="form-group">
//           <label for="message-text" class="col-form-label">Comentari:</label>
//           <textarea class="form-control" id="review_comment"></textarea>
//         </div>
//       </div>
//       <div class="modal-footer">
//         <button type="button" class="btn btn-secondary" data-dismiss="modal">Tancar</button>
//         <button type="button" id="save_review_button" class="btn btn-primary">Enviar</button>
//       </div>
//     </div>
//   </div>
// </div>`;
//
// function review_match(match_id, role){
//   $('#modal_container').html(modal_html);
//   reviewing_match_id = match_id;
//   $('#reviewModal').modal('show');
//
//   $("#save_review_button").click(function() {
//     $('#reviewModal').modal('hide');
//     comment = $('#review_comment').val()
//     rating = $('#review_rating').val();
//     $.post( "/match/review/"+reviewing_match_id+"/", {comment: comment, rating: rating, role:role},
//       function() {
//         console.log( "Success reviewing match:" + reviewing_match_id );
//         window.location.reload(true);
//     });
//
//   });
// }


function add_match(offer_id, request_id, role) {

  $.get("/match/add/", { offer_id: offer_id, request_id: request_id, role: role },
    function (data) {
      if (data.status) {
        console.log("Success creating match.");
        if (role == "driver") { $(location).attr("href", "/offer/user/me/"); }
        else { $(location).attr("href", "/request/user/me/"); }
      } else {
        if (data.reason == 'ALREADY_MATCHED') alert("No es possible dur a terme l'operació ja que existeix ja una petició en curs o acceptada.");
        else alert("No es possible dur a terme l'operació.");
      }
    });

};

function addPackages(num) {
  // Loop through elements with id="packageX"
  if(num >= 2){
    var form = document.getElementById('postFormCoopcycle');
    for (let i = 1; i <= num; i++) {
      // Find the element with id="packageX"
      var elementToAdd = document.getElementById('package1');
      if (elementToAdd) {
        // Clone the element
        var clonedElement = elementToAdd.cloneNode(true);
        // Check if the element exists before attempting to remove it
        clonedElement.id = 'package' + i + '_cloned';
        clonedElement.name = 'delivery[tasks][1][packages][' + (i-1) + '][quantity]';
        clonedElement.value = '1';
        // Append the cloned element to the form
        form.appendChild(clonedElement);
        clonedElement.id = 'package' + i + '_value';
        clonedElement.name = 'delivery[tasks][1][packages][' + (i-1) + '][package]';
        clonedElement.value = 'L';
        form.appendChild(clonedElement);
          
      }
    }
  }
}
function openCoopCycleForm(origin, lat_orig, lon_orig, destination, lat_dest, lon_dest, delivery_details, packages_weight,
                            packages_num, name, email, phone) {

  // addPackages(packages_num);
  document.getElementById('origin_streetAddress').value = origin;
  document.getElementById('lat_orig').value = lat_orig;
  document.getElementById('lon_orig').value = lon_orig;
  document.getElementById('delivery_details').value = delivery_details;
  document.getElementById('weight').value = packages_weight;
  document.getElementById('package_amount').value = packages_num;

  document.getElementById('name').value = name;
  document.getElementById('email').value = email;
  document.getElementById('telephone').value = phone;
  

  document.getElementById('destination_streetAddress').value = destination;
  document.getElementById('lat_dest').value = lat_dest;
  document.getElementById('lon_dest').value = lon_dest;
  document.getElementById('postFormCoopcycle').submit();
}