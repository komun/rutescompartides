self.addEventListener('install', (e) => {
  e.waitUntil(
    caches.open('fox-store').then((cache) => cache.addAll([
      '/index.html',
      '/static/js/a2hs/index.js',
      '/static/js/base.js',
      '/static/js/model_actions.js',
      '/static/js/search_utils.js',
      '/static/js/auto-complete-sharetrip.js',
      '/static/js/intermediate_points.js',
      '/static/js/jquery.query-objects.js',
      '/static/js/leaflet_display.js',
      '/static/js/cookies-consent.js',
      '/static/css/base.css',
      '/static/css/ratings.css',
      '/static/css/auto-complete.css',
      '/static/css/custom_pages.css',
    ])),
  );
});

self.addEventListener('fetch', (e) => {
  console.log(e.request.url);
  e.respondWith(
    caches.match(e.request).then((response) => response || fetch(e.request)),
  );
});
