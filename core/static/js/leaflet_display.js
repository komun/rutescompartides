var map = L.map('map').setView([41.583, 2.247], 8);
tiles_url = 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
var tiles = L.tileLayer(tiles_url, {
  maxZoom: 18,
  attribution: '© <a target="blank" href="https://www.openstreetmap.org/copyright">Col·laboradors de OpenStreetMap</a> ♥'
});
tiles.addTo(map);

//In computers, we let the maxwidth of popups to be wider
popup_maxwidth = { maxWidth: 560 };
if (navigator.userAgent.toLowerCase().match(/mobile/i)) { popup_maxwidth = {}; }


function fatalitiesLayer(feature, layer) {
  layer.bindPopup("<p><img src='/static/img/reshot-icon-truck.svg'>La teva ruta</p>");
}

var orangeIcon = new L.Icon({
  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});


var greenIcon = new L.Icon({
  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

var greyIcon = new L.Icon({
  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-grey.png',

  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

var blueHouseIcon = new L.Icon({
  iconUrl: '/static/img/marker_house_blue.png',
  // shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [41, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

var greyHouseIcon = new L.Icon({
  iconUrl: '/static/img/marker_house_grey.png',
  // shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [41, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});
var logisticNodeIcon = new L.Icon({
  iconUrl: '/static/img/logisticnode.png',
  iconSize: [41, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});


function display_in_map(type, url) {

  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function (response) {
      var marker1 = L.geoJson(response.origin, {
        pointToLayer: function (feature, latlng) {
          return L.marker(latlng, { icon: orangeIcon, title: "Origen" });
        }
      }).bindPopup('Origen');
      var marker2 = L.geoJson(response.destination, {
        pointToLayer: function (feature, latlng) {
          return L.marker(latlng, { icon: greyIcon, title: "Destinació" });
        }
      }).bindPopup('Destinació');
      if (type == "request") {
        group = L.featureGroup([marker1, marker2]).addTo(map)
      } else if (type == "storage" || type == "logisticnode") {
        group = L.featureGroup([marker1]).addTo(map)
      } else {
        var route = L.geoJson(response.route, {
          onEachFeature: fatalitiesLayer,
          style: function () {
            return { color: '#D36A35' }
          }
        });
        group = L.featureGroup([route, marker1, marker2]).addTo(map)
      }
      map.fitBounds(group.getBounds());
    }
  });
}


function display_route_in_request_map(url) {
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function (response) {
      var route = L.geoJson(response.route, {
        onEachFeature: fatalitiesLayer,
        style: function () {
          return { color: '#D36A35' }
        }
      });


      //map.fitBounds(group.getBounds());
      group = L.featureGroup([route]).addTo(map)
    }
  });
}


function onClickFeature(route_id, layer, type = "trip_offer") {
  if (type == "trip_offer") view_url = "/offer/mapview/" + route_id;
  else view_url = "/request/mapview/" + route_id;
  $.ajax({
    type: "GET",
    url: view_url,
    dataType: 'html',
    success: function (popupContent) {
      // Bind the popup to the layer and open it
      layer.bindPopup("<p>" + popupContent + "</p>", popup_maxwidth).openPopup();
    }
  });
}

function display_geojson_in_map(url) {
  // Load and display GeoJSON
  fetch(url)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      element = L.geoJSON(data).addTo(map);
      map.fitBounds(element.getBounds());
      map.setZoom(11);
    });


}

function display_logisticnodes_in_map(url) {

  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function (responses) {
      responses.forEach(function (response) {
        html_popup = '<a href="/logisticnode/view/' + response.id + '"><h3 class="headline">' + response.name + '</h3></a><br>Tipus: Node logístic<br>' + response.body;
        icon = logisticNodeIcon;

        L.geoJson(response.location, {
          pointToLayer: function (feature, latlng) {
            return L.marker(latlng, { icon: icon });
          }
        }).addTo(map).bindPopup(html_popup);

      }); //foreach
    } //success
  });
}

function display_storages_in_map(url) {

  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function (responses) {
      responses.forEach(function (response) {
        html_popup = '<a href="/storage/view/' + response.id + '"><h3 class="headline">' + response.name + ' ' + response.icons_html + '</h3></a><br>' + response.body;
        if (response.is_public) {
          icon = blueHouseIcon;
        } else { icon = greyHouseIcon; }

        L.geoJson(response.location, {
          pointToLayer: function (feature, latlng) {
            return L.marker(latlng, { icon: icon });
          }
        }).addTo(map).bindPopup(html_popup);

      }); //foreach
    } //success
  });
}
function display_requests_in_map(url) {
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function (responses) {
      responses.forEach(function (response) {
        console.log(response);
        var origin = L.geoJson(response.origin, {
          pointToLayer: function (feature, latlng) {
            return L.marker(latlng, { icon: greenIcon });
          }
        }).addTo(map);

        var destination = L.geoJson(response.destination, {
          pointToLayer: function (feature, latlng) {
            return L.marker(latlng, { icon: greenIcon });
          }
        }).addTo(map);

        [origin, destination].forEach(function (layer) {
          var route_url = "/request/view/" + response.id + "/geojson";
          $.ajax({
            type: "GET",
            url: route_url,
            dataType: 'json',
            success: function (routeResponse) { // Rename the variable to avoid conflict with outer 'response'
              // Create the route layer from the fetched GeoJSON
              var route = L.geoJson(routeResponse.route, {
                style: function () {
                  return { color: '#25aa22', weight: 5 }
                }
              });
              route.addTo(map);
              route.on('click', (function (id) { // Use closure to capture response.id
                return function (event) {
                  onClickFeature(id, layer, "trip_request");
                };
              })(response.id)); // Pass response.id to the closure
            }
          });

          layer.on('click', function (event) {
            onClickFeature(response.id, layer, "trip_request");
          });
        });

        group = L.featureGroup([origin, destination]).addTo(map);
      });
    }
  });
}


function display_offers_in_map(url) {
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function (responses) {
      responses.forEach(function (response) {
        var origin = L.geoJson(response.origin, {
          pointToLayer: function (feature, latlng) {
            return L.marker(latlng, { icon: orangeIcon });
          }
        });

        var destination = L.geoJson(response.destination, {
          pointToLayer: function (feature, latlng) {
            return L.marker(latlng, { icon: orangeIcon });
          }
        });

        [origin, destination].forEach(function (layer) {
          var route_url = "/offer/view/" + response.id + "/geojson"; // Assuming response.id is defined outside this scope
          $.ajax({
            type: "GET",
            url: route_url,
            dataType: 'json',
            success: function (response) {
              // Create the route layer from the fetched GeoJSON
              var route = L.geoJson(response.route, {
                style: function () {
                  return { color: '#cb862c', weight: 5 }
                }
              });
              // Add the route layer to the map
              route.addTo(map);
              route.on('click', function (event) {
                // Fetch the popup content via Ajax
                onClickFeature(response.id, layer, "trip_offer");
              });
            }
          });
          layer.on('click', function (event) {
            onClickFeature(response.id, layer, "trip_offer");
          });
        });


        // Add the origin and destination layers to the map
        group = L.featureGroup([origin, destination]).addTo(map);
      });
    }
  });
}

showLegend = true;  // default value showing the legend

var toggleLegend = function () {
  if (showLegend === true) {
    /* use jquery to select your DOM elements that has the class 'legend' */
    $('.legend').hide();
    showLegend = false;
  } else {
    $('.legend').show();
    showLegend = true;
  }
}


function insert_legend() {
  var legend = L.control({ position: 'bottomright' });

  legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
      labels = ["Rutes", "Comandes", "[Només amb sessió iniciada]", "Punts habituals privats ", "Nodes logístics"],
      images = ["https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-orange.png",
        "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png",
        "",
        "/static/img/marker_house_grey.png",
        "/static/img/logisticnode.png"];


    // loop through our density intervals and generate a label with a colored square for each interval
    div.innerHTML += '<button style="float:right" type="button" onclick="toggleLegend();">X</button>'
    div.innerHTML += ("<img src=" + images[0] + " height='30' width='20'> ") + labels[0] + '<br>';
    div.innerHTML += (" <img src=" + images[1] + " height='30' width='20'> ") + labels[1] + '<br>';
    div.innerHTML += ("<br><small> ") + labels[2] + '</small> <br>';
    div.innerHTML += (" <img src=" + images[3] + " height='30' width='30'><small> ") + labels[3] + '</small>';
    div.innerHTML += ("<br><img src=" + images[4] + " height='30' width='30'><small> ") + labels[4] + '</small>';

    return div;
  };

  legend.addTo(map);
}
