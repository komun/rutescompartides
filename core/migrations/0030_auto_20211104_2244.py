# Generated by Django 3.2 on 2021-11-04 21:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_triprequest_wants_carpool'),
    ]

    operations = [
        migrations.AddField(
            model_name='tripmatch',
            name='times_reminded_rate',
            field=models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='Vegades recordat per a valorar'),
        ),
        migrations.AlterField(
            model_name='tripoffer',
            name='max_detour_km',
            field=models.FloatField(blank=True, default=10, help_text="Quants km estic disposat/da a desviar-me de la meva ruta per recollir/entregar comandes d'altres.", null=True, verbose_name='Màxim desviament de la ruta (km)'),
        ),
    ]
