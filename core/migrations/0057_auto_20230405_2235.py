# Generated by Django 3.2 on 2023-04-05 20:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0056_alter_logisticnode_api_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logisticnode',
            name='active',
            field=models.BooleanField(default=False, verbose_name='Actiu'),
        ),
        migrations.AlterField(
            model_name='logisticnode',
            name='api_url',
            field=models.URLField(blank=True, max_length=100, null=True, verbose_name='Url Api Coopcycle'),
        ),
    ]
