# Generated by Django 3.2.15 on 2022-12-26 16:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0049_triprequest_co2_saved'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tripmatch',
            name='co2_saved',
        ),
    ]
