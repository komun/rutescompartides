# Generated by Django 3.1 on 2021-05-03 19:13

import core.helpers
from django.conf import settings
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taggit', '0003_taggeditem_add_unique_index'),
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='auth.user')),
                ('language', models.CharField(choices=[('ca', 'Català'), ('es', 'Castellano')], default='ca', max_length=15, verbose_name='App Language')),
                ('notes', core.helpers.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Public Notes')),
                ('picture', models.ImageField(blank=True, default='user_profiles/default.png', help_text='Dimensions minimes recomanades: 200 x 100px.', null=True, upload_to='user_profiles', verbose_name='Fotografia')),
                ('organization', models.CharField(max_length=30, verbose_name='Organization')),
                ('vehicle', models.CharField(max_length=30, verbose_name='Type of Vehicle')),
                ('cost_km', models.FloatField(blank=True, null=True)),
                ('phone', models.CharField(max_length=15, verbose_name='Phone')),
                ('contact', models.JSONField(verbose_name='Other Contacts')),
                ('consent_date', models.DateField(blank=True, null=True, verbose_name='Data Consent')),
                ('tags', taggit.managers.TaggableManager(blank=True, help_text='Vehicle conditions or Hashtags', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags')),
            ],
            options={
                'verbose_name': 'User profile',
                'verbose_name_plural': 'User profiles',
                'ordering': ['user'],
            },
        ),
        migrations.CreateModel(
            name='TripRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('origin_location', django.contrib.gis.db.models.fields.PointField(geography=True, srid=4326, verbose_name='Origin Location')),
                ('origin_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Origin Name')),
                ('destination_location', django.contrib.gis.db.models.fields.PointField(geography=True, srid=4326, verbose_name='Destination Location')),
                ('destination_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Destination Name')),
                ('min_time_arrival', models.DateTimeField(verbose_name='Minimum Depart Time')),
                ('max_time_arrival', models.DateTimeField(blank=True, null=True, verbose_name='Maximum Depart Time')),
                ('comment', models.CharField(blank=True, max_length=2000, null=True, verbose_name='Comment')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('client', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Requesting User')),
                ('tags', taggit.managers.TaggableManager(blank=True, help_text='Requested conditions or Hashtags', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags')),
            ],
        ),
        migrations.CreateModel(
            name='TripOffer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('origin_location', django.contrib.gis.db.models.fields.PointField(geography=True, srid=4326, verbose_name='Origin Location')),
                ('origin_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Origin Name')),
                ('step1_location', django.contrib.gis.db.models.fields.PointField(geography=True, srid=4326, verbose_name='Stepping Location 1')),
                ('step1_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Stepping Location 1 Name')),
                ('step2_location', django.contrib.gis.db.models.fields.PointField(geography=True, srid=4326, verbose_name='Stepping Location 2')),
                ('step2_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Stepping Location 2 Name')),
                ('destination_location', django.contrib.gis.db.models.fields.PointField(geography=True, srid=4326, verbose_name='Destination Location')),
                ('destination_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Destination Name')),
                ('time_depart', models.DateTimeField(verbose_name='Expected Depart Time')),
                ('time_arrival', models.DateTimeField(verbose_name='Expected Arrival Time')),
                ('comment', models.CharField(blank=True, max_length=2000, null=True, verbose_name='Comment')),
                ('frequency', models.CharField(choices=[('ONCE', 'Once'), ('DAILY', 'Daily'), ('WEEKLY', 'Weekly')], default='ONCE', max_length=15, verbose_name='Frequency')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('driver', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Driver')),
            ],
        ),
        migrations.CreateModel(
            name='TripMatch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('status', models.CharField(choices=[('PENDING', 'Pending'), ('ACCEPTED', 'Accepted'), ('REJECTED', 'Rejected')], default='PENDING', max_length=15, verbose_name='Match Status')),
                ('driver_comment', models.CharField(blank=True, max_length=2000, null=True, verbose_name='Comment from Driver')),
                ('driver_rating', models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='Rating from Driver')),
                ('client_comment', models.CharField(blank=True, max_length=2000, null=True, verbose_name='Comment from Client')),
                ('client_rating', models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='Rating from Client')),
                ('co2_saved', models.FloatField(blank=True, null=True)),
                ('offer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='matches', to='core.tripoffer')),
                ('request', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='matches', to='core.triprequest')),
            ],
            options={
                'get_latest_by': 'modified',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('length', models.FloatField(null=True)),
                ('width', models.FloatField(null=True)),
                ('height', models.FloatField(null=True)),
                ('weight', models.FloatField(null=True)),
                ('request', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='packages', to='core.triprequest', verbose_name='Trip Request')),
            ],
        ),
    ]
