import telebot
from notifications.signals import notify
from django.core.mail import EmailMessage
from django.conf import settings
import locale
from math import floor
from dateutil.relativedelta import relativedelta
from calendar import monthrange

locale.setlocale(locale.LC_ALL, "ca_ES.utf8")

# is @sharetrip_bot

bot = telebot.TeleBot(settings.TELEGRAM_BOT_ID, parse_mode="markdown")
chat_id = settings.TELEGRAM_CHAT_ID



def _filter_town(name):
    split_name = name.split(",")
    return split_name[0]


@bot.message_handler(commands=["start", "help"])
def send_welcome(message):
    bot.reply_to(message, "Howdy, how are you doing?")


def get_day_month_name(mydate):
    # This will give us something like -8GEN
    day = mydate.strftime("%d")
    month = mydate.strftime("%b").upper()
    if month.startswith("DE "):
        month = month[len("DE ") :]  # noqa E203
    if month.endswith("."):
        month = month[:-1]
    return day, month


def get_nextmonth_by_weekday(mydate, original_weeknumber):
    previous_weekday = mydate.weekday()

    mydate += relativedelta(months=1)
    days_in_month = monthrange(mydate.year, mydate.month)[1]
    mydate = mydate.replace(day=1)
    while True:
        weeknumber = floor(mydate.day / 7)
        weekday = mydate.weekday()

        if previous_weekday == weekday:
            last_match_date = mydate
            if original_weeknumber == weeknumber:
                break

        if mydate.day == days_in_month:
            break
        mydate += relativedelta(days=1)
    return last_match_date


def send_html_email(to_list, subject, msg_html, sender=settings.DEFAULT_FROM_EMAIL):
    # msg_html = render_to_string(template_name, context)
    msg = EmailMessage(subject=subject, body=msg_html, from_email=sender, bcc=to_list)
    msg.content_subtype = "html"  # Main content is now text/html
    return msg.send()


def pay(recipient, amount, msg_notif):
    recipient.userprofile.points += amount
    recipient.userprofile.save()
    msg_user = f"<b>{amount} RUCs</b>: {msg_notif}"
    notify.send(
        sender=recipient,
        recipient=recipient,
        verb=msg_user,
        payment_amount=amount,
        payment_msg=msg_notif,
    )


def send_notifications(sender, recipient, msg_notif, subject, body):
    notify.send(
        sender=sender,
        recipient=recipient,
        verb="",  # msg_notif[0:255],
        description=msg_notif,
    )
    ok = True
    if recipient.userprofile.stop_email_notifications or settings.LOCAL:
        print("Not sending the following mail to", recipient.email)
        print("Subject", subject)
        print("Body", body)
        return ok
    else:
        email_recipient = recipient.email

        ok = send_html_email([email_recipient], subject, body)
        if not ok:
            print("Error sending mail. ", ok)
        return ok


def create_hashtags(object):
    hashtags = []
    if object.transport_isothermic:
        hashtags.append("#Isoterm")
    if object.transport_cold:
        hashtags.append("#Refrigerat")
    if object.transport_freezer:
        hashtags.append("#Congelat")
    if object.transport_dry:
        hashtags.append("#SenseHumitat")

    tags = list(object.tags.all())
    #print("tags", tags)
    for tag in tags:
        if tag.name:
            hashtags.append(f"#{tag.name}")

    return " ".join(hashtags)
