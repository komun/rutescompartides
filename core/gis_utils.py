import requests
import json
import random
import os

from django.conf import settings
from django.contrib.gis.geos import Point, LineString, fromstr, GEOSGeometry


routing_api_url = "https://routing.openstreetmap.de/routed-car/route/v1/driving/{}?overview=full&alternatives=false&steps=false&geometries=geojson"


def calculate_co2(meters):
    return (meters / 1000) * 0.15


def request_route(coords, with_random_offset=True):
    coord_str = ""
    ln = None
    for c in coords:
        coord_str += f"{c[0]},{c[1]};"
    coord_str = coord_str[:-1]
    output = {}

    try:
        api_url = routing_api_url.format(coord_str)
        print("GET ", api_url)
        response = requests.get(api_url)
        response.raise_for_status()
        # access JSOn content
        jsonResponse = response.json()
        api_coords = jsonResponse["routes"][0]["geometry"]["coordinates"]
    except requests.HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
    except Exception as err:
        print(f"Other error occurred: {err}")
    else:
        # out_file = open("/tmp/myfile.json", "w")
        # json.dump(jsonResponse["routes"][0]["geometry"], out_file, indent=6)
        #
        # out_file.close()
        if with_random_offset:
            rnumber = random.randint(0, 4)
            route = api_coords
            api_coords = []
            x_offset = 0
            y_offset = 0
            if rnumber == 1:
                x_offset = 0.001
            elif rnumber == 2:
                x_offset = -0.01
            elif rnumber == 3:
                y_offset = 0.001
            elif rnumber == 4:
                y_offset = -0.001

            for r in route:
                api_coords.append([r[0] + x_offset, r[1] + y_offset])
        ln = LineString(api_coords, srid=4326)  # .transform(3875, clone=True)
        output["linestring"] = ln
        output["distance"] = jsonResponse["routes"][0]["distance"]

    return output


def check_coordinates_inside_geojson_files(filenames, coordinatesA, coordinatesB):
    is_inside_coordA = False
    is_inside_coordB = False
    for filename in filenames:
        filenamepath = os.path.join(settings.MEDIA_GEOJSON, filename)
        print("Loading %s" % filenamepath)
        with open(filenamepath, "r") as f:
            data = json.load(f)
            geometry = GEOSGeometry(json.dumps(data["features"][0]["geometry"]))
            print(f"Loaded geometry for {filename}")
            # Verifica si coordinatesA está dentro de la geometría
            if coordinatesA and geometry.contains(
                GEOSGeometry("POINT({} {})".format(coordinatesA[0], coordinatesA[1]))
            ):
                is_inside_coordA = True
                print("CoordA está dentro de", filename)

            # Verifica si coordinatesB está dentro de la geometría
            if coordinatesB and geometry.contains(
                GEOSGeometry("POINT({} {})".format(coordinatesB[0], coordinatesB[1]))
            ):
                is_inside_coordB = True
                print("CoordB está dentro de", filename)

    print("check_coordinates_inside_geojson_files", is_inside_coordA, is_inside_coordB)
    return is_inside_coordA, is_inside_coordB


def detect_possible_logistic_nodes(object):
    from core.models import LogisticNode

    nodes = LogisticNode.objects.filter(active=True, deleted=False)
    nodes_results = []

    for node in nodes:
        print("DB LogisticNode", node)
        if node.delivery_areas:
            geojson_files = node.delivery_areas.split(",")
            print("GeoJSON Files", geojson_files)
            # TODO Loop with all the logistic nodes and their geojson zones
            result = [node]
            if (
                object.origin_logisticnode
            ):  # if triprequest has chosen already an logistic node for origin
                origin_location = None
            else:
                origin_location = object.origin_location

            if (
                object.destination_logisticnode
            ):  # if triprequest has chosen already an logistic node for origin
                destination_location = None
            else:
                destination_location = object.destination_location

            result.append(
                check_coordinates_inside_geojson_files(
                    geojson_files, origin_location, destination_location
                )
            )
            nodes_results.append(result)
    return nodes_results
