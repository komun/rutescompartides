from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from core.models import (
    UserProfile,
    TripOffer,
    TripMatch,
    TripRequest,
    MatchStatus,
    search_trip_requests,
    search_trip_offers,
    Storage,
    LogisticNode,
    FrequencyType,
)

from tempus_dominus.widgets import DateTimePicker, DatePicker
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AnonymousUser
from django.conf import settings
from dateutil.relativedelta import relativedelta


def file_size(value):  # add this to some file where you can import it from
    limit = 3 * 1024 * 1024
    if value.size > limit:
        raise ValidationError("L'adjunt té una mida massa gran. Màxim 3MB.")


class TripOfferForm(forms.ModelForm):
    required_css_class = "required"
    original_time_depart = forms.DateTimeField(required=False)
    original_time_arrival = forms.DateTimeField(required=False)
    time_depart = forms.DateTimeField(
        required=True,
        label="Hora de sortida",
        widget=DateTimePicker(
            options={
                # 'useCurrent': True,
                "collapse": False,
                "locale": "ca",
                "format": "DD/MM/YYYY HH:mm",
            },
            attrs={
                "append": "fa fa-calendar",
                "icon_toggle": True,
            },
        ),
        # initial=timezone.localtime(),
    )
    time_arrival = forms.DateTimeField(
        label="Hora d'arribada aproximada",
        widget=DateTimePicker(
            options={
                "format": "DD/MM/YYYY HH:mm",
                "useCurrent": True,
                "collapse": False,
                "locale": "ca",
            },
            attrs={
                "append": "fa fa-calendar",
                "icon_toggle": True,
            },
        ),
        # initial=timezone.localtime() + timezone.timedelta(0, 3600 * 5, 0),
    )
    transport_dry = forms.BooleanField(required=False)
    transport_isothermic = forms.BooleanField(required=False)
    transport_cold = forms.BooleanField(required=False)
    transport_freezer = forms.BooleanField(required=False)
    frequency = forms.ChoiceField(
        label="Freqüència de la ruta",
        required=True,
        choices=FrequencyType.choices,
        help_text="Crearà totes les rutes que pertoqui fins a la data final indicada, amb un màxim de 3 mesos. Recorda editar o eliminar les rutes si canvien a posteriori o n'hi ha alguna que finalment no faràs. La freqüència mensual implica aquell dia de la setmana i la mateixa setmana de cada mes (és a dir, el 1er dimecres de mes, o el 2on dijous de mes, etc).",
    )
    repeat_until = forms.DateField(
        label="Data màxima de repetició",
        required=False,
        widget=DatePicker(
            options={
                "format": "DD/MM/YYYY",
                # "useCurrent": True,
                "collapse": False,
                "locale": "ca",
                "display": False,
            },
            attrs={
                "append": "fa fa-calendar",
                "icon_toggle": True,
            },
        ),
        # initial=timezone.localtime() + timezone.timedelta(0, 3600 * 5, 0),
    )
    max_detour_km = forms.IntegerField(
        required=True, initial=10, label="Max. desviament(km)"
    )
    tags = forms.CharField(required=False)
    save_scope = forms.CharField(required=False)
    object_id = forms.IntegerField(required=False)
    match_object = forms.IntegerField(required=False)
    internal_name = forms.CharField(required=False, label="Nom intern de la ruta")
    origin_name = forms.CharField(required=True, label="Punt de sortida")
    destination_name = forms.CharField(required=True, label="Punt d'arribada")
    cost_km = forms.FloatField(
        required=False,
        label="Cost en € per cada KM recorregut",
        help_text='Per ajudar amb una estimació del preu del transport compartit. En cas que prefereixis oferir una alternativa (productes a canvi del transport, altres monedes, etc.) pots utilitzar el camp "Comentaris" (a sota) per especificar la teva proposta i l\'etiqueta "intercanvi" o "monedasocial" al camp "Etiquetes"',
    )

    class Meta:
        model = TripOffer
        widgets = {
            "comment": forms.Textarea(
                attrs={
                    "rows": 3,
                    "placeholder": "Si hi ha condicions especials quant a la recollida, entrega o transport, especificar-ho en aquest espai (ex: faré una parada de 6 hores al punt intermedi X de la meva ruta abans d'arribar al destí final; necessito confirmació d'horari d'entrega als comentaris per poder acceptar comandes, accepto productes com a forma de pagament, etc.)",
                }
            ),
            "available_space": forms.Textarea(
                attrs={
                    "rows": 2,
                    "placeholder": "m3 aprox.  (o una altra descripció que ajudi a entendre si hi cap un palet / hi ha espai per a 1 caixa...)",
                }
            ),
        }
        fields = (
            "internal_name",
            "origin_name",
            "origin_location",
            "destination_name",
            "destination_location",
            "time_depart",
            "time_arrival",
            "comment",
            "tags",
            "max_detour_km",
            "available_seats",
            "available_space",
            "cost_km",
            "vehicle",
            "transport_dry",
            "transport_isothermic",
            "transport_cold",
            "transport_freezer",
            "step_location_1",
            "step_name_1",
            "step_location_2",
            "step_name_2",
            "step_location_3",
            "step_name_3",
            "step_location_4",
            "step_name_4",
            "step_location_5",
            "step_name_5",
            "step_location_6",
            "step_name_6",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"

    def clean(self):
        errors = []
        cleaned_data = super().clean()
        print("cleaned_data", cleaned_data)

        object_id = cleaned_data.get("object_id")
        match_object = cleaned_data.get("match_object")

        transport_dry = cleaned_data.get("transport_dry")
        transport_isothermic = cleaned_data.get("transport_isothermic")
        transport_cold = cleaned_data.get("transport_cold")
        transport_freezer = cleaned_data.get("transport_freezer")
        max_detour_km = cleaned_data.get("max_detour_km")
        route = None
        origin = cleaned_data.get("origin_location")
        destination = cleaned_data.get("destination_location")
        print("origin", origin)
        print("destination", destination)
        time_depart = cleaned_data.get("time_depart")
        time_arrival = cleaned_data.get("time_arrival")
        repeat_until = cleaned_data.get("repeat_until")
        frequency = cleaned_data.get("frequency")
        save_scope = cleaned_data.get("save_scope", "this")

        print("time_depart", time_depart)
        print("time_arrival", time_arrival)

        if object_id:
            trip_offer = TripOffer.objects.filter(id=object_id).first()
            print("trip_offer.origin_location", trip_offer.origin_location)
            print("trip_offer.origin_name", trip_offer.origin_name)

            print("trip_offer.destination_location", trip_offer.destination_location)
            print("trip_offer.destination_name", trip_offer.destination_name)
            if (
                trip_offer.origin_location == origin
                and trip_offer.destination_location == destination
            ):
                route = trip_offer.route

            if frequency == "MONTHLY" and save_scope != "this":

                if (
                    time_depart != trip_offer.time_depart
                    or time_arrival != trip_offer.time_arrival
                ):
                    errors.append(
                        "Les rutes mensuals no poden modificar la data de sortida o arribada si es canvie per a tota la sèrie."
                    )

        if not object_id and frequency != "ONCE" and time_depart:
            if not repeat_until:
                errors.append(
                    "La data màxima de repetició ha d'omplir-se en cas de ser una ruta recurrent."
                )
            else:
                max_future_date = time_depart.date() + relativedelta(
                    months=settings.MAX_MONTHS_FREQUENT
                )
                if repeat_until > max_future_date:
                    errors.append(
                        "La repetició d'una ruta no pot superar els 3 mesos des de la data de sortida."
                    )

        trip_requests = search_trip_requests(
            AnonymousUser,
            route,
            origin,
            destination,
            transport_isothermic,
            transport_cold,
            transport_freezer,
            transport_dry,
            time_arrival,
            [],
            max_detour_km,
            exclude_frozen=False,
        )
        print("Trip requests found after editing/creation...", trip_requests)
        if object_id:
            pending_matches = trip_offer.matches.filter(
                status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
            )
            for match in pending_matches:
                print("Checking request", match.request.id)
                if match.request not in trip_requests:
                    print("Trip Request", match.request.id, " not found anymore.")
                    errors.append(
                        f"Els canvis no son compatibles amb la comanda #{match.request.id} associada a aquesta ruta. Cancel·la abans la comanda si vols guardar els canvis de la ruta."
                    )
        elif match_object:
            trip_request = TripRequest.objects.filter(id=match_object).first()

            if trip_request not in trip_requests:
                print(
                    "Trip Request",
                    trip_request.id,
                    " not matching the offer about to be created.",
                )
                errors.append(
                    "Les dades de la oferta de transport no son compatibles amb la comanda que es vol cobrir."
                )

        if time_depart < timezone.now():
            errors.append("La data de sortida no pot ser anterior al moment actual.")

        if time_arrival < timezone.now():
            errors.append("La data d'arribada no pot ser anterior al moment actual.")

        if time_arrival < time_depart:
            errors.append(
                "La data de sortida no pot ser igual o superior a la d'arribada."
            )

        if time_arrival == time_depart:
            errors.append("Has de posar una hora d'arribada diferent a la de sortida.")

        if errors:
            raise ValidationError(errors)


class TripRequestForm(forms.ModelForm):
    required_css_class = "required"
    max_time_arrival = forms.DateTimeField(
        label="Data màxima d'arribada",
        required=False,
        widget=DateTimePicker(
            options={
                "format": "DD/MM/YYYY HH:mm",
                "useCurrent": True,
                "collapse": False,
                "locale": "ca",
            },
            attrs={
                "append": "fa fa-calendar",
                "icon_toggle": True,
            },
        ),
        # initial=timezone.localtime() + timezone.timedelta(0, 3600 * 5, 0),
    )
    min_time_arrival = forms.DateTimeField(
        label="Data mínima d'arribada",
        required=True,
        widget=DateTimePicker(
            options={
                "format": "DD/MM/YYYY HH:mm",
                "useCurrent": True,
                "collapse": False,
                "locale": "ca",
            },
            attrs={
                "append": "fa fa-calendar",
                "icon_toggle": True,
            },
        ),
        # initial=timezone.localtime(),
    )
    transport_dry = forms.BooleanField(required=False)
    transport_isothermic = forms.BooleanField(required=False)
    transport_cold = forms.BooleanField(required=False)
    transport_freezer = forms.BooleanField(required=False)
    tags = forms.CharField(required=False)
    object_id = forms.IntegerField(required=False)
    match_object = forms.IntegerField(required=False)
    internal_name = forms.CharField(required=False, label="Nom intern de la comanda")
    packages_num = forms.IntegerField(
        required=True, initial=1, label="Nombre de paquets"
    )
    packages_height = forms.DecimalField(required=True, initial=1, label="Alçària(cm)")
    packages_width = forms.DecimalField(required=True, initial=1, label="Amplada(cm)")
    packages_length = forms.DecimalField(required=True, initial=1, label="Llargada(cm)")
    packages_weight = forms.DecimalField(required=True, initial=1, label="Pes(kg)")
    packages_fragile = forms.BooleanField(
        required=False, initial=False, label="Són fràgils"
    )
    delivery_note = forms.FileField(
        required=False,
        label="Albarà (PDF)",
        help_text="Adjuntar aquesta documentació és útil per a qui fa el transport, però no imprescindible.",
        validators=[file_size],
    )

    origin_name = forms.CharField(required=True, label="Punt de sortida")
    destination_name = forms.CharField(required=True, label="Punt d'arribada")

    class Meta:
        model = TripRequest
        widgets = {
            "comment": forms.Textarea(
                attrs={
                    "rows": 3,
                    "placeholder": "Si hi ha condicions especials quant a la recollida, entrega, transport, etc. Especificar en aquest espai (ex: tipus de punt d'entrega final; adreça/horari de tancament de la botiga on ha d'arribar la comanda; etc.)",
                },
            ),
            "delivery_contact": forms.Textarea(
                attrs={
                    "required": False,
                    "rows": 3,
                    "placeholder": "Dades de contactes per al transportista.",
                }
            ),
        }
        fields = (
            "internal_name",
            "origin_name",
            "origin_location",
            "destination_name",
            "destination_location",
            "max_time_arrival",
            "min_time_arrival",
            "comment",
            "delivery_contact",
            "delivery_note",
            "transport_dry",
            "transport_isothermic",
            "transport_cold",
            "transport_freezer",
            "wants_carpool",
            "wants_delivery_notification",
            "packages_num",
            "packages_height",
            "packages_width",
            "packages_length",
            "packages_weight",
            "packages_fragile",
        )

    def __init__(self, *args, **kwargs):
        form = super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        print("form", form)
        print("args", args)
        print("kwargs", kwargs)

    def save(self, *args, **kw):
        print("Saving trip request")
        print(self.cleaned_data)
        # import ipdb; ipdb.set_trace()
        origin_location = self.cleaned_data.get("origin_location")
        origin_name = self.cleaned_data.get("origin_name")
        print("origin_name", origin_name)
        print("origin_location", origin_location)

        return super().save(*args, **kw)

    def clean(self):
        errors = []

        print("Form Clean", self.cleaned_data)
        cleaned_data = super().clean()
        print("cleaned_data", cleaned_data)
        object_id = cleaned_data.get("object_id")
        match_object = cleaned_data.get("match_object")

        transport_dry = cleaned_data.get("transport_dry")
        transport_isothermic = cleaned_data.get("transport_isothermic")
        transport_cold = cleaned_data.get("transport_cold")
        transport_freezer = cleaned_data.get("transport_freezer")

        origin = cleaned_data.get("origin_location")
        destination = cleaned_data.get("destination_location")
        min_time_arrival = cleaned_data.get("min_time_arrival")
        max_time_arrival = cleaned_data.get("max_time_arrival")
        offers = search_trip_offers(
            AnonymousUser,
            origin,
            destination,
            transport_isothermic,
            transport_cold,
            transport_freezer,
            transport_dry,
            min_time_arrival,
            max_time_arrival,
            [],
        )
        print("Offers found after editing/creation...", offers)
        if object_id:
            trip_request = TripRequest.objects.filter(id=object_id).first()
            pending_matches = trip_request.matches.filter(
                status__in=[MatchStatus.PENDING, MatchStatus.ACCEPTED]
            )
            pending_offers = []
            for match in pending_matches:
                pending_offers.append(match.offer)
                if match.offer not in offers:
                    print("Offer", match.offer.id, " not found anymore.")
                    errors.append(
                        "Els canvis no son compatibles amb la ruta associada a aquesta comanda. Cancel·la la ruta abans si vols guardar els canvis."
                    )
        else:
            if min_time_arrival.date() < timezone.datetime.today().date():
                errors.append(
                    "La data d'arribada mínima no pot ser més antiga que el dia d'avui."
                )
            if match_object:
                trip_offer = TripOffer.objects.filter(id=match_object).first()
                if trip_offer not in offers:
                    print(
                        "Offer",
                        trip_offer.id,
                        " not matching the trip_request about to be created.",
                    )
                    errors.append(
                        "Les dades de la comanda no son compatibles amb la ruta que es vol demanar."
                    )

        if max_time_arrival:
            if max_time_arrival.date() < timezone.datetime.today().date():
                errors.append(
                    "La data d'arribada màxima no pot ser més antiga que el dia d'avui."
                )

            if max_time_arrival <= min_time_arrival:
                errors.append(
                    "La data d'arribada mínima no pot ser superior a la màxima."
                )

        if errors:
            raise ValidationError(errors)


def json_extra(data):
    return {
        "im_telegram": data["im_telegram"],
        "im_whatsapp": data["im_whatsapp"],
        "im_signal": data["im_signal"],
    }


class ConfirmDeliveryForm(forms.ModelForm):
    required_css_class = "required"
    # delivery_comment = forms.CharField(max_length=2000)
    delivery_signature = forms.FileField(
        required=False,
        label="Signatura del/de la receptor/a:",
        help_text="Aquest camp es opcional",
        validators=[file_size],
    )
    delivery_photo = forms.FileField(
        required=False,
        label="Foto de l'entrega:",
        help_text="Aquest camp es opcional",
        validators=[file_size],
    )
    delivery_comment = forms.CharField(
        label="Comentaris sobre l'entrega",
        help_text="",
        widget=forms.Textarea(
            attrs={
                "rows": 3,
                "help_text": "Seran enviats al usuari qui ha enviat la comanda.",
            }
        ),
    )

    class Meta:
        model = TripMatch
        # widgets = {
        #     "delivery_comment": forms.Textarea(
        #
        #     )
        # }
        fields = (
            "delivery_comment",
            "delivery_signature",
            "delivery_photo",
        )

    # def __init__(self, *args, **kwargs):
    #     form = super().__init__(*args, **kwargs)
    #     self.helper = FormHelper()
    #     self.helper.form_method = "post"
    #     print("form", form)
    #     print("args", args)
    #     print("kwargs", kwargs)
    #
    # def save(self, *args, **kw):
    #     print("Saving storage")
    #     print(self.cleaned_data)
    #
    #     return super().save(*args, **kw)
    #


class StorageForm(forms.ModelForm):
    required_css_class = "required"

    transport_dry = forms.BooleanField(required=False)
    transport_isothermic = forms.BooleanField(required=False)
    transport_cold = forms.BooleanField(required=False)
    transport_freezer = forms.BooleanField(required=False)
    accepts_pallets = forms.BooleanField(required=False)
    tags = forms.CharField(required=False)
    is_public = forms.BooleanField(required=False)
    name = forms.CharField(required=False, label="Nom del punt habitual")
    origin_name = forms.CharField(required=True, label="Localització")

    class Meta:
        model = Storage
        widgets = {
            "notes": forms.Textarea(
                attrs={
                    "rows": 6,
                    "placeholder": "Condicions, contactes, horaris, etc. (important en cas de tractar-se d'un punt habitual públic.)",
                }
            )
        }
        fields = (
            "name",
            "origin_name",
            "origin_location",
            "notes",
            "is_public",
            "transport_dry",
            "transport_isothermic",
            "transport_cold",
            "transport_freezer",
            "accepts_pallets",
        )

    def __init__(self, *args, **kwargs):
        form = super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        print("form", form)
        print("args", args)
        print("kwargs", kwargs)

    def save(self, *args, **kw):
        print("Saving storage")
        print(self.cleaned_data)
        # import ipdb; ipdb.set_trace()
        origin_location = self.cleaned_data.get("origin_location")
        origin_name = self.cleaned_data.get("origin_name")
        print("origin_name", origin_name)
        print("origin_location", origin_location)

        return super().save(*args, **kw)

class LogisticNodeForm(forms.ModelForm):
    required_css_class = "required"

    name = forms.CharField(required=False, label="Nom del Node Logístic")
    origin_name = forms.CharField(required=True, label="Localització")
    delivery_areas = forms.CharField(required=False, label="Zones de distribució",  widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    has_isothermic = forms.BooleanField(label="Espai estanc aïllat isoterm", required=False)
    has_frigo_room = forms.BooleanField(label="Cambra frigorífica", required=False)
    has_fridge = forms.BooleanField(label="Nevera", required=False)
    has_freezer = forms.BooleanField(label="Congelador", required=False)
    has_cold_dry = forms.BooleanField(label="Fred sense humitat", required=False)
    has_toro = forms.BooleanField(label="Toro", required=False)
    has_traspalet = forms.BooleanField(label="Traspalet", required=False)
    has_loading_dock = forms.BooleanField(label="Moll de descàrrega camions", required=False)
    allows_parking = forms.BooleanField(label="Permet estacionament/aturada de vehicle davant la porta", required=False)

    class Meta:
        model = LogisticNode
        fields = (
            "name",
            "phone",
            "email",
            "comments",
            "origin_name",
            "origin_location",
            "timetable",
            "dimensions_fresh_m2",
            "dimensions_dry_m2",
            "has_isothermic",
            "has_frigo_room",
            "has_fridge",
            "has_freezer",
            "has_cold_dry",
            "has_toro",
            "has_traspalet",
            "has_loading_dock",
            "delivery_companies",
            "vehicle_types",
            "timetable",
            "allows_parking",
            "agreement_type",
            "price_per_dry_storage_day",
            "price_per_fresh_storage_day",
            "delivery_areas",
            "api_url",
            "api_client_id",
            "api_client_secret",
            "checkout_form_url"
        )


class UserProfileForm(forms.ModelForm):
    first_name = forms.CharField(label=_("Nom"), max_length=30)
    last_name = forms.CharField(label=_("Cognoms"), max_length=30)
    email = forms.CharField(label=_("Correu electrònic"), max_length=30)
    im_telegram = forms.BooleanField(label="Disponible per Telegram", required=False)
    im_whatsapp = forms.BooleanField(label="Contactable per WhatsApp", required=False)
    im_signal = forms.BooleanField(label="Contactable per Signal", required=False)
    stop_email_notifications = forms.BooleanField(
        label="Deixar de rebre notificacions de l'app via email", required=False
    )

    class Meta:
        model = UserProfile
        fields = (
            "phone",
            "im_whatsapp",
            "im_telegram",
            "im_signal",
            "notes",
            "organization",
            "stop_email_notifications",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.fields["first_name"].initial = self.instance.user.first_name
        self.fields["last_name"].initial = self.instance.user.last_name
        self.fields["email"].initial = self.instance.user.email
        if self.instance.extra:
            self.fields["im_telegram"].initial = self.instance.extra["im_telegram"]
            self.fields["im_whatsapp"].initial = self.instance.extra["im_whatsapp"]
            self.fields["im_signal"].initial = self.instance.extra["im_signal"]

    def save(self, *args, **kw):
        print("userprofileform save", self.cleaned_data)
        self.instance.user.first_name = self.cleaned_data.get("first_name")
        self.instance.user.last_name = self.cleaned_data.get("last_name")
        self.instance.user.email = self.cleaned_data.get("email")
        self.instance.extra = json_extra(self.cleaned_data)
        super().save(*args, **kw)
        self.instance.user.save()


from django.contrib.auth.forms import AuthenticationForm

class EmailAuthenticationForm(AuthenticationForm):
    username = forms.EmailField(widget=forms.TextInput(attrs={'autofocus': True}), label='Correu electrònic')
