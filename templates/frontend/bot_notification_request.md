#DEMANDA
{% if edited %}Editada {% else %}Nova {% endif %}[demanda de transport]({{trip_request_url}}) de [{{trip_request.client.username}}]({{username_url}}):

🏠 De: {{trip_request.origin_name}}
📅 Sortida min.: {{trip_request.min_time_arrival|date:"D j \d\e F \d\e Y, H:i"}}

🚩Fins a: {{trip_request.destination_name}}
📅 Arribada max.: {% if trip_request.max_time_arrival is None %}Flexible{% else %}{{trip_request.max_time_arrival|date:"D j \d\e F \d\e Y, H:i"}}{% endif %}

{% if trip_request.comment %}Comentari: _{{trip_request.comment}}_{% endif %}
{% if hashtags %}{{hashtags}}{% endif %}
