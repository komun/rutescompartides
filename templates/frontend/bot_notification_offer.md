#OFERTA
{% if edited %}Editada {% else %}Nova {% endif %}[oferta de transport]({{offer_url}}) de [{{trip_offer.driver.username}}]({{username_url}}):

🏠 De: {{trip_offer.origin_name}}
📅 Sortida: {{trip_offer.time_depart|date:"D j \d\e F \d\e Y, H:i"}}
{% if trip_offer.steps_name %}Punts intermitjos: {{trip_offer.steps_name}}{% endif %}
🚩Fins a: {{trip_offer.destination_name}}
📅 Arribada: {{trip_offer.time_arrival|date:"D j \d\e F \d\e Y, H:i"}}

{% if trip_offer.comment %}Comentari: _{{trip_offer.comment}}_{% endif %}
{% if hashtags %}{{hashtags}}{% endif %}
