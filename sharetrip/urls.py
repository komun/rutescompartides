import notifications.urls
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from core import views
from core.views import logistic_nodes

# from django_registration.backends.one_step.views import RegistrationView
from django_registration.forms import RegistrationFormUniqueEmail
from rest_framework.authtoken.views import ObtainAuthToken

urlpatterns = [
    path("", views.frontend_view, name="index"),
    path("index.html", views.frontend_view),
    path("manifest.webmanifest", views.render_manifest),
    path(
        "accounts/register/",
        views.RegistrationView.as_view(form_class=RegistrationFormUniqueEmail),
        name="django_registration_register",
    ),
    # path("accounts/", include("django_registration.backends.one_step.urls")),
    path("accounts/login/", views.CustomLoginView.as_view(), name="custom_login"),
    path("accounts/", include("django_registration.backends.activation.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("admin/", admin.site.urls),
    path("chat/", include("chat.urls")),
    # path("baton/", include("baton.urls")),
    # path("jsi18n/", JavaScriptCatalog.as_view(), name="javascript_catalog"),
    path(
        "inbox/notifications/", include(notifications.urls, namespace="notifications")
    ),
    path("page/<str:pagename>/", views.page_endpoints),
    path("offer/add/", views.TripOfferAddView.as_view(), name="tripoffer-add"),
    path(
        "offer/edit/<int:offer_id>",
        views.TripOfferUpdateView.as_view(),
        name="tripoffer-edit",
    ),
    path(
        "offer/view/<int:offer_id>",
        views.TripOfferDetailView.as_view(),
        name="tripoffer-detail",
    ),
    path(
        "offer/summary/<str:format>/<int:offer_id>/",
        views.export_summary_offer,
        name="tripoffer-summary",
    ),
    path(
        "offer/mapview/<int:offer_id>",
        views.TripOfferMapView.as_view(),
        name="tripoffer-mapview",
    ),
    # path(
    #     "offer/view/all/",
    #     views.get_all_tripoffer_json,
    #     name="tripoffer-all-json",
    # ),
    path(
        "offer/view/all/geojson",
        views.get_all_tripoffer_geojson,
        name="tripoffer-all-geojson",
    ),
    path(
        "offer/view/<int:offer_id>/geojson",
        views.get_tripoffer_geojson,
        name="tripoffer-geojson",
    ),
    path("offer/search/", views.search_offers_view, name="tripoffer-search"),
    path("offer/first/", views.first_offers_view, name="tripoffer-first"),
    path("offer/user/me/", views.user_offers_view, name="tripoffer-user-view"),
    path(
        "offer/update/<int:offer_id>/", views.update_tripoffer, name="tripoffer-update"
    ),
    path(
        "offer/use_logistic_node/<int:offer_id>/<int:logisticnode_id>/<str:point>",
        views.tripoffer_take_logisticnode,
        name="tripoffer-take-logisticnode",
    ),
    path(
        "offer/duplicate/<int:offer_id>/",
        views.duplicate_tripoffer,
        name="tripoffer-duplicate",
    ),
    path(
        "offer/frequent/<int:frequency_group_id>/",
        views.frequent_offers_view,
        name="tripoffer-frequent-view",
    ),
    path("request/add/", views.TripRequestAddView.as_view(), name="triprequest-add"),
    path(
        "request/edit/<int:request_id>",
        views.TripRequestUpdateView.as_view(),
        name="triprequest-edit",
    ),
    path(
        "request/update/<int:request_id>/",
        views.update_triprequest,
        name="triprequest-update",
    ),
    path("request/search/", views.search_requests_view, name="triprequest-search"),
    path("request/first/", views.first_requests_view, name="triprequest-first"),
    path(
        "request/view/<int:request_id>",
        views.TripRequestDetailView.as_view(),
        name="triprequest-detail",
    ),
    path(
        "request/view/<int:request_id>/geojson",
        views.get_triprequest_geojson,
        name="triprequest-geojson",
    ),
    path(
        "request/view/all/geojson",
        views.get_all_triprequest_geojson,
        name="triprequest-all-geojson",
    ),
    path(
        "request/mapview/<int:request_id>",
        views.TripRequestMapView.as_view(),
        name="triprequest-mapview",
    ),
    path("request/user/me/", views.user_requests_view, name="triprequest-user-view"),
    path(
        "request/to_offer/<int:request_id>/",
        views.convert_request_to_offer,
        name="triprequest-convert-offer",
    ),
    path(
        "request/use_logistic_node/<int:request_id>/<int:logisticnode_id>/<str:point>",
        views.triprequest_take_logisticnode,
        name="triprequest-take-logisticnode",
    ),
    # Actions with Storages and Logistic Nodes
    path("storage/user/me/", views.user_storages_view, name="storage-user-view"),
    path("storage/add/", views.StorageAddView.as_view(), name="storage-add"),
    path(
        "storage/view/<int:storage_id>",
        views.StorageDetailView.as_view(),
        name="storage-detail",
    ),
    path(
        "storage/view/<int:storage_id>/geojson",
        views.get_storage_geojson,
        name="storage-geojson",
    ),
    path(
        "storage/view/all/geojson",
        views.get_all_storage_geojson,
        name="storage-all-geojson",
    ),
    path(
        "storage/edit/<int:storage_id>",
        views.StorageUpdateView.as_view(),
        name="storage-edit",
    ),
    path(
        "storage/update/<int:object_id>/",
        views.update_storage,
        name="storage-update",
    ),
    path(
        "logisticnode/user/me/",
        logistic_nodes.user_logisticnodes_view,
        name="logisticnode-user-view",
    ),
    path(
        "logisticnode/add/",
        logistic_nodes.LogisticNodeAddView.as_view(),
        name="logisticnode-add",
    ),
    path(
        "logisticnode/view/<int:logisticnode_id>",
        logistic_nodes.LogisticNodeDetailView.as_view(),
        name="logisticnode-detail",
    ),
    path(
        "logisticnode/view/<int:logisticnode_id>/geojson",
        logistic_nodes.get_logisticnode_geojson,
        name="logisticnode-geojson",
    ),
    path(
        "logisticnode/view/all/geojson",
        logistic_nodes.get_all_logisticnode_geojson,
        name="logisticnode-all-geojson",
    ),
    path(
        "logisticnode/edit/<int:logisticnode_id>",
        logistic_nodes.LogisticNodeUpdateView.as_view(),
        name="logisticnode-edit",
    ),
    path(
        "logisticnode/update/<int:object_id>/",
        logistic_nodes.update_logisticnode,
        name="logisticnode-update",
    ),
    path(
        "profile/edit/<str:username>",
        views.UserProfileUpdateView.as_view(),
        name="profile-edit",
    ),
    path(
        "profile/view/<str:username>",
        views.UserProfileDetailView.as_view(),
        name="profile-view",
    ),
    path("match/add/", views.add_match_view, name="tripmatch-add"),
    path(
        "match/update/<int:match_id>/",
        views.update_match_status,
        name="tripmatch-update-status",
    ),
    path(
        "match/review/<int:match_id>/",
        views.update_match_review,
        name="tripmatch-review",
    ),
    path(
        "match/delivery/<int:match_id>/",
        views.update_confirmation_delivery,
        name="tripmatch-delivery",
    ),
    path(
        "points/view/<str:username>",
        views.view_points,
        name="points-view",
    ),
    path("test_make_notification/", views.make_notification),
    path(
        "export",
        views.database_export,
        name="database-export",
    ),
    # API endpoints
    path("api/token/", ObtainAuthToken.as_view(), name="api_token_auth"),
    path("api/register/", views.RegistrationAPIView.as_view(), name="api_register"),
    path(
        "api/tripoffer/<int:offer_id>/",
        views.TripOfferAPIView.as_view(),
        name="api-tripoffer-detail",
    ),
    path(
        "api/triprequest/<int:request_id>/",
        views.TripRequestAPIView.as_view(),
        name="api-triprequest-detail",
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import os.path

    # settings.STATICFILES_DIRS += os.path.join(settings.BASE_DIR, "frontend","static"),
    # print('settings.STATICFILES_DIRS', settings.STATICFILES_DIRS)
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    FRONTEND_URL = "/static/baton/"
    FRONTEND_ROOT = os.path.join(settings.BASE_DIR, "baton", "static")
    urlpatterns += static(FRONTEND_URL, document_root=FRONTEND_ROOT)
    FRONTEND_URL = "/static/core/"
    FRONTEND_ROOT = os.path.join(settings.BASE_DIR, "core", "static")
    urlpatterns += static(FRONTEND_URL, document_root=FRONTEND_ROOT)
    # print("FRONTEND_ROOT", FRONTEND_ROOT)
    # # Serve static and media files from development server
