import os

from pathlib import Path

APP_VERSION = "1.8.0"
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "$snie(6t6@ovwdpz5je-u0qwb%1n4h7ukb=&1_5t9xivq)*q)m"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
LOGIN_URL = "/accounts/login/"  # this is the name of the url
LOGOUT_REDIRECT_URL = "/"  # this is the name of the url
LOGIN_REDIRECT_URL = "/"  # this is the name of the url
ALLOWED_HOSTS = ["*"]

APPEND_SLASH = True

DEFAULT_LANGUAGE = "ca"  # default language profiles
DEFAULT_DOMAIN = "http://0.0.0.0:8000"

LANGUAGE_CHOICES = (
    ("ca", "Català"),
    ("es", "Castellano"),
)
TAGGIT_CASE_INSENSITIVE = True
# Application definition
REGISTRATION_OPEN = True
# change depending on domain
SIMPLE_BACKEND_REDIRECT_URL = "/"
# Default
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
REGISTRATION_EMAIL_HTML = True
AUTHENTICATION_BACKENDS = ["sharetrip.login_backend.EmailBackend"]

# EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
# EMAIL_HOST = "host_server_mail"
# EMAIL_HOST_USER = "user"
# EMAIL_HOST_PASSWORD = "password"
# DEFAULT_FROM_EMAIL = xxxxx"
# DEFAULT_REPLYTO_EMAIL = "xxxxx"
# EMAIL_PORT = 465
# EMAIL_USE_SSL = True
# EMAIL_USE_TLS = False

# Màxim temps per a fer una valoració
MINUTES_AFTER_REVIEW_FREEZES = 60 * 24 * 4  # 4 días normalmente
MINUTES_BEFORE_REVIEW_FREEZES_REMINDER = 60 * 24 * 2  # 2 días normalmente

# Abans de quants minuts es pot editar una ruta/comanda amb matches acceptada.
CAN_EDIT_WITH_MATCHES_MINUTES_MAX = 60 * 24  # 24h
REQUESTS_EXPIRE_MINUTES_MAX = 60 * 24 * 30  # usat per el script anterior
MINUTES_AFTER_OFFERS_FINALIZES = 60 * 1  # normalment 1h
MAX_MONTHS_FREQUENT = 3  # 3 months is the maximum repetition for a frequent tripoffer

# Parameters based on the route distance
# punts persona usuaria per registrar-se:
POINTS_REGISTRATION = 30

# - punts persona usuaria per enviar una comanda (primer km):
POINTS_FINISHED_ROUTE_CLIENT_FIRST_KM = 5

# - punts persona usuaria per enviar una comanda (per km adicional):
POINTS_FINISHED_ROUTE_CLIENT_EXTRA_KM = 1

# - punts transportista per fer arribar una comanda (primer km):
POINTS_FINISHED_ROUTE_DRIVER_FIRST_KM = 2

# - punts transportista per fer arribar una comanda (per km adicional):
POINTS_FINISHED_ROUTE_DRIVER_EXTRA_KM = 0

# - punts transportista adicionals per confirmar amb una foto d'entrega:
POINTS_FINISHED_ROUTE_DRIVER_VERIFICATION_PROOF = 0

# - euros que s'estalvia amb una empresa transportista (primer km):
AMOUNT_MONEY_FIRST_KM = 1

# - euros que s'estalvia amb una empresa transportista (per km adicional):
AMOUNT_MONEY_EXTRA_KM = 0.1

# - kgs (a cada 25KGs s’hi sumaria 1€
AMOUNT_MONEY_PER_EXTRA_25KG = 1

# - refrigerat/congelat (sí=+1€; no=0)
AMOUNT_MONEY_FREEZER_OR_COLD = 1

GAMIFICATION = [(0, "Benvingut"), (50, "Experimentat"), (500, "Figura")]

CRONJOBS = [
    (  # every 30 minutes
        "*/30 * * * *",
        "core.cron.autofrozen_job",
        ">>/tmp/sharetrip.log 2>>/tmp/sharetrip.log",
    ),
    (  # every 20 minutes
        "*/20 * * * *",
        "core.cron.autofinalize_job",
        ">>/tmp/sharetrip.log 2>>/tmp/sharetrip.log",
    ),  # every 20 minutes
    (  # every 4 hours
        "* */4 * * *",
        "core.cron.autopublish_telegram",
        ">>/tmp/sharetrip.log 2>>/tmp/sharetrip.log",
    ),
    (  # every 30 minutes
        "*/25 * * * *",
        "core.cron.mail_notification_unread_chats",
        ">>/tmp/sharetrip.log 2>>/tmp/sharetrip.log",
    ),
]

LEAFLET_CONFIG = {
    "TILES": "http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
    "DEFAULT_CENTER": (41.533254, 1.94458),
    "DEFAULT_ZOOM": 9,
    # "MIN_ZOOM": 9,
    # "MAX_ZOOM": 18,
    "RESET_VIEW": False,
}

STATIC_ROOT = os.path.join(BASE_DIR, "static")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_GEOJSON = os.path.join(MEDIA_ROOT, "geojson")
MEDIA_URL = "/media/"

# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, "baton", "static"),
#     os.path.join(BASE_DIR, "core", "static"),
# ]
# STATICFILES_FINDERS = [
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
# ]

INSTALLED_APPS = [
    # "baton",  # always before django.contrib.admin
    "django_crontab",
    "taggit",
    "tempus_dominus",
    "djgeojson",
    "django.contrib.admin",
    "django_registration",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "notifications",
    "core",
    "leaflet",
    "django_extensions",
    "crispy_forms",
    "chat.apps.ChatConfig",
    "rest_framework",
    "rest_framework.authtoken",
    "widget_tweaks",
    "baton.autodiscover",  # very end
]

REST_FRAMEWORK = {
    "DEFAULT_PARSER_CLASSES": ("rest_framework.parsers.JSONParser",),
}
CRISPY_TEMPLATE_PACK = "bootstrap4"
DJANGO_NOTIFICATIONS_CONFIG = {"USE_JSONFIELD": True}
SERIALIZATION_MODULES = {
    "geojson": "django.contrib.gis.serializers.geojson",
}

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    #"django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "sharetrip.urls"


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.globals_processor",
            ],
        },
    },
]

WSGI_APPLICATION = "sharetrip.wsgi.application"


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "mydatabase",
        "USER": "userdatabase",
        "PASSWORD": "userpassword",
        "HOST": "localhost",
        "PORT": "5432",
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = "ca-es"
TEMPUS_DOMINUS_LOCALIZE = True
TIME_ZONE = "Europe/Madrid"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = "/static/"

from sharetrip.local_settings import *
