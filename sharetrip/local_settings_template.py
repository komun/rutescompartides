

DEBUG = True
LOCAL = True
DEV = True
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "mail.xxxxx.net"
EMAIL_HOST_USER = "xxxx"
EMAIL_HOST_PASSWORD = "xxxx"
DEFAULT_FROM_EMAIL = "RutesCompartides.cat <rutescompartides@xxxx.net>"
DEFAULT_REPLYTO_EMAIL = "RutesCompartides.cat <rutescompartides@xxxx.net>"
EMAIL_PORT = 465
EMAIL_USE_SSL = True
EMAIL_USE_TLS = False

ADMINS_EMAIL = ["xxxx@xxx.net", "xxxx@xxxx.cat"]
LOGISTIC_NODE_ADMINS = ["xxxx@xxxxx.net"]

MEDIA_ROOT = "/absolutepath_to_sharetrip/rutescompartides/media/"
# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "gis_sharetrip",
        "USER": "xxxx",
        "PASSWORD": "xxxx",
        "HOST": "localhost",
        "PORT": "xxxxx",
    }
}
DEFAULT_DOMAIN = "http://0.0.0.0:8000"
TELEGRAM_BOT_ID = "xxxxx:xxxx"

TELEGRAM_CHAT_ID = 00000 #you can get it with the @myidbot